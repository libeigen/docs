var structEigen_1_1internal_1_1evaluator_3_01ReturnByValue_3_01Derived_01_4_01_4 =
[
    [ "Base", "structEigen_1_1internal_1_1evaluator.html#a23e380d87319deaa59368cdf3db87b2b", null ],
    [ "Base", "structEigen_1_1internal_1_1evaluator_3_01ReturnByValue_3_01Derived_01_4_01_4.html#a83b99ea3f0fa1030fa56750cb31bfc17", null ],
    [ "PlainObject", "structEigen_1_1internal_1_1evaluator_3_01ReturnByValue_3_01Derived_01_4_01_4.html#ad401c194bc0d4bf1768aa039d54df83a", null ],
    [ "XprType", "structEigen_1_1internal_1_1evaluator_3_01ReturnByValue_3_01Derived_01_4_01_4.html#a45412c388bf32160dec93549d4245b3b", null ],
    [ "evaluator", "structEigen_1_1internal_1_1evaluator_3_01ReturnByValue_3_01Derived_01_4_01_4.html#ad8a554b2447a2d885de0e52c7c35194c", null ],
    [ "evaluator", "structEigen_1_1internal_1_1evaluator.html#aa5256326a3cdce7abf548b8547780c09", null ],
    [ "m_result", "structEigen_1_1internal_1_1evaluator_3_01ReturnByValue_3_01Derived_01_4_01_4.html#abc40dbe3721e9fc75fa633f67d58f1c2", null ]
];