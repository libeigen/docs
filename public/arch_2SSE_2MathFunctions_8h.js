var arch_2SSE_2MathFunctions_8h =
[
    [ "Eigen::internal::psqrt< Packet16b >", "namespaceEigen_1_1internal.html#a6d957f7d79806b15808e77a4322b16e9", null ],
    [ "Eigen::internal::psqrt< Packet2d >", "namespaceEigen_1_1internal.html#ab80f0ddd4e5cdea3e5fc6c8e6e10e798", null ],
    [ "Eigen::internal::psqrt< Packet4f >", "namespaceEigen_1_1internal.html#a39d1181bf9250876c212fe7d79b23373", null ],
    [ "Eigen::numext::sqrt", "namespaceEigen_1_1numext.html#ae684eba905f6309911f1638cffb0e266", null ],
    [ "Eigen::numext::sqrt", "namespaceEigen_1_1numext.html#a6ac4c97636fc7501fb2194ac73264266", null ]
];