var classEigen_1_1Serializer_3_01SparseVector_3_01Scalar_00_01Options_00_01StorageIndex_01_4_00_01void_01_4 =
[
    [ "Header", "structEigen_1_1Serializer_3_01SparseVector_3_01Scalar_00_01Options_00_01StorageIndex_01_4_00_01void_01_4_1_1Header.html", "structEigen_1_1Serializer_3_01SparseVector_3_01Scalar_00_01Options_00_01StorageIndex_01_4_00_01void_01_4_1_1Header" ],
    [ "SparseMat", "classEigen_1_1Serializer_3_01SparseVector_3_01Scalar_00_01Options_00_01StorageIndex_01_4_00_01void_01_4.html#a52b8000fb40eefd171f2ff94caa28020", null ],
    [ "deserialize", "classEigen_1_1Serializer_3_01SparseVector_3_01Scalar_00_01Options_00_01StorageIndex_01_4_00_01void_01_4.html#ad375716c144b851ddeacd393462fa011", null ],
    [ "serialize", "classEigen_1_1Serializer_3_01SparseVector_3_01Scalar_00_01Options_00_01StorageIndex_01_4_00_01void_01_4.html#a1aaa5fe0e1b72b490ec27f6379ea9238", null ],
    [ "size", "classEigen_1_1Serializer_3_01SparseVector_3_01Scalar_00_01Options_00_01StorageIndex_01_4_00_01void_01_4.html#aa81deb5bab7a8efd2f1e715ead4d84fa", null ]
];