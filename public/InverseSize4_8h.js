var InverseSize4_8h =
[
    [ "Eigen::internal::compute_inverse_size4< Architecture::Target, double, MatrixType, ResultType >", "structEigen_1_1internal_1_1compute__inverse__size4_3_01Architecture_1_1Target_00_01double_00_01MatrixType_00_01ResultType_01_4.html", "structEigen_1_1internal_1_1compute__inverse__size4_3_01Architecture_1_1Target_00_01double_00_01MatrixType_00_01ResultType_01_4" ],
    [ "Eigen::internal::compute_inverse_size4< Architecture::Target, float, MatrixType, ResultType >", "structEigen_1_1internal_1_1compute__inverse__size4_3_01Architecture_1_1Target_00_01float_00_01MatrixType_00_01ResultType_01_4.html", "structEigen_1_1internal_1_1compute__inverse__size4_3_01Architecture_1_1Target_00_01float_00_01MatrixType_00_01ResultType_01_4" ]
];