var structEigen_1_1internal_1_1traits_3_01SelfAdjointView_3_01MatrixType_00_01UpLo_01_4_01_4 =
[
    [ "ExpressionType", "structEigen_1_1internal_1_1traits_3_01SelfAdjointView_3_01MatrixType_00_01UpLo_01_4_01_4.html#aeaf9375b523c8415dddb7e48b60a3347", null ],
    [ "FullMatrixType", "structEigen_1_1internal_1_1traits_3_01SelfAdjointView_3_01MatrixType_00_01UpLo_01_4_01_4.html#a0580ec066d0f63149d93af28a0c1ce84", null ],
    [ "MatrixTypeNested", "structEigen_1_1internal_1_1traits_3_01SelfAdjointView_3_01MatrixType_00_01UpLo_01_4_01_4.html#a78fbe1472ee4d87826b253c2b194062b", null ],
    [ "MatrixTypeNestedCleaned", "structEigen_1_1internal_1_1traits_3_01SelfAdjointView_3_01MatrixType_00_01UpLo_01_4_01_4.html#a4fb24cab0a6d26914ac9c7f20560255f", null ]
];