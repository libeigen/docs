var structEigen_1_1internal_1_1nearest__integer__packetop__impl_3_01Packet_00_01false_00_01true_01_4 =
[
    [ "run_ceil", "structEigen_1_1internal_1_1nearest__integer__packetop__impl_3_01Packet_00_01false_00_01true_01_4.html#af380f21f90c4dc8e87574cee9bedaa2c", null ],
    [ "run_ceil", "structEigen_1_1internal_1_1nearest__integer__packetop__impl.html#a6b49ffbf08e930fac02edb7893406b8e", null ],
    [ "run_floor", "structEigen_1_1internal_1_1nearest__integer__packetop__impl_3_01Packet_00_01false_00_01true_01_4.html#a93f2c796e3e1eaa1b43a7d24c14687c3", null ],
    [ "run_floor", "structEigen_1_1internal_1_1nearest__integer__packetop__impl.html#a0a20484e279e2dd2622f658373003afd", null ],
    [ "run_rint", "structEigen_1_1internal_1_1nearest__integer__packetop__impl_3_01Packet_00_01false_00_01true_01_4.html#ab77317c62ed48f64bc144a416b188bac", null ],
    [ "run_rint", "structEigen_1_1internal_1_1nearest__integer__packetop__impl.html#ab4d6844b9fe8e42786af3e74833cd78c", null ],
    [ "run_round", "structEigen_1_1internal_1_1nearest__integer__packetop__impl_3_01Packet_00_01false_00_01true_01_4.html#aa74c03ad63b9dfd03c1396bd91cebe04", null ],
    [ "run_round", "structEigen_1_1internal_1_1nearest__integer__packetop__impl.html#a7e43491a01d05269b4fdfc873e8e761e", null ],
    [ "run_trunc", "structEigen_1_1internal_1_1nearest__integer__packetop__impl_3_01Packet_00_01false_00_01true_01_4.html#a728b801536fc02213b4d1c0bbe3d3eeb", null ],
    [ "run_trunc", "structEigen_1_1internal_1_1nearest__integer__packetop__impl.html#a9d48264c073fefbecf9b346c9cdbc43d", null ]
];