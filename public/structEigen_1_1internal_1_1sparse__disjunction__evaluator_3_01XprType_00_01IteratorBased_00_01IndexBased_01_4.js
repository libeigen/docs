var structEigen_1_1internal_1_1sparse__disjunction__evaluator_3_01XprType_00_01IteratorBased_00_01IndexBased_01_4 =
[
    [ "InnerIterator", "classEigen_1_1internal_1_1sparse__disjunction__evaluator_3_01XprType_00_01IteratorBased_00_01IndexBased_01_4_1_1InnerIterator.html", "classEigen_1_1internal_1_1sparse__disjunction__evaluator_3_01XprType_00_01IteratorBased_00_01IndexBased_01_4_1_1InnerIterator" ],
    [ "LhsArg", "structEigen_1_1internal_1_1sparse__disjunction__evaluator_3_01XprType_00_01IteratorBased_00_01IndexBased_01_4.html#a7a066f5faef8aac10c154c6c2010daa1", null ],
    [ "LhsIterator", "structEigen_1_1internal_1_1sparse__disjunction__evaluator_3_01XprType_00_01IteratorBased_00_01IndexBased_01_4.html#a2fa7e55e5d0e636e09c3bea7ea3cf7bd", null ],
    [ "RhsArg", "structEigen_1_1internal_1_1sparse__disjunction__evaluator_3_01XprType_00_01IteratorBased_00_01IndexBased_01_4.html#a2b91ba1896ccf3361f82e7fb0049158d", null ],
    [ "RhsEvaluator", "structEigen_1_1internal_1_1sparse__disjunction__evaluator_3_01XprType_00_01IteratorBased_00_01IndexBased_01_4.html#aa1f664b5742df0f8b5712e7ef7cfdfb7", null ],
    [ "Scalar", "structEigen_1_1internal_1_1sparse__disjunction__evaluator_3_01XprType_00_01IteratorBased_00_01IndexBased_01_4.html#acbb41f931b14fd3f1d127204fcfcf02b", null ],
    [ "StorageIndex", "structEigen_1_1internal_1_1sparse__disjunction__evaluator_3_01XprType_00_01IteratorBased_00_01IndexBased_01_4.html#a97b077cafdc28ec8fa99df3a8e45ba84", null ],
    [ "sparse_disjunction_evaluator", "structEigen_1_1internal_1_1sparse__disjunction__evaluator_3_01XprType_00_01IteratorBased_00_01IndexBased_01_4.html#ac5885157803471ed9d71e07280d6e9dc", null ],
    [ "nonZerosEstimate", "structEigen_1_1internal_1_1sparse__disjunction__evaluator_3_01XprType_00_01IteratorBased_00_01IndexBased_01_4.html#a2313fdd47aba731e254dabb64a2dd68b", null ],
    [ "m_expr", "structEigen_1_1internal_1_1sparse__disjunction__evaluator_3_01XprType_00_01IteratorBased_00_01IndexBased_01_4.html#a97e8b6b97476484e6267d99807dd6795", null ],
    [ "m_functor", "structEigen_1_1internal_1_1sparse__disjunction__evaluator_3_01XprType_00_01IteratorBased_00_01IndexBased_01_4.html#a9c84a745790783e8f6dd3c5f1607c9c2", null ],
    [ "m_lhsImpl", "structEigen_1_1internal_1_1sparse__disjunction__evaluator_3_01XprType_00_01IteratorBased_00_01IndexBased_01_4.html#a67687a023f4c83adc391de5d62f17dd0", null ],
    [ "m_rhsImpl", "structEigen_1_1internal_1_1sparse__disjunction__evaluator_3_01XprType_00_01IteratorBased_00_01IndexBased_01_4.html#a3ad93eef88d182b61f18e947a6dbab71", null ]
];