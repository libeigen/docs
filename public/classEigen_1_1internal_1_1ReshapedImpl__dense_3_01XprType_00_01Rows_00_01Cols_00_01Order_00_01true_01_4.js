var classEigen_1_1internal_1_1ReshapedImpl__dense_3_01XprType_00_01Rows_00_01Cols_00_01Order_00_01true_01_4 =
[
    [ "Base", "classEigen_1_1internal_1_1ReshapedImpl__dense_3_01XprType_00_01Rows_00_01Cols_00_01Order_00_01true_01_4.html#af9b5f1fea3dcd3000ca2a367f0acfd2d", null ],
    [ "ReshapedType", "classEigen_1_1internal_1_1ReshapedImpl__dense_3_01XprType_00_01Rows_00_01Cols_00_01Order_00_01true_01_4.html#a21e9062bbd475422a4450b9edd579362", null ],
    [ "XprTypeNested", "classEigen_1_1internal_1_1ReshapedImpl__dense_3_01XprType_00_01Rows_00_01Cols_00_01Order_00_01true_01_4.html#ac85e2870d32da192c1bef631a456163f", null ],
    [ "ReshapedImpl_dense", "classEigen_1_1internal_1_1ReshapedImpl__dense_3_01XprType_00_01Rows_00_01Cols_00_01Order_00_01true_01_4.html#a1cac12c41505bf4459c3b0ea15a45e6e", null ],
    [ "ReshapedImpl_dense", "classEigen_1_1internal_1_1ReshapedImpl__dense_3_01XprType_00_01Rows_00_01Cols_00_01Order_00_01true_01_4.html#a95c317fbe56280ce23868259e3c4691e", null ],
    [ "innerStride", "classEigen_1_1internal_1_1ReshapedImpl__dense_3_01XprType_00_01Rows_00_01Cols_00_01Order_00_01true_01_4.html#a846335f9136dc7e12df1459d1439d80d", null ],
    [ "nestedExpression", "classEigen_1_1internal_1_1ReshapedImpl__dense_3_01XprType_00_01Rows_00_01Cols_00_01Order_00_01true_01_4.html#a751f1abd97df34451545a3a8cd5cd044", null ],
    [ "nestedExpression", "classEigen_1_1internal_1_1ReshapedImpl__dense_3_01XprType_00_01Rows_00_01Cols_00_01Order_00_01true_01_4.html#a2958233691292509052e32af714b2ec2", null ],
    [ "outerStride", "classEigen_1_1internal_1_1ReshapedImpl__dense_3_01XprType_00_01Rows_00_01Cols_00_01Order_00_01true_01_4.html#ae67fe42ee5d678eb5994c46e58668d7d", null ],
    [ "m_xpr", "classEigen_1_1internal_1_1ReshapedImpl__dense_3_01XprType_00_01Rows_00_01Cols_00_01Order_00_01true_01_4.html#af6f65408dd6c29a23c37b25b4c503b40", null ]
];