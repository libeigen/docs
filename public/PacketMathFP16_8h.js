var PacketMathFP16_8h =
[
    [ "Eigen::internal::is_arithmetic< Packet8h >", "structEigen_1_1internal_1_1is__arithmetic_3_01Packet8h_01_4.html", null ],
    [ "Eigen::internal::packet_traits< half >", "structEigen_1_1internal_1_1packet__traits_3_01half_01_4.html", "structEigen_1_1internal_1_1packet__traits_3_01half_01_4" ],
    [ "Eigen::internal::unpacket_traits< Packet16h >", "structEigen_1_1internal_1_1unpacket__traits_3_01Packet16h_01_4.html", "structEigen_1_1internal_1_1unpacket__traits_3_01Packet16h_01_4" ],
    [ "Eigen::internal::unpacket_traits< Packet32h >", "structEigen_1_1internal_1_1unpacket__traits_3_01Packet32h_01_4.html", "structEigen_1_1internal_1_1unpacket__traits_3_01Packet32h_01_4" ],
    [ "Eigen::internal::unpacket_traits< Packet8h >", "structEigen_1_1internal_1_1unpacket__traits_3_01Packet8h_01_4.html", "structEigen_1_1internal_1_1unpacket__traits_3_01Packet8h_01_4" ],
    [ "PACKET32H_TRANSPOSE_HELPER", "PacketMathFP16_8h.html#ab25d8f058b6d92bac6400c22a210b43a", null ],
    [ "Eigen::internal::Packet32h", "namespaceEigen_1_1internal.html#af0654b9a1f110013c81e535e56aa0069", null ],
    [ "Eigen::internal::combine2Packet16h", "namespaceEigen_1_1internal.html#af725bb47c9b72f84f275293307abcc31", null ],
    [ "Eigen::internal::extract2Packet16h", "namespaceEigen_1_1internal.html#a5151ff8455e6cbec01a543a30bd9a284", null ],
    [ "Eigen::internal::pabs< Packet32h >", "namespaceEigen_1_1internal.html#afa7e829727fee7234036ea6b603d5658", null ],
    [ "Eigen::internal::padd< Packet16h >", "namespaceEigen_1_1internal.html#a984370dbad0c2f02de4aee69669e6a8b", null ],
    [ "Eigen::internal::padd< Packet32h >", "namespaceEigen_1_1internal.html#acea1cee0fc16e39ec03735afca145c79", null ],
    [ "Eigen::internal::padd< Packet8h >", "namespaceEigen_1_1internal.html#a30273e0ecdbbda5ae387cc7216f3172b", null ],
    [ "Eigen::internal::pand", "namespaceEigen_1_1internal.html#a95314406466a7b4761f0ec5a13c5e805", null ],
    [ "Eigen::internal::pandnot", "namespaceEigen_1_1internal.html#a8befce7049e606b0363b88b5ed458508", null ],
    [ "Eigen::internal::pceil< Packet32h >", "namespaceEigen_1_1internal.html#a0d744e9250bd5c98689072862552cd62", null ],
    [ "Eigen::internal::pcmp_eq", "namespaceEigen_1_1internal.html#a718726bcc372789b7a7f77e75846d5dd", null ],
    [ "Eigen::internal::pcmp_le", "namespaceEigen_1_1internal.html#ae2f3808523cf843655ed8ddcda5f1299", null ],
    [ "Eigen::internal::pcmp_lt", "namespaceEigen_1_1internal.html#a5f702287dfdf550b842139f05100343e", null ],
    [ "Eigen::internal::pcmp_lt_or_nan", "namespaceEigen_1_1internal.html#a70c97e4d456903b243743eed89bd8d40", null ],
    [ "Eigen::internal::pconj< Packet32h >", "namespaceEigen_1_1internal.html#adb34975c1c9880859b02b3a53b567a55", null ],
    [ "Eigen::internal::pcos< Packet16h >", "namespaceEigen_1_1internal.html#ab0932c1ea7b2750a8c3cac27511668ac", null ],
    [ "Eigen::internal::pcos< Packet32h >", "namespaceEigen_1_1internal.html#ad848829fa4130f0e777fac67a8d985e8", null ],
    [ "Eigen::internal::pdiv< Packet16h >", "namespaceEigen_1_1internal.html#aed3f31d4442022ee1db5d434ac47cdd2", null ],
    [ "Eigen::internal::pdiv< Packet32h >", "namespaceEigen_1_1internal.html#a721c2322843f1fba3e4044e080b5d06e", null ],
    [ "Eigen::internal::pdiv< Packet8h >", "namespaceEigen_1_1internal.html#a0b27bf689d7fe68e4c3ab5b37132253e", null ],
    [ "Eigen::internal::pexp< Packet16h >", "namespaceEigen_1_1internal.html#ae6026f260ed1c98060015cb8bb3ef3b1", null ],
    [ "Eigen::internal::pexp< Packet32h >", "namespaceEigen_1_1internal.html#a8b37b95acadebf321c07f0ba843f8467", null ],
    [ "Eigen::internal::pexpm1< Packet16h >", "namespaceEigen_1_1internal.html#a025785e80997091e8e11860cd1b07950", null ],
    [ "Eigen::internal::pexpm1< Packet32h >", "namespaceEigen_1_1internal.html#a3535d6b9c384890c167aeb00b98447e7", null ],
    [ "Eigen::internal::pfirst< Packet32h >", "namespaceEigen_1_1internal.html#ae06f11f1cacea3e1a713d2bd75d28c26", null ],
    [ "Eigen::internal::pfloor< Packet32h >", "namespaceEigen_1_1internal.html#aebf6232cd109448fff253590c2fbc21e", null ],
    [ "Eigen::internal::pfrexp< Packet16h >", "namespaceEigen_1_1internal.html#a00bee1b34b69b60236af077a4c56fbd8", null ],
    [ "Eigen::internal::pfrexp< Packet32h >", "namespaceEigen_1_1internal.html#afd11ecf7447460e68cf33aa29b8d37de", null ],
    [ "Eigen::internal::pgather< Eigen::half, Packet32h >", "namespaceEigen_1_1internal.html#a53f73c15d59ca4de6c24f869624214cd", null ],
    [ "Eigen::internal::pldexp< Packet16h >", "namespaceEigen_1_1internal.html#a1f7748a63f097dc29966d6e56a0bf297", null ],
    [ "Eigen::internal::pldexp< Packet32h >", "namespaceEigen_1_1internal.html#acf4192ce865fca2610c31c2b2ca7e2f3", null ],
    [ "Eigen::internal::pload< Packet32h >", "namespaceEigen_1_1internal.html#a72dcd7c68c7cd946774c3b5a998b4c4b", null ],
    [ "Eigen::internal::ploaddup< Packet32h >", "namespaceEigen_1_1internal.html#a73ab08692065a1e7c8737acdeccca9a4", null ],
    [ "Eigen::internal::ploadquad< Packet32h >", "namespaceEigen_1_1internal.html#aee4443528e4a50016860b48216364fa6", null ],
    [ "Eigen::internal::ploadu< Packet32h >", "namespaceEigen_1_1internal.html#a04d1fcecef1bf67f7ca828ab057bbc3c", null ],
    [ "Eigen::internal::plog1p< Packet16h >", "namespaceEigen_1_1internal.html#a35339b701d8d4cbf6f84dc6cced7391f", null ],
    [ "Eigen::internal::plog1p< Packet32h >", "namespaceEigen_1_1internal.html#a067495b56cb555b75060c7d294627a7b", null ],
    [ "Eigen::internal::plog2< Packet16h >", "namespaceEigen_1_1internal.html#a47dbb54b723fd6970049788fe71248e7", null ],
    [ "Eigen::internal::plog2< Packet32h >", "namespaceEigen_1_1internal.html#a96e26b8802fab6c579fa4c5602517c69", null ],
    [ "Eigen::internal::plog< Packet16h >", "namespaceEigen_1_1internal.html#a6abced74961ce3778f05216cfbdf1465", null ],
    [ "Eigen::internal::plog< Packet32h >", "namespaceEigen_1_1internal.html#ad7bb225d8160be5900582bf34701234b", null ],
    [ "Eigen::internal::plset< Packet32h >", "namespaceEigen_1_1internal.html#a114d959d8f63dc05fce117e20be8440b", null ],
    [ "Eigen::internal::pmax< Packet32h >", "namespaceEigen_1_1internal.html#ab4329b8a30b3a3d969c1c4ba925112b3", null ],
    [ "Eigen::internal::pmin< Packet32h >", "namespaceEigen_1_1internal.html#a8e923e5f2bb7424d67331a7c05a65088", null ],
    [ "Eigen::internal::pmul< Packet16h >", "namespaceEigen_1_1internal.html#aa20960dc6bd8db3f631d6d04675b4397", null ],
    [ "Eigen::internal::pmul< Packet32h >", "namespaceEigen_1_1internal.html#a1a7aaae1631891ec34af17618aea9a3b", null ],
    [ "Eigen::internal::pmul< Packet8h >", "namespaceEigen_1_1internal.html#ad3e0e80ba5e254d466fc53ea58e6c420", null ],
    [ "Eigen::internal::pnegate< Packet32h >", "namespaceEigen_1_1internal.html#a738a0bcefa92dc4668d43e309396a3ba", null ],
    [ "Eigen::internal::por", "namespaceEigen_1_1internal.html#aa3e74cd4711c498c7f8980deb1e47922", null ],
    [ "Eigen::internal::preciprocal< Packet32h >", "namespaceEigen_1_1internal.html#ae98c8e8fa29fb7e67b2724e3c67d19d6", null ],
    [ "Eigen::internal::predux< Packet16h >", "namespaceEigen_1_1internal.html#a6e233bc8d77d79779bb3f288654a87c7", null ],
    [ "Eigen::internal::predux< Packet32h >", "namespaceEigen_1_1internal.html#a28e1b604e94b46555739250b557d258a", null ],
    [ "Eigen::internal::predux< Packet8h >", "namespaceEigen_1_1internal.html#a7d33853b00bbd60b6b77769916626c54", null ],
    [ "Eigen::internal::predux_half_dowto4< Packet32h >", "namespaceEigen_1_1internal.html#aaac80d7d52f488cb333e226d6e8e97e2", null ],
    [ "Eigen::internal::preverse", "namespaceEigen_1_1internal.html#a9cd31bde403be88d1d772b9faf48766b", null ],
    [ "Eigen::internal::print< Packet32h >", "namespaceEigen_1_1internal.html#a37af5be0cb18eeede7e3c7a6eba619e9", null ],
    [ "Eigen::internal::pround< Packet32h >", "namespaceEigen_1_1internal.html#a0f24f2748f34852a47743b1e2f940e3b", null ],
    [ "Eigen::internal::prsqrt< Packet32h >", "namespaceEigen_1_1internal.html#a2beba7b91baffe981f49d3dd55bf3928", null ],
    [ "Eigen::internal::pscatter< half, Packet32h >", "namespaceEigen_1_1internal.html#a005d3b93f901fbee77f61966ab0299e9", null ],
    [ "Eigen::internal::pselect", "namespaceEigen_1_1internal.html#a87a0fd31381105909cb912021e710ca8", null ],
    [ "Eigen::internal::pset1< Packet32h >", "namespaceEigen_1_1internal.html#a6a73f0797c3fa5242efd92ee1829e4e3", null ],
    [ "Eigen::internal::pset1frombits< Packet32h >", "namespaceEigen_1_1internal.html#ae5278f184c86865f4c284ad0ae0e9062", null ],
    [ "Eigen::internal::psignbit< Packet32h >", "namespaceEigen_1_1internal.html#adf250c895f8e5dd7cac6b683c79c90f0", null ],
    [ "Eigen::internal::psin< Packet16h >", "namespaceEigen_1_1internal.html#acc41210510fe4c31095fe4e811d077ce", null ],
    [ "Eigen::internal::psin< Packet32h >", "namespaceEigen_1_1internal.html#a7e052dc80af64e7fcec90d1c9c942211", null ],
    [ "Eigen::internal::psqrt< Packet32h >", "namespaceEigen_1_1internal.html#ab31240cbc3efeb09d67fed212162f152", null ],
    [ "Eigen::internal::pstore< half >", "namespaceEigen_1_1internal.html#a9e46678aaf0cd7969dd75f843c9d0099", null ],
    [ "Eigen::internal::pstoreu< half >", "namespaceEigen_1_1internal.html#aba207b9a3c9e08bf21456ff6ea4e33ba", null ],
    [ "Eigen::internal::psub< Packet16h >", "namespaceEigen_1_1internal.html#ac996d634606a8af7fc4231b837575a61", null ],
    [ "Eigen::internal::psub< Packet32h >", "namespaceEigen_1_1internal.html#aec82ce0bb1a47a95e001084855de6710", null ],
    [ "Eigen::internal::psub< Packet8h >", "namespaceEigen_1_1internal.html#a7df93d316912425735f61cd6ca6ecb82", null ],
    [ "Eigen::internal::ptanh< Packet16h >", "namespaceEigen_1_1internal.html#a736b114f9ec0fdb70138037c40d04800", null ],
    [ "Eigen::internal::ptanh< Packet32h >", "namespaceEigen_1_1internal.html#a4604c96ed7ce38e9e95af8ac11629eb5", null ],
    [ "Eigen::internal::ptranspose", "namespaceEigen_1_1internal.html#a9392000ecb7edc08abc0c55a86ce0344", null ],
    [ "Eigen::internal::ptranspose", "namespaceEigen_1_1internal.html#a806c8198eb6487c12ce5ed45086936ee", null ],
    [ "Eigen::internal::ptrunc< Packet32h >", "namespaceEigen_1_1internal.html#afb701e138b2b8ece250a40b8afb57a95", null ],
    [ "Eigen::internal::pxor", "namespaceEigen_1_1internal.html#ab4b663e0f3e1ec3130346c77af9541dc", null ],
    [ "Eigen::internal::pzero", "namespaceEigen_1_1internal.html#a6887ca3eded6c7a786170ff60b5b6ca5", null ]
];