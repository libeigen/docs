var structEigen_1_1internal_1_1traits_3_01Reverse_3_01MatrixType_00_01Direction_01_4_01_4 =
[
    [ "MatrixTypeNested", "structEigen_1_1internal_1_1traits_3_01Reverse_3_01MatrixType_00_01Direction_01_4_01_4.html#a29a753d24c5bd3262a9b5aa1ae177e52", null ],
    [ "MatrixTypeNested_", "structEigen_1_1internal_1_1traits_3_01Reverse_3_01MatrixType_00_01Direction_01_4_01_4.html#a1610abf7b298e84df1789ff8d0f54a70", null ],
    [ "Scalar", "structEigen_1_1internal_1_1traits_3_01Reverse_3_01MatrixType_00_01Direction_01_4_01_4.html#ac5be0e9568d9607bcb0a7c1730b3d012", null ],
    [ "StorageKind", "structEigen_1_1internal_1_1traits_3_01Reverse_3_01MatrixType_00_01Direction_01_4_01_4.html#a1a9e576ce2d866a919ff6f333673fc14", null ],
    [ "XprKind", "structEigen_1_1internal_1_1traits_3_01Reverse_3_01MatrixType_00_01Direction_01_4_01_4.html#a661e935d2f1281c8872523d6d81dabfe", null ]
];