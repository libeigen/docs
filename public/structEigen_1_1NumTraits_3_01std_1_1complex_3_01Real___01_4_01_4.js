var structEigen_1_1NumTraits_3_01std_1_1complex_3_01Real___01_4_01_4 =
[
    [ "Literal", "structEigen_1_1NumTraits_3_01std_1_1complex_3_01Real___01_4_01_4.html#aaeeff13ecf9f0bedd55a94030cc27fc9", null ],
    [ "Real", "structEigen_1_1NumTraits_3_01std_1_1complex_3_01Real___01_4_01_4.html#a5630f9929809fb057da9738f16f631dc", null ],
    [ "digits10", "structEigen_1_1NumTraits_3_01std_1_1complex_3_01Real___01_4_01_4.html#ad5b0a5d614406ebc21057608b61a945c", null ],
    [ "dummy_precision", "structEigen_1_1NumTraits_3_01std_1_1complex_3_01Real___01_4_01_4.html#a5a76c6cf40bd7988dac01bf8ced501a7", null ],
    [ "epsilon", "structEigen_1_1NumTraits_3_01std_1_1complex_3_01Real___01_4_01_4.html#a030d5a798b353555c976b5a7a9d78ae0", null ],
    [ "max_digits10", "structEigen_1_1NumTraits_3_01std_1_1complex_3_01Real___01_4_01_4.html#a4ac2cb0f644c638e0bdb19ab2ffc7061", null ]
];