var Quaternion_8h =
[
    [ "Eigen::internal::quat_conj< Arch, Derived, Scalar >", "structEigen_1_1internal_1_1quat__conj.html", "structEigen_1_1internal_1_1quat__conj" ],
    [ "Eigen::internal::quat_product< Arch, Derived1, Derived2, Scalar >", "structEigen_1_1internal_1_1quat__product.html", "structEigen_1_1internal_1_1quat__product" ],
    [ "Eigen::internal::quaternionbase_assign_impl< Other, 3, 3 >", "structEigen_1_1internal_1_1quaternionbase__assign__impl_3_01Other_00_013_00_013_01_4.html", "structEigen_1_1internal_1_1quaternionbase__assign__impl_3_01Other_00_013_00_013_01_4" ],
    [ "Eigen::internal::quaternionbase_assign_impl< Other, 4, 1 >", "structEigen_1_1internal_1_1quaternionbase__assign__impl_3_01Other_00_014_00_011_01_4.html", "structEigen_1_1internal_1_1quaternionbase__assign__impl_3_01Other_00_014_00_011_01_4" ],
    [ "Eigen::internal::traits< Map< const Quaternion< Scalar_ >, Options_ > >", "structEigen_1_1internal_1_1traits_3_01Map_3_01const_01Quaternion_3_01Scalar___01_4_00_01Options___01_4_01_4.html", "structEigen_1_1internal_1_1traits_3_01Map_3_01const_01Quaternion_3_01Scalar___01_4_00_01Options___01_4_01_4" ],
    [ "Eigen::internal::traits< Map< Quaternion< Scalar_ >, Options_ > >", "structEigen_1_1internal_1_1traits_3_01Map_3_01Quaternion_3_01Scalar___01_4_00_01Options___01_4_01_4.html", "structEigen_1_1internal_1_1traits_3_01Map_3_01Quaternion_3_01Scalar___01_4_00_01Options___01_4_01_4" ],
    [ "Eigen::internal::traits< Quaternion< Scalar_, Options_ > >", "structEigen_1_1internal_1_1traits_3_01Quaternion_3_01Scalar___00_01Options___01_4_01_4.html", "structEigen_1_1internal_1_1traits_3_01Quaternion_3_01Scalar___00_01Options___01_4_01_4" ],
    [ "Eigen::Quaterniond", "group__Geometry__Module.html#ga5daab8e66aa480465000308455578830", null ],
    [ "Eigen::Quaternionf", "group__Geometry__Module.html#ga66aa915a26d698c60ed206818c3e4c9b", null ],
    [ "Eigen::QuaternionMapAlignedd", "group__Geometry__Module.html#ga4289f38cc6ecf302e07d2365abc6a902", null ],
    [ "Eigen::QuaternionMapAlignedf", "group__Geometry__Module.html#gadaf7f3ee984d9828ca94d66355f0b226", null ],
    [ "Eigen::QuaternionMapd", "group__Geometry__Module.html#ga89412d1dcf23537e5990dfb3089ace76", null ],
    [ "Eigen::QuaternionMapf", "group__Geometry__Module.html#ga867ff508ac860bdf7cab3b8a8fc1048d", null ]
];