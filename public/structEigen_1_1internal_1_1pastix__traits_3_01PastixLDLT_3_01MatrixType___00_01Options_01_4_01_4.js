var structEigen_1_1internal_1_1pastix__traits_3_01PastixLDLT_3_01MatrixType___00_01Options_01_4_01_4 =
[
    [ "MatrixType", "structEigen_1_1internal_1_1pastix__traits_3_01PastixLDLT_3_01MatrixType___00_01Options_01_4_01_4.html#a566d7a5307df727a7e53b6d183e468cc", null ],
    [ "RealScalar", "structEigen_1_1internal_1_1pastix__traits_3_01PastixLDLT_3_01MatrixType___00_01Options_01_4_01_4.html#a09e94cf2738f1ca9b41dc0539171ba9e", null ],
    [ "Scalar", "structEigen_1_1internal_1_1pastix__traits_3_01PastixLDLT_3_01MatrixType___00_01Options_01_4_01_4.html#aa2172027ba962c50a3ef7ff719379b89", null ],
    [ "StorageIndex", "structEigen_1_1internal_1_1pastix__traits_3_01PastixLDLT_3_01MatrixType___00_01Options_01_4_01_4.html#a333f517e5116b59cd920108c9bacb151", null ]
];