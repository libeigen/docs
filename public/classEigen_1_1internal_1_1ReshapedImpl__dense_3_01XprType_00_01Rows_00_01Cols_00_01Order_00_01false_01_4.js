var classEigen_1_1internal_1_1ReshapedImpl__dense_3_01XprType_00_01Rows_00_01Cols_00_01Order_00_01false_01_4 =
[
    [ "Base", "classEigen_1_1internal_1_1ReshapedImpl__dense_3_01XprType_00_01Rows_00_01Cols_00_01Order_00_01false_01_4.html#a677144c05a9fafcc383943b620664ffc", null ],
    [ "MatrixTypeNested", "classEigen_1_1internal_1_1ReshapedImpl__dense_3_01XprType_00_01Rows_00_01Cols_00_01Order_00_01false_01_4.html#a6be5af6abff8fbd0e3aba2c3d8507198", null ],
    [ "NestedExpression", "classEigen_1_1internal_1_1ReshapedImpl__dense_3_01XprType_00_01Rows_00_01Cols_00_01Order_00_01false_01_4.html#a10cd4764cba8a138b7d53c549ba45e80", null ],
    [ "ReshapedType", "classEigen_1_1internal_1_1ReshapedImpl__dense_3_01XprType_00_01Rows_00_01Cols_00_01Order_00_01false_01_4.html#a5a5876fa5ae211942f90653f51d72d53", null ],
    [ "ReshapedImpl_dense", "classEigen_1_1internal_1_1ReshapedImpl__dense_3_01XprType_00_01Rows_00_01Cols_00_01Order_00_01false_01_4.html#aac89463d0afb2ef928a35e1299756820", null ],
    [ "ReshapedImpl_dense", "classEigen_1_1internal_1_1ReshapedImpl__dense_3_01XprType_00_01Rows_00_01Cols_00_01Order_00_01false_01_4.html#a8d03550c01142d492c584cb8c947f796", null ],
    [ "cols", "classEigen_1_1internal_1_1ReshapedImpl__dense_3_01XprType_00_01Rows_00_01Cols_00_01Order_00_01false_01_4.html#acd9ff8002e9ae95ebedb53c605866a6e", null ],
    [ "data", "classEigen_1_1internal_1_1ReshapedImpl__dense_3_01XprType_00_01Rows_00_01Cols_00_01Order_00_01false_01_4.html#a4dfb32be858a91b47cee65705f327df8", null ],
    [ "innerStride", "classEigen_1_1internal_1_1ReshapedImpl__dense_3_01XprType_00_01Rows_00_01Cols_00_01Order_00_01false_01_4.html#a7352d19e0ab954607bede94781fa28c1", null ],
    [ "nestedExpression", "classEigen_1_1internal_1_1ReshapedImpl__dense_3_01XprType_00_01Rows_00_01Cols_00_01Order_00_01false_01_4.html#a1215b45de09782fe4f70035f8c31544a", null ],
    [ "nestedExpression", "classEigen_1_1internal_1_1ReshapedImpl__dense_3_01XprType_00_01Rows_00_01Cols_00_01Order_00_01false_01_4.html#a23b624a7bb2a8f7c8ce6a23c0991d5a9", null ],
    [ "outerStride", "classEigen_1_1internal_1_1ReshapedImpl__dense_3_01XprType_00_01Rows_00_01Cols_00_01Order_00_01false_01_4.html#ae516ea1a77248a7c102ed9ce9d49a18b", null ],
    [ "rows", "classEigen_1_1internal_1_1ReshapedImpl__dense_3_01XprType_00_01Rows_00_01Cols_00_01Order_00_01false_01_4.html#acd22b05bf56605e0dc1c8fca534815e8", null ],
    [ "m_cols", "classEigen_1_1internal_1_1ReshapedImpl__dense_3_01XprType_00_01Rows_00_01Cols_00_01Order_00_01false_01_4.html#a9f52d2053428791701ebdf67f68cf240", null ],
    [ "m_rows", "classEigen_1_1internal_1_1ReshapedImpl__dense_3_01XprType_00_01Rows_00_01Cols_00_01Order_00_01false_01_4.html#acdf9c337552f6711a5ea346845e2e863", null ],
    [ "m_xpr", "classEigen_1_1internal_1_1ReshapedImpl__dense_3_01XprType_00_01Rows_00_01Cols_00_01Order_00_01false_01_4.html#a862b3adb82bc603d43929f68c18a261a", null ]
];