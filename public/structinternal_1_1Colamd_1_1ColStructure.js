var structinternal_1_1Colamd_1_1ColStructure =
[
    [ "is_alive", "structinternal_1_1Colamd_1_1ColStructure.html#a0f5e31122c196d453144a3636e6ff8b6", null ],
    [ "is_dead", "structinternal_1_1Colamd_1_1ColStructure.html#a24a7a8dc0b862451b8e2894ee4ba4152", null ],
    [ "is_dead_principal", "structinternal_1_1Colamd_1_1ColStructure.html#a0fc5d3d3d33c4d413b1af9c640581732", null ],
    [ "kill_non_principal", "structinternal_1_1Colamd_1_1ColStructure.html#a3a9adcc3207638d5f2adee379b17b545", null ],
    [ "kill_principal", "structinternal_1_1Colamd_1_1ColStructure.html#a740eb9c001d52e399d1625637922d956", null ],
    [ "degree_next", "structinternal_1_1Colamd_1_1ColStructure.html#a8430e726e7770e66b139d897405ad6be", null ],
    [ "hash", "structinternal_1_1Colamd_1_1ColStructure.html#a4b0e47d2d733c41bace3fade69be3f5f", null ],
    [ "hash_next", "structinternal_1_1Colamd_1_1ColStructure.html#a7150e646dd5f4ce690d730fe24575852", null ],
    [ "headhash", "structinternal_1_1Colamd_1_1ColStructure.html#abea6ef4d2fe07802d7558a320b6dd2d2", null ],
    [ "length", "structinternal_1_1Colamd_1_1ColStructure.html#af5dcec994d79dbd8fce2d624b8319442", null ],
    [ "order", "structinternal_1_1Colamd_1_1ColStructure.html#a15fcc91a5838d4f1b84ab8dc6305a45a", null ],
    [ "parent", "structinternal_1_1Colamd_1_1ColStructure.html#a431a3b0b79d0af39354909ea44a2514a", null ],
    [ "prev", "structinternal_1_1Colamd_1_1ColStructure.html#a19d894cd4c27ff43af5fff6e7a4c5f27", null ],
    [ "score", "structinternal_1_1Colamd_1_1ColStructure.html#a0fcbd2d0d06756b10d471e77fc808a50", null ],
    [ "shared1", "structinternal_1_1Colamd_1_1ColStructure.html#a348381939a25391007aba326244f2a88", null ],
    [ "shared2", "structinternal_1_1Colamd_1_1ColStructure.html#a778b7ac8e2fa450de83a8a99175e93b1", null ],
    [ "shared3", "structinternal_1_1Colamd_1_1ColStructure.html#afd30454330514c412ce3bf117b26b7e1", null ],
    [ "shared4", "structinternal_1_1Colamd_1_1ColStructure.html#a9d407960af66063c8a9b50f1738bc9d2", null ],
    [ "start", "structinternal_1_1Colamd_1_1ColStructure.html#a2858e82a69f02e4577df3716f13217f5", null ],
    [ "thickness", "structinternal_1_1Colamd_1_1ColStructure.html#a64cce92c11ffb45bf381ecbed255da73", null ]
];