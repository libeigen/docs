var arch_2AVX_2MathFunctions_8h =
[
    [ "Eigen::internal::pfrexp", "namespaceEigen_1_1internal.html#a5a029d6108811252a01fc231543231e9", null ],
    [ "Eigen::internal::pfrexp", "namespaceEigen_1_1internal.html#a7a4a40e195920e01b31306b4a91f017b", null ],
    [ "Eigen::internal::pldexp", "namespaceEigen_1_1internal.html#ac5d10e9076edd614ca4ab3a4cc824653", null ],
    [ "Eigen::internal::pldexp", "namespaceEigen_1_1internal.html#a9a2440e4976811677b5a554471ede446", null ],
    [ "Eigen::internal::psqrt< Packet4d >", "namespaceEigen_1_1internal.html#aec4ed263f1380dbfe9aa48e9e227194e", null ],
    [ "Eigen::internal::psqrt< Packet8f >", "namespaceEigen_1_1internal.html#a83f0aa9e5025c5f52ce78c88af1b26ba", null ]
];