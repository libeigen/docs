var structEigen_1_1internal_1_1traits_3_01Block_3_01XprType___00_01BlockRows_00_01BlockCols_00_01InnerPanel___01_4_01_4 =
[
    [ "Scalar", "structEigen_1_1internal_1_1traits_3_01Block_3_01XprType___00_01BlockRows_00_01BlockCols_00_01InnerPanel___01_4_01_4.html#acea6903e2bee40e575ee8c48a75a05b5", null ],
    [ "StorageKind", "structEigen_1_1internal_1_1traits_3_01Block_3_01XprType___00_01BlockRows_00_01BlockCols_00_01InnerPanel___01_4_01_4.html#a04724cbd70229d604c162f0e47c8a1f1", null ],
    [ "XprKind", "structEigen_1_1internal_1_1traits_3_01Block_3_01XprType___00_01BlockRows_00_01BlockCols_00_01InnerPanel___01_4_01_4.html#aefaa945591916b95f9521c9e7c763197", null ],
    [ "XprTypeNested", "structEigen_1_1internal_1_1traits_3_01Block_3_01XprType___00_01BlockRows_00_01BlockCols_00_01InnerPanel___01_4_01_4.html#abdde1a47b703f3892ab5047203089c52", null ],
    [ "XprTypeNested_", "structEigen_1_1internal_1_1traits_3_01Block_3_01XprType___00_01BlockRows_00_01BlockCols_00_01InnerPanel___01_4_01_4.html#a3253b054f342b1a4e654b2be81e0e2f1", null ]
];