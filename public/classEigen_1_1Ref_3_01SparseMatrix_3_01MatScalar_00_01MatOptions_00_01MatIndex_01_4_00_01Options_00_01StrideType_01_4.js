var classEigen_1_1Ref_3_01SparseMatrix_3_01MatScalar_00_01MatOptions_00_01MatIndex_01_4_00_01Options_00_01StrideType_01_4 =
[
    [ "Base", "classEigen_1_1Ref.html#a6df268d7056480b5377f0295bc6c28ab", null ],
    [ "Base", "classEigen_1_1Ref_3_01SparseMatrix_3_01MatScalar_00_01MatOptions_00_01MatIndex_01_4_00_01Options_00_01StrideType_01_4.html#adfa4fced570eab7a4539df608f7078f2", null ],
    [ "PlainObjectType", "classEigen_1_1Ref_3_01SparseMatrix_3_01MatScalar_00_01MatOptions_00_01MatIndex_01_4_00_01Options_00_01StrideType_01_4.html#af03b2b334d36bd33214067c20d3d8db8", null ],
    [ "Traits", "classEigen_1_1Ref.html#a49325e714d6d855ac077a67f11d535a1", null ],
    [ "Traits", "classEigen_1_1Ref_3_01SparseMatrix_3_01MatScalar_00_01MatOptions_00_01MatIndex_01_4_00_01Options_00_01StrideType_01_4.html#ae3a136386e9fbc04930b364b573a7564", null ],
    [ "Ref", "classEigen_1_1Ref_3_01SparseMatrix_3_01MatScalar_00_01MatOptions_00_01MatIndex_01_4_00_01Options_00_01StrideType_01_4.html#a65d916aa4ef680ca5082954f9b7a4e9f", null ],
    [ "Ref", "classEigen_1_1Ref_3_01SparseMatrix_3_01MatScalar_00_01MatOptions_00_01MatIndex_01_4_00_01Options_00_01StrideType_01_4.html#a56260a4a57a5ca4f6de35e75298e5c78", null ],
    [ "Ref", "classEigen_1_1Ref_3_01SparseMatrix_3_01MatScalar_00_01MatOptions_00_01MatIndex_01_4_00_01Options_00_01StrideType_01_4.html#a6e5f2a6f091c8a9b1f35c4838e4e721b", null ],
    [ "Ref", "classEigen_1_1Ref_3_01SparseMatrix_3_01MatScalar_00_01MatOptions_00_01MatIndex_01_4_00_01Options_00_01StrideType_01_4.html#a1f8c47768de52de012f830eb7e66d501", null ],
    [ "Ref", "classEigen_1_1Ref_3_01SparseMatrix_3_01MatScalar_00_01MatOptions_00_01MatIndex_01_4_00_01Options_00_01StrideType_01_4.html#a0e87d2d88da89611227a55c0a689905a", null ],
    [ "Ref", "classEigen_1_1Ref.html#afb7f834c343aba20adab879b344a629b", null ],
    [ "Ref", "classEigen_1_1Ref.html#a32cf9dede3293eecbee94b544c190dfc", null ],
    [ "Ref", "classEigen_1_1Ref.html#acf91b683784c47aabe9100f4a41cdea5", null ]
];