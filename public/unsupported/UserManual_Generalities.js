var UserManual_Generalities =
[
    [ "Writing Functions Taking Eigen Types as Parameters", "../TopicFunctionTakingEigenTypes.html", null ],
    [ "Preprocessor directives", "../TopicPreprocessorDirectives.html", null ],
    [ "Assertions", "../TopicAssertions.html", null ],
    [ "Eigen and multi-threading", "../TopicMultiThreading.html", null ],
    [ "Using BLAS/LAPACK from Eigen", "../TopicUsingBlasLapack.html", null ],
    [ "Using Intel® MKL from Eigen", "../TopicUsingIntelMKL.html", null ],
    [ "Using Eigen in CUDA kernels", "../TopicCUDA.html", null ],
    [ "Common pitfalls", "../TopicPitfalls.html", null ],
    [ "The template and typename keywords in C++", "../TopicTemplateKeyword.html", null ],
    [ "Understanding Eigen", "../UserManual_UnderstandingEigen.html", "UserManual_UnderstandingEigen" ],
    [ "Using Eigen in CMake Projects", "../TopicCMakeGuide.html", null ]
];