var UserManual_CustomizingEigen =
[
    [ "Extending MatrixBase (and other classes)", "../TopicCustomizing_Plugins.html", null ],
    [ "Inheriting from Matrix", "../TopicCustomizing_InheritingMatrix.html", null ],
    [ "Using custom scalar types", "../TopicCustomizing_CustomScalar.html", null ],
    [ "Matrix manipulation via nullary-expressions", "../TopicCustomizing_NullaryExpr.html", null ],
    [ "Adding a new expression type", "../TopicNewExpressionType.html", null ]
];