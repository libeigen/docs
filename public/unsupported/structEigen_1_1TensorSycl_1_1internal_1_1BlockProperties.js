var structEigen_1_1TensorSycl_1_1internal_1_1BlockProperties =
[
    [ "OutScalar", "structEigen_1_1TensorSycl_1_1internal_1_1BlockProperties.html#a76bbf20a8c24a38f079ad926b65e747f", null ],
    [ "OutType", "structEigen_1_1TensorSycl_1_1internal_1_1BlockProperties.html#a4b47fcb1b9af29b6d4153b0712b85acd", null ],
    [ "c_stride", "structEigen_1_1TensorSycl_1_1internal_1_1BlockProperties.html#a1d8d0aa45d8c5d1c33acb2c66031a210", null ],
    [ "elements_per_access", "structEigen_1_1TensorSycl_1_1internal_1_1BlockProperties.html#a2f721f8a4d26c833042f5993944603af", null ],
    [ "is_coalesced_layout", "structEigen_1_1TensorSycl_1_1internal_1_1BlockProperties.html#a563f2a016a0ae4e2934ad25748c87ba5", null ],
    [ "is_rhs", "structEigen_1_1TensorSycl_1_1internal_1_1BlockProperties.html#a7038927b38c84fc162392f392a1d32bb", null ],
    [ "nc_stride", "structEigen_1_1TensorSycl_1_1internal_1_1BlockProperties.html#aa2cf6fcc0859cd7a78f64f6b576bb93b", null ],
    [ "packet_load", "structEigen_1_1TensorSycl_1_1internal_1_1BlockProperties.html#a79ceafcd1a3714747e86bdc32eb44b3b", null ]
];