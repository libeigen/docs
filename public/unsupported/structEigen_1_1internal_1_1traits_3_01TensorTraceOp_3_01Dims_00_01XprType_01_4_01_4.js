var structEigen_1_1internal_1_1traits_3_01TensorTraceOp_3_01Dims_00_01XprType_01_4_01_4 =
[
    [ "Index", "structEigen_1_1internal_1_1traits_3_01TensorTraceOp_3_01Dims_00_01XprType_01_4_01_4.html#afaa872dc1b48afee78cb54664dea73a4", null ],
    [ "Nested", "structEigen_1_1internal_1_1traits_3_01TensorTraceOp_3_01Dims_00_01XprType_01_4_01_4.html#a57826f22e95c97276552956c1c541e1b", null ],
    [ "Nested_", "structEigen_1_1internal_1_1traits_3_01TensorTraceOp_3_01Dims_00_01XprType_01_4_01_4.html#a37ced4f2be3e5b97029f3f8f7a3a2c36", null ],
    [ "Scalar", "structEigen_1_1internal_1_1traits_3_01TensorTraceOp_3_01Dims_00_01XprType_01_4_01_4.html#a3945fa34978b159cecee653528fde1ba", null ],
    [ "StorageKind", "structEigen_1_1internal_1_1traits_3_01TensorTraceOp_3_01Dims_00_01XprType_01_4_01_4.html#a7b066e3c19a048ce71c9087f50ccbda3", null ],
    [ "XprTraits", "structEigen_1_1internal_1_1traits_3_01TensorTraceOp_3_01Dims_00_01XprType_01_4_01_4.html#af9234bd001ce6c19ae8bb3d8b347b029", null ],
    [ "Layout", "structEigen_1_1internal_1_1traits_3_01TensorTraceOp_3_01Dims_00_01XprType_01_4_01_4.html#a320deb06d7124da8962dd37f8ba2ab10", null ],
    [ "NumDimensions", "structEigen_1_1internal_1_1traits_3_01TensorTraceOp_3_01Dims_00_01XprType_01_4_01_4.html#a2878857e423c379d9ca7f5cdd52a1dea", null ]
];