var structEigen_1_1internal_1_1group__theory_1_1dimino__first__step__elements =
[
    [ "first_generator", "structEigen_1_1internal_1_1group__theory_1_1dimino__first__step__elements.html#a6ddd9e313c93a200234882395fbe320e", null ],
    [ "generators_done", "structEigen_1_1internal_1_1group__theory_1_1dimino__first__step__elements.html#ac308c5e3fd190ecc41ac6d3d9e55d268", null ],
    [ "helper", "structEigen_1_1internal_1_1group__theory_1_1dimino__first__step__elements.html#a3c48437f651052792b47a08e45590b54", null ],
    [ "next_generators", "structEigen_1_1internal_1_1group__theory_1_1dimino__first__step__elements.html#add0c1d545f7aa928965c413e478a920b", null ],
    [ "type", "structEigen_1_1internal_1_1group__theory_1_1dimino__first__step__elements.html#a0680cf4fd6fb9846b17aa2eddd775c5c", null ],
    [ "global_flags", "structEigen_1_1internal_1_1group__theory_1_1dimino__first__step__elements.html#a80983f85881a15eaa401cedee1436990", null ]
];