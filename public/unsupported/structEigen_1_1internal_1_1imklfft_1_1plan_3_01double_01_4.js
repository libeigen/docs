var structEigen_1_1internal_1_1imklfft_1_1plan_3_01double_01_4 =
[
    [ "complex_type", "structEigen_1_1internal_1_1imklfft_1_1plan_3_01double_01_4.html#a43eb83747513b33fe42ead3ccfc34e02", null ],
    [ "scalar_type", "structEigen_1_1internal_1_1imklfft_1_1plan_3_01double_01_4.html#aec52bbd95c0392c0f33092fb3b6155de", null ],
    [ "plan", "structEigen_1_1internal_1_1imklfft_1_1plan_3_01double_01_4.html#a7fa9019b57c0f4d7a24c2742cb6a8ee8", null ],
    [ "forward", "structEigen_1_1internal_1_1imklfft_1_1plan_3_01double_01_4.html#ad30de0edb5e65d69889ef8288033c739", null ],
    [ "forward", "structEigen_1_1internal_1_1imklfft_1_1plan_3_01double_01_4.html#adf839cf1fb6dcd1ba4f5ba002304b096", null ],
    [ "forward2", "structEigen_1_1internal_1_1imklfft_1_1plan_3_01double_01_4.html#ac6b460707ea986e018e9e833ae23faa8", null ],
    [ "inverse", "structEigen_1_1internal_1_1imklfft_1_1plan_3_01double_01_4.html#a80f6f4af89d8bd66f799b4025720a30f", null ],
    [ "inverse", "structEigen_1_1internal_1_1imklfft_1_1plan_3_01double_01_4.html#a6110a59b6c80885a1872d04105f48343", null ],
    [ "inverse2", "structEigen_1_1internal_1_1imklfft_1_1plan_3_01double_01_4.html#a0ad9bd2692e7495abbd18156deff967e", null ],
    [ "m_plan", "structEigen_1_1internal_1_1imklfft_1_1plan_3_01double_01_4.html#a45745758527894d53e48474d03c7cbf5", null ],
    [ "precision", "structEigen_1_1internal_1_1imklfft_1_1plan_3_01double_01_4.html#a2ac7d2ac0f71b8f73fd62aed8320069c", null ]
];