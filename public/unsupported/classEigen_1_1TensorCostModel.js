var classEigen_1_1TensorCostModel =
[
    [ "numThreads", "classEigen_1_1TensorCostModel.html#aac2795828fcb03337b0781b274c09192", null ],
    [ "taskSize", "classEigen_1_1TensorCostModel.html#a862939a3362193e4f9e97346cc43f192", null ],
    [ "totalCost", "classEigen_1_1TensorCostModel.html#a4e3a9ce74750dd6dae1dd603736e27a4", null ],
    [ "kDeviceCyclesPerComputeCycle", "classEigen_1_1TensorCostModel.html#a72c9fb1313ef5bfbd248083ca7027230", null ],
    [ "kPerThreadCycles", "classEigen_1_1TensorCostModel.html#a9a8d747099b9f03478ceef1bce63563e", null ],
    [ "kStartupCycles", "classEigen_1_1TensorCostModel.html#aa0ee6d42a164ac5ad26d0a5a3bb79400", null ],
    [ "kTaskSize", "classEigen_1_1TensorCostModel.html#af33afbe1c48dd9cccd3b01cfc3b73d5c", null ]
];