var structEigen_1_1internal_1_1traits_3_01TensorCwiseNullaryOp_3_01NullaryOp_00_01XprType_01_4_01_4 =
[
    [ "PointerType", "structEigen_1_1internal_1_1traits_3_01TensorCwiseNullaryOp_3_01NullaryOp_00_01XprType_01_4_01_4.html#a5ef093c86c090d7637b0a693ded19c84", null ],
    [ "Scalar", "structEigen_1_1internal_1_1traits_3_01TensorCwiseNullaryOp_3_01NullaryOp_00_01XprType_01_4_01_4.html#a6c095bf88419b5205f8d2e657b10f0cf", null ],
    [ "XprTraits", "structEigen_1_1internal_1_1traits_3_01TensorCwiseNullaryOp_3_01NullaryOp_00_01XprType_01_4_01_4.html#ae4ef2d28b006d50fd199a2376d1826f4", null ],
    [ "XprTypeNested", "structEigen_1_1internal_1_1traits_3_01TensorCwiseNullaryOp_3_01NullaryOp_00_01XprType_01_4_01_4.html#a3d927edad5fd802aac140468f6f52b82", null ],
    [ "XprTypeNested_", "structEigen_1_1internal_1_1traits_3_01TensorCwiseNullaryOp_3_01NullaryOp_00_01XprType_01_4_01_4.html#a95788b12430eb3c299eb4f2479ea893d", null ],
    [ "Layout", "structEigen_1_1internal_1_1traits_3_01TensorCwiseNullaryOp_3_01NullaryOp_00_01XprType_01_4_01_4.html#a4cd36af61753f0d22990ab3148448661", null ],
    [ "NumDimensions", "structEigen_1_1internal_1_1traits_3_01TensorCwiseNullaryOp_3_01NullaryOp_00_01XprType_01_4_01_4.html#a4e6a6c258775e177d4e3e752500bbdc6", null ]
];