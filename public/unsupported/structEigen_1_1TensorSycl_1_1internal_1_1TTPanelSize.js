var structEigen_1_1TensorSycl_1_1internal_1_1TTPanelSize =
[
    [ "BC", "structEigen_1_1TensorSycl_1_1internal_1_1TTPanelSize.html#acfd745cc9b9a6600e88698c446fe2165", null ],
    [ "DoubleBuffer", "structEigen_1_1TensorSycl_1_1internal_1_1TTPanelSize.html#a55647f2ea471131727cf64c74c84f445", null ],
    [ "LoadPerThreadLhs", "structEigen_1_1TensorSycl_1_1internal_1_1TTPanelSize.html#abd8c6bc4bd593018f3f9cc618b5be521", null ],
    [ "LoadPerThreadRhs", "structEigen_1_1TensorSycl_1_1internal_1_1TTPanelSize.html#ac95557c20722ad270363badd6bb0e012", null ],
    [ "LocalThreadSizeM", "structEigen_1_1TensorSycl_1_1internal_1_1TTPanelSize.html#a8acb4ccdad88b4a6c5dc42e68d404c6d", null ],
    [ "LocalThreadSizeN", "structEigen_1_1TensorSycl_1_1internal_1_1TTPanelSize.html#a9004d3f55c0a2a0e73cccc7903eec37e", null ],
    [ "TileSizeDimK", "structEigen_1_1TensorSycl_1_1internal_1_1TTPanelSize.html#afc2eb8005717a5b331fe54843abdf144", null ],
    [ "TileSizeDimM", "structEigen_1_1TensorSycl_1_1internal_1_1TTPanelSize.html#a95cfb48f44d6430c86673a274ddc3268", null ],
    [ "TileSizeDimN", "structEigen_1_1TensorSycl_1_1internal_1_1TTPanelSize.html#ad5310816490531aa1c77bfdd8e54da93", null ],
    [ "WorkLoadPerThreadM", "structEigen_1_1TensorSycl_1_1internal_1_1TTPanelSize.html#a4e0f2a7179b2965263120ab82eab1eab", null ],
    [ "WorkLoadPerThreadN", "structEigen_1_1TensorSycl_1_1internal_1_1TTPanelSize.html#a28202eff53323fc7c9de78f052e7d3ac", null ]
];