var dir_32cf1c807a6a54b74f3595d5e0573479 =
[
    [ "AutoDiff", "dir_09a635ca2c970f5bd168289771e5408f.html", "dir_09a635ca2c970f5bd168289771e5408f" ],
    [ "BVH", "dir_99e6683d581f55ff80743049d7037541.html", "dir_99e6683d581f55ff80743049d7037541" ],
    [ "Eigenvalues", "dir_96f727b062bbc416bae671230b6e4398.html", "dir_96f727b062bbc416bae671230b6e4398" ],
    [ "EulerAngles", "dir_0e18fb9d4675d169ae2639bf867b827e.html", "dir_0e18fb9d4675d169ae2639bf867b827e" ],
    [ "FFT", "dir_5b79a6dbeca9158cca2139edf55a2af0.html", "dir_5b79a6dbeca9158cca2139edf55a2af0" ],
    [ "IterativeSolvers", "dir_130d1cd5b99ef5e7a6b6686ed40fd7ec.html", "dir_130d1cd5b99ef5e7a6b6686ed40fd7ec" ],
    [ "KroneckerProduct", "dir_450de5fb628dd7e4b8a28388e43ec4c2.html", "dir_450de5fb628dd7e4b8a28388e43ec4c2" ],
    [ "LevenbergMarquardt", "dir_e64d74fdc1defde03efbbf5c04b4c882.html", "dir_e64d74fdc1defde03efbbf5c04b4c882" ],
    [ "MatrixFunctions", "dir_61fe7d7028d809a87436fb7749332b49.html", "dir_61fe7d7028d809a87436fb7749332b49" ],
    [ "NonLinearOptimization", "dir_16abe625072346cf0edf99a4f34aac9f.html", "dir_16abe625072346cf0edf99a4f34aac9f" ],
    [ "NumericalDiff", "dir_071d43020807c4dffc506e8b6b234af6.html", "dir_071d43020807c4dffc506e8b6b234af6" ],
    [ "Polynomials", "dir_d5537d6a2231218fc2b9b9ed6c07a248.html", "dir_d5537d6a2231218fc2b9b9ed6c07a248" ],
    [ "SparseExtra", "dir_e0583bd66b2cd0b7e4021151b6455aa2.html", "dir_e0583bd66b2cd0b7e4021151b6455aa2" ],
    [ "SpecialFunctions", "dir_d78a08284db3b665c8eab09945e5dbc6.html", "dir_d78a08284db3b665c8eab09945e5dbc6" ],
    [ "Splines", "dir_89c75b97f2f3ecce2251198eb10031dd.html", "dir_89c75b97f2f3ecce2251198eb10031dd" ]
];