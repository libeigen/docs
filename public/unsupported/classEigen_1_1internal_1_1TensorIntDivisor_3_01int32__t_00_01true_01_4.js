var classEigen_1_1internal_1_1TensorIntDivisor_3_01int32__t_00_01true_01_4 =
[
    [ "UnsignedType", "structEigen_1_1internal_1_1TensorIntDivisor.html#ad22cd4c5102b07bb60d108556a06aa6c", null ],
    [ "TensorIntDivisor", "classEigen_1_1internal_1_1TensorIntDivisor_3_01int32__t_00_01true_01_4.html#a071da36ee4f392b8d9f793a33f589eba", null ],
    [ "TensorIntDivisor", "classEigen_1_1internal_1_1TensorIntDivisor_3_01int32__t_00_01true_01_4.html#a52762b6366ac02552800572905d5095a", null ],
    [ "TensorIntDivisor", "structEigen_1_1internal_1_1TensorIntDivisor.html#ad0db384a81190d6059617aae98ce97e5", null ],
    [ "TensorIntDivisor", "structEigen_1_1internal_1_1TensorIntDivisor.html#abbae4b4373f1d5e4e873da3f24b60ad6", null ],
    [ "calcMagic", "classEigen_1_1internal_1_1TensorIntDivisor_3_01int32__t_00_01true_01_4.html#a50940fad242a30f74531140caad870ca", null ],
    [ "divide", "classEigen_1_1internal_1_1TensorIntDivisor_3_01int32__t_00_01true_01_4.html#a940700fe1453b8703ec7b1ae6bb56f13", null ],
    [ "divide", "structEigen_1_1internal_1_1TensorIntDivisor.html#ae80426f3d033d0eb00959cfd3056db9a", null ],
    [ "magic", "classEigen_1_1internal_1_1TensorIntDivisor_3_01int32__t_00_01true_01_4.html#af88b344c024ffe1bc49c99fdea72438a", null ],
    [ "multiplier", "structEigen_1_1internal_1_1TensorIntDivisor.html#ad50c9e64517dcd54a150d2d84f39cf4d", null ],
    [ "shift", "classEigen_1_1internal_1_1TensorIntDivisor_3_01int32__t_00_01true_01_4.html#ae1a2b3930ffc5dd40c65d1ac73d69982", null ],
    [ "shift1", "structEigen_1_1internal_1_1TensorIntDivisor.html#abd6b297ae89ca6ffac0db95043bb4a3d", null ],
    [ "shift2", "structEigen_1_1internal_1_1TensorIntDivisor.html#a1e0df9c5187aa484179e9f265fa8b37f", null ]
];