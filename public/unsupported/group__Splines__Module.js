var group__Splines__Module =
[
    [ "Eigen::Spline< Scalar, Dim, Degree >", "classEigen_1_1Spline.html", [
      [ "BasisDerivativeType", "classEigen_1_1Spline.html#a0d6477c46ad41dc152d8941d1412b45b", null ],
      [ "BasisVectorType", "classEigen_1_1Spline.html#af5d096aded151021dc604452bc814b53", null ],
      [ "ControlPointVectorType", "classEigen_1_1Spline.html#a9e250890b6e3b160a1aa3acc3dc27861", null ],
      [ "KnotVectorType", "classEigen_1_1Spline.html#afd3ce4a146f805551f0312fe17eb0dab", null ],
      [ "ParameterVectorType", "classEigen_1_1Spline.html#afac3b0d185948e4879bbcb3861f2c4a8", null ],
      [ "PointType", "classEigen_1_1Spline.html#ade5a148cb91bf80024ad2952c4c4224b", null ],
      [ "Scalar", "classEigen_1_1Spline.html#ab443da0c5f94b50ae4426f81c275d243", null ],
      [ "Spline", "classEigen_1_1Spline.html#a0d6ad9f4c9500555f49ec95fa250f997", null ],
      [ "Spline", "classEigen_1_1Spline.html#a5e2cdeb3c0733126556ecccadf678940", null ],
      [ "Spline", "classEigen_1_1Spline.html#ab892a522ba84371613456b5b159199ed", null ],
      [ "BasisFunctionDerivatives", "classEigen_1_1Spline.html#ac42ec47147bdd9578d55928d0988ae2c", null ],
      [ "basisFunctionDerivatives", "classEigen_1_1Spline.html#a4d82b3272ef6368d6b12ad366ffd7de5", null ],
      [ "basisFunctionDerivatives", "classEigen_1_1Spline.html#a6853458a52a21d5d958a0cc34e482505", null ],
      [ "BasisFunctions", "classEigen_1_1Spline.html#ad73da31fc6a112763c392e02422f5c48", null ],
      [ "basisFunctions", "classEigen_1_1Spline.html#aeaec16e89270af47114e685289ff10a1", null ],
      [ "ctrls", "classEigen_1_1Spline.html#af3dcc53d44d3eced2b451e523d80c5ca", null ],
      [ "degree", "classEigen_1_1Spline.html#a099e9ac2cfbba7fc880e1e89081bd3cf", null ],
      [ "derivatives", "classEigen_1_1Spline.html#adbd002b6fc849c7b089ed56d1408ab10", null ],
      [ "derivatives", "classEigen_1_1Spline.html#a63005296a2f5bd02b24569ae6340f6be", null ],
      [ "knots", "classEigen_1_1Spline.html#ae39dc894ebd2ddff151958145164eaaf", null ],
      [ "operator()", "classEigen_1_1Spline.html#a15d8f85af634fc85d640369189d4083e", null ],
      [ "Span", "classEigen_1_1Spline.html#a68fbc57ca471069789801fea000a0c90", null ],
      [ "span", "classEigen_1_1Spline.html#a8fa2ad1f7ddf1eaf83464a87cce49e9d", null ]
    ] ],
    [ "Eigen::SplineFitting< SplineType >", "structEigen_1_1SplineFitting.html", [
      [ "Interpolate", "structEigen_1_1SplineFitting.html#adc80b6f0dd0dbbea28130fb254626874", null ],
      [ "Interpolate", "structEigen_1_1SplineFitting.html#af08185c8b635283f7c76efe91576cc83", null ],
      [ "InterpolateWithDerivatives", "structEigen_1_1SplineFitting.html#a7bd937fdcfa168dbdc27932886a4da9f", null ],
      [ "InterpolateWithDerivatives", "structEigen_1_1SplineFitting.html#a0317c97f2b57ccf5dcf077409d51e54d", null ]
    ] ],
    [ "Eigen::SplineTraits< Spline< Scalar_, Dim_, Degree_ >, _DerivativeOrder >", "structEigen_1_1SplineTraits_3_01Spline_3_01Scalar___00_01Dim___00_01Degree___01_4_00_01__DerivativeOrder_01_4.html", [
      [ "BasisDerivativeType", "structEigen_1_1SplineTraits_3_01Spline_3_01Scalar___00_01Dim___00_01Degree___01_4_00_01__DerivativeOrder_01_4.html#ada7fa50fe27674678be060570b990682", null ],
      [ "DerivativeType", "structEigen_1_1SplineTraits_3_01Spline_3_01Scalar___00_01Dim___00_01Degree___01_4_00_01__DerivativeOrder_01_4.html#ae0061d4441e865df3b52a0ed947bfa58", null ]
    ] ],
    [ "Eigen::SplineTraits< Spline< Scalar_, Dim_, Degree_ >, Dynamic >", "structEigen_1_1SplineTraits_3_01Spline_3_01Scalar___00_01Dim___00_01Degree___01_4_00_01Dynamic_01_4.html", [
      [ "BasisDerivativeType", "structEigen_1_1SplineTraits_3_01Spline_3_01Scalar___00_01Dim___00_01Degree___01_4_00_01Dynamic_01_4.html#a18f74648544d3ec76af5764b5ae44032", null ],
      [ "BasisVectorType", "structEigen_1_1SplineTraits_3_01Spline_3_01Scalar___00_01Dim___00_01Degree___01_4_00_01Dynamic_01_4.html#a3b3dbdb68634c2d1b4e9080e018687eb", null ],
      [ "ControlPointVectorType", "structEigen_1_1SplineTraits_3_01Spline_3_01Scalar___00_01Dim___00_01Degree___01_4_00_01Dynamic_01_4.html#a6dff54ef43fb4080b3794290b38d3efe", null ],
      [ "DerivativeType", "structEigen_1_1SplineTraits_3_01Spline_3_01Scalar___00_01Dim___00_01Degree___01_4_00_01Dynamic_01_4.html#ac538fb6f640140ecc93f207cbc62950b", null ],
      [ "KnotVectorType", "structEigen_1_1SplineTraits_3_01Spline_3_01Scalar___00_01Dim___00_01Degree___01_4_00_01Dynamic_01_4.html#af75a3b2dd9bca842ea12dedefcdf3e39", null ],
      [ "ParameterVectorType", "structEigen_1_1SplineTraits_3_01Spline_3_01Scalar___00_01Dim___00_01Degree___01_4_00_01Dynamic_01_4.html#a213611723985e7cd56415b553f19bfad", null ],
      [ "PointType", "structEigen_1_1SplineTraits_3_01Spline_3_01Scalar___00_01Dim___00_01Degree___01_4_00_01Dynamic_01_4.html#a2ffdcf626600bf9a45c4a5d7ac11a04c", null ],
      [ "Scalar", "structEigen_1_1SplineTraits_3_01Spline_3_01Scalar___00_01Dim___00_01Degree___01_4_00_01Dynamic_01_4.html#ac7ad5c8cce020be6923054f125e1c050", null ]
    ] ],
    [ "Eigen::ChordLengths", "group__Splines__Module.html#ga1b4cbde5d98411405871accf877552d2", null ],
    [ "Eigen::KnotAveraging", "group__Splines__Module.html#ga9474da5ed68bbd9a6788a999330416d6", null ],
    [ "Eigen::KnotAveragingWithDerivatives", "group__Splines__Module.html#gae10a6f9b6ab7fb400a2526b6382c533b", null ]
];