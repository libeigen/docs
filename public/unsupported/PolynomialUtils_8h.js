var PolynomialUtils_8h =
[
    [ "Eigen::cauchy_max_bound", "group__Polynomials__Module.html#gaa6dcbc8ff39a5916783d9b2d25af90d1", null ],
    [ "Eigen::cauchy_min_bound", "group__Polynomials__Module.html#gab9ddb420f8dd9f00bfea8df504cceae5", null ],
    [ "Eigen::poly_eval", "group__Polynomials__Module.html#gadb64ffddaa9e83634e3ab0e3fd3664f5", null ],
    [ "Eigen::poly_eval_horner", "group__Polynomials__Module.html#gaadbf059bc28ce1cf94c57c1454633d40", null ],
    [ "Eigen::roots_to_monicPolynomial", "group__Polynomials__Module.html#gafbc3648f7ef67db3d5d04454fc1257fd", null ]
];