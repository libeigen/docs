var structEigen_1_1internal_1_1traits_3_01BlockSparseTimeDenseProduct_3_01BlockSparseMatrixT_00_01VecType_01_4_01_4 =
[
    [ "Index", "structEigen_1_1internal_1_1traits_3_01BlockSparseTimeDenseProduct_3_01BlockSparseMatrixT_00_01VecType_01_4_01_4.html#a5f48cf253639e03234fded0ee6c53955", null ],
    [ "Scalar", "structEigen_1_1internal_1_1traits_3_01BlockSparseTimeDenseProduct_3_01BlockSparseMatrixT_00_01VecType_01_4_01_4.html#ad88be2f430459cc510fe058037f755f5", null ],
    [ "StorageKind", "structEigen_1_1internal_1_1traits_3_01BlockSparseTimeDenseProduct_3_01BlockSparseMatrixT_00_01VecType_01_4_01_4.html#a387208e4d6d296d54341f012ec0455bd", null ],
    [ "XprKind", "structEigen_1_1internal_1_1traits_3_01BlockSparseTimeDenseProduct_3_01BlockSparseMatrixT_00_01VecType_01_4_01_4.html#a8f4b16d01b6febf4bbd6c8f1a4c9fe29", null ]
];