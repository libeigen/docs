var structEigen_1_1internal_1_1group__theory_1_1dimino__add__generator =
[
    [ "_helper", "structEigen_1_1internal_1_1group__theory_1_1dimino__add__generator.html#afe0cbd616ff11cc5d7144454985ac6c3", null ],
    [ "multiplied_elements", "structEigen_1_1internal_1_1group__theory_1_1dimino__add__generator.html#a75eb0e8b924e2e3bf734c75d43b825ee", null ],
    [ "new_elements", "structEigen_1_1internal_1_1group__theory_1_1dimino__add__generator.html#aa9203ba4903591d0fbe3680ff03f40aa", null ],
    [ "type", "structEigen_1_1internal_1_1group__theory_1_1dimino__add__generator.html#a5fa10aaef000f60fb08eeec36e97183b", null ],
    [ "global_flags", "structEigen_1_1internal_1_1group__theory_1_1dimino__add__generator.html#a7dcd08b9ae7b1254005d79af1363b535", null ],
    [ "rep_pos", "structEigen_1_1internal_1_1group__theory_1_1dimino__add__generator.html#a6ba640074f77cbb30bb5261acececf24", null ]
];