var SplineFitting_8h =
[
    [ "Eigen::ChordLengths", "group__Splines__Module.html#ga1b4cbde5d98411405871accf877552d2", null ],
    [ "Eigen::KnotAveraging", "group__Splines__Module.html#ga9474da5ed68bbd9a6788a999330416d6", null ],
    [ "Eigen::KnotAveragingWithDerivatives", "group__Splines__Module.html#gae10a6f9b6ab7fb400a2526b6382c533b", null ]
];