var structEigen_1_1internal_1_1unary__evaluator_3_01CoherentPadOp_3_01ArgType_00_01SizeAtCompileTime_01_4_01_4 =
[
    [ "ArgTypeNested", "structEigen_1_1internal_1_1unary__evaluator_3_01CoherentPadOp_3_01ArgType_00_01SizeAtCompileTime_01_4_01_4.html#a01a68ef054d3880c01d7a70ac19a1ebd", null ],
    [ "ArgTypeNestedCleaned", "structEigen_1_1internal_1_1unary__evaluator_3_01CoherentPadOp_3_01ArgType_00_01SizeAtCompileTime_01_4_01_4.html#a67529e56940837a94a14a82f1ee8c65a", null ],
    [ "CoeffReturnType", "structEigen_1_1internal_1_1unary__evaluator_3_01CoherentPadOp_3_01ArgType_00_01SizeAtCompileTime_01_4_01_4.html#ada3e235f454c5762978549e50c0e713b", null ],
    [ "XprType", "structEigen_1_1internal_1_1unary__evaluator_3_01CoherentPadOp_3_01ArgType_00_01SizeAtCompileTime_01_4_01_4.html#af260be2418af2120ba45dbadcf1bcb97", null ],
    [ "unary_evaluator", "structEigen_1_1internal_1_1unary__evaluator_3_01CoherentPadOp_3_01ArgType_00_01SizeAtCompileTime_01_4_01_4.html#a92f741545ee32b4b32e01c4d516cae67", null ],
    [ "coeff", "structEigen_1_1internal_1_1unary__evaluator_3_01CoherentPadOp_3_01ArgType_00_01SizeAtCompileTime_01_4_01_4.html#ab1f1defdc79b6213886e7e02e667b670", null ],
    [ "coeff", "structEigen_1_1internal_1_1unary__evaluator_3_01CoherentPadOp_3_01ArgType_00_01SizeAtCompileTime_01_4_01_4.html#afd06827b8201b4338174c0add3587735", null ],
    [ "packet", "structEigen_1_1internal_1_1unary__evaluator_3_01CoherentPadOp_3_01ArgType_00_01SizeAtCompileTime_01_4_01_4.html#a6d9cef99093fd3c3f5a376f8b749dcbe", null ],
    [ "packet", "structEigen_1_1internal_1_1unary__evaluator_3_01CoherentPadOp_3_01ArgType_00_01SizeAtCompileTime_01_4_01_4.html#afdf7d4fb40dd611912c891210456d9b0", null ],
    [ "m_arg", "structEigen_1_1internal_1_1unary__evaluator_3_01CoherentPadOp_3_01ArgType_00_01SizeAtCompileTime_01_4_01_4.html#a1bf0abef0fa93bd99237f68b66ad290e", null ],
    [ "m_argImpl", "structEigen_1_1internal_1_1unary__evaluator_3_01CoherentPadOp_3_01ArgType_00_01SizeAtCompileTime_01_4_01_4.html#abc5f7d497a4f60e5101f0e8017d261f3", null ],
    [ "m_size", "structEigen_1_1internal_1_1unary__evaluator_3_01CoherentPadOp_3_01ArgType_00_01SizeAtCompileTime_01_4_01_4.html#a00c95204d05344af083823f2f440f562", null ]
];