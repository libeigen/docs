var structEigen_1_1internal_1_1CoeffLoader =
[
    [ "CoeffLoader", "structEigen_1_1internal_1_1CoeffLoader.html#aff94cb4c1461d9ea9c65f46cd49d2fe2", null ],
    [ "coeff", "structEigen_1_1internal_1_1CoeffLoader.html#a3146d1f9f02152d086b134fc0343b891", null ],
    [ "data", "structEigen_1_1internal_1_1CoeffLoader.html#a34580e618cf36fa9c72524a12d11d819", null ],
    [ "offsetBuffer", "structEigen_1_1internal_1_1CoeffLoader.html#a52efd39781ab4496c41d2c9ce5a4b647", null ],
    [ "packet", "structEigen_1_1internal_1_1CoeffLoader.html#a2bea8af391567dfa3cf999ecb74f0e2c", null ],
    [ "m_tensor", "structEigen_1_1internal_1_1CoeffLoader.html#a28c05fb6dd102fceeee23c1dd55e256d", null ]
];