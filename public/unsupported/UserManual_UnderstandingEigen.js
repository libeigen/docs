var UserManual_UnderstandingEigen =
[
    [ "What happens inside Eigen, on a simple example", "../TopicInsideEigenExample.html", null ],
    [ "The class hierarchy", "../TopicClassHierarchy.html", null ],
    [ "Lazy Evaluation and Aliasing", "../TopicLazyEvaluation.html", null ]
];