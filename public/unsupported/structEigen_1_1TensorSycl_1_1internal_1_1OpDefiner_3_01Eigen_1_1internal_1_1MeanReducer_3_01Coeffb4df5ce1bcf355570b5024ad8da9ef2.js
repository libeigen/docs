var structEigen_1_1TensorSycl_1_1internal_1_1OpDefiner_3_01Eigen_1_1internal_1_1MeanReducer_3_01Coeffb4df5ce1bcf355570b5024ad8da9ef2 =
[
    [ "PacketReturnType", "structEigen_1_1TensorSycl_1_1internal_1_1OpDefiner.html#ac3377a7382a65e1be4362ed0aac75ce8", null ],
    [ "type", "structEigen_1_1TensorSycl_1_1internal_1_1OpDefiner.html#aab5c1eaabba46b27d16d897c615f31d7", null ],
    [ "type", "structEigen_1_1TensorSycl_1_1internal_1_1OpDefiner_3_01Eigen_1_1internal_1_1MeanReducer_3_01Coeffb4df5ce1bcf355570b5024ad8da9ef2.html#ab38d4182f9e7df3d13d8d9ce1d4e1ad4", null ],
    [ "finalise_op", "structEigen_1_1TensorSycl_1_1internal_1_1OpDefiner_3_01Eigen_1_1internal_1_1MeanReducer_3_01Coeffb4df5ce1bcf355570b5024ad8da9ef2.html#a6fe63f8378156d283c726d0f093ecece", null ],
    [ "finalise_op", "structEigen_1_1TensorSycl_1_1internal_1_1OpDefiner.html#ac3f17530a4a3f2a9bf4824004ae66bec", null ],
    [ "get_op", "structEigen_1_1TensorSycl_1_1internal_1_1OpDefiner_3_01Eigen_1_1internal_1_1MeanReducer_3_01Coeffb4df5ce1bcf355570b5024ad8da9ef2.html#ac4983268406502d120864db5fa11a716", null ],
    [ "get_op", "structEigen_1_1TensorSycl_1_1internal_1_1OpDefiner.html#abbc30f819161d59de33efc955f98c536", null ]
];