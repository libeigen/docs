var structEigen_1_1internal_1_1traits_3_01TensorCustomUnaryOp_3_01CustomUnaryFunc_00_01XprType_01_4_01_4 =
[
    [ "Index", "structEigen_1_1internal_1_1traits_3_01TensorCustomUnaryOp_3_01CustomUnaryFunc_00_01XprType_01_4_01_4.html#a370b75d43ad4b040967dc76e2b0c38ea", null ],
    [ "Nested", "structEigen_1_1internal_1_1traits_3_01TensorCustomUnaryOp_3_01CustomUnaryFunc_00_01XprType_01_4_01_4.html#ae18f30be7d4180c41041c8f598be2bd3", null ],
    [ "Nested_", "structEigen_1_1internal_1_1traits_3_01TensorCustomUnaryOp_3_01CustomUnaryFunc_00_01XprType_01_4_01_4.html#ad648798634c3e5397df929a420bcc244", null ],
    [ "PointerType", "structEigen_1_1internal_1_1traits_3_01TensorCustomUnaryOp_3_01CustomUnaryFunc_00_01XprType_01_4_01_4.html#a1c317e3d70a98b934b6c929ab49806fd", null ],
    [ "Scalar", "structEigen_1_1internal_1_1traits_3_01TensorCustomUnaryOp_3_01CustomUnaryFunc_00_01XprType_01_4_01_4.html#a510d37c2a3ca985023dcb436a80999f4", null ],
    [ "StorageKind", "structEigen_1_1internal_1_1traits_3_01TensorCustomUnaryOp_3_01CustomUnaryFunc_00_01XprType_01_4_01_4.html#ac7b976b445bffbe3fbbe2b2bea5cfd38", null ],
    [ "Layout", "structEigen_1_1internal_1_1traits_3_01TensorCustomUnaryOp_3_01CustomUnaryFunc_00_01XprType_01_4_01_4.html#a6cc384489deb390bb2eee09f20290ccb", null ],
    [ "NumDimensions", "structEigen_1_1internal_1_1traits_3_01TensorCustomUnaryOp_3_01CustomUnaryFunc_00_01XprType_01_4_01_4.html#afa12a0f0eff3360c6326ea96018d92f3", null ]
];