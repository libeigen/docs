/*
 @licstart  The following is the entire license notice for the JavaScript code in this file.

 The MIT License (MIT)

 Copyright (C) 1997-2020 by Dimitri van Heesch

 Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 and associated documentation files (the "Software"), to deal in the Software without restriction,
 including without limitation the rights to use, copy, modify, merge, publish, distribute,
 sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all copies or
 substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

 @licend  The above is the entire license notice for the JavaScript code in this file
*/
var NAVTREE =
[
  [ "Eigen-unsupported", "index.html", [
    [ "Getting started", "../GettingStarted.html", null ],
    [ "Extending/Customizing Eigen", "../UserManual_CustomizingEigen.html", "UserManual_CustomizingEigen" ],
    [ "General topics", "../UserManual_Generalities.html", "UserManual_Generalities" ],
    [ "Understanding Eigen", "../UserManual_UnderstandingEigen.html", "UserManual_UnderstandingEigen" ],
    [ "Unclassified pages", "../UnclassifiedPages.html", "UnclassifiedPages" ],
    [ "Eigen SYCL Backend", "SYCL_EIGEN.html", null ],
    [ "Eigen Tensors", "eigen_tensors.html", [
      [ "Tensor Classes", "eigen_tensors.html#autotoc_md0", [
        [ "Class Tensor<data_type, rank>", "eigen_tensors.html#autotoc_md1", [
          [ "Constructor Tensor<data_type, rank>(size0, size1, ...)", "eigen_tensors.html#autotoc_md2", null ],
          [ "Constructor Tensor<data_type, rank>(size_array)", "eigen_tensors.html#autotoc_md3", null ]
        ] ],
        [ "Class TensorFixedSize<data_type, Sizes<size0, size1, ...>>", "eigen_tensors.html#autotoc_md4", null ],
        [ "Class TensorMap<Tensor<data_type, rank>>", "eigen_tensors.html#autotoc_md6", [
          [ "Constructor TensorMap<Tensor<data_type, rank>>(data, size0, size1, ...)", "eigen_tensors.html#autotoc_md7", null ],
          [ "Class TensorRef", "eigen_tensors.html#autotoc_md8", null ]
        ] ]
      ] ],
      [ "Accessing Tensor Elements", "eigen_tensors.html#autotoc_md9", null ],
      [ "TensorLayout", "eigen_tensors.html#autotoc_md11", null ],
      [ "Tensor Operations", "eigen_tensors.html#autotoc_md12", [
        [ "Tensor Operations and C++ \"auto\"", "eigen_tensors.html#autotoc_md13", null ],
        [ "Controlling When Expression are Evaluated", "eigen_tensors.html#autotoc_md14", [
          [ "Assigning to a Tensor, TensorFixedSize, or TensorMap.", "eigen_tensors.html#autotoc_md15", null ],
          [ "Calling eval().", "eigen_tensors.html#autotoc_md16", null ],
          [ "Assigning to a TensorRef.", "eigen_tensors.html#autotoc_md17", null ]
        ] ],
        [ "Controlling How Expressions Are Evaluated", "eigen_tensors.html#autotoc_md22", [
          [ "Evaluating With the DefaultDevice", "eigen_tensors.html#autotoc_md23", null ],
          [ "Evaluating with a Thread Pool", "eigen_tensors.html#autotoc_md24", null ],
          [ "Evaluating On GPU", "eigen_tensors.html#autotoc_md25", null ]
        ] ]
      ] ],
      [ "API Reference", "eigen_tensors.html#autotoc_md26", [
        [ "Datatypes", "eigen_tensors.html#autotoc_md27", [
          [ "<Tensor-Type>::Dimensions", "eigen_tensors.html#autotoc_md28", null ],
          [ "<Tensor-Type>::Index", "eigen_tensors.html#autotoc_md29", null ],
          [ "<Tensor-Type>::Scalar", "eigen_tensors.html#autotoc_md30", null ],
          [ "(Operation)", "eigen_tensors.html#autotoc_md31", null ]
        ] ]
      ] ],
      [ "Built-in Tensor Methods", "eigen_tensors.html#autotoc_md32", null ],
      [ "Metadata", "eigen_tensors.html#autotoc_md33", [
        [ "int NumDimensions", "eigen_tensors.html#autotoc_md34", null ],
        [ "Dimensions dimensions()", "eigen_tensors.html#autotoc_md35", null ],
        [ "Index dimension(Index n)", "eigen_tensors.html#autotoc_md36", null ],
        [ "Index size()", "eigen_tensors.html#autotoc_md37", null ],
        [ "Getting Dimensions From An Operation", "eigen_tensors.html#autotoc_md38", null ]
      ] ],
      [ "Constructors", "eigen_tensors.html#autotoc_md39", [
        [ "Tensor", "eigen_tensors.html#autotoc_md40", null ],
        [ "TensorFixedSize", "eigen_tensors.html#autotoc_md41", null ],
        [ "TensorMap", "eigen_tensors.html#autotoc_md42", null ]
      ] ],
      [ "Contents Initialization", "eigen_tensors.html#autotoc_md43", [
        [ "<Tensor-Type> setConstant(const Scalar& val)", "eigen_tensors.html#autotoc_md44", null ],
        [ "<Tensor-Type> setZero()", "eigen_tensors.html#autotoc_md45", null ],
        [ "<Tensor-Type> setValues({..initializer_list})", "eigen_tensors.html#autotoc_md46", null ],
        [ "<Tensor-Type> setRandom()", "eigen_tensors.html#autotoc_md47", null ]
      ] ],
      [ "Data Access", "eigen_tensors.html#autotoc_md48", [
        [ "Scalar* data() and const Scalar* data() const", "eigen_tensors.html#autotoc_md49", null ]
      ] ],
      [ "Tensor Operations", "eigen_tensors.html#autotoc_md50", [
        [ "(Operation) constant(const Scalar& val)", "eigen_tensors.html#autotoc_md51", null ],
        [ "(Operation) random()", "eigen_tensors.html#autotoc_md52", null ]
      ] ],
      [ "Unary Element Wise Operations", "eigen_tensors.html#autotoc_md53", [
        [ "(Operation) operator-()", "eigen_tensors.html#autotoc_md55", null ],
        [ "(Operation) sqrt()", "eigen_tensors.html#autotoc_md57", null ],
        [ "(Operation) rsqrt()", "eigen_tensors.html#autotoc_md58", null ],
        [ "(Operation) square()", "eigen_tensors.html#autotoc_md60", null ],
        [ "(Operation) inverse()", "eigen_tensors.html#autotoc_md63", null ],
        [ "(Operation) exp()", "eigen_tensors.html#autotoc_md64", null ],
        [ "(Operation) log()", "eigen_tensors.html#autotoc_md65", null ],
        [ "(Operation) abs()", "eigen_tensors.html#autotoc_md66", null ],
        [ "(Operation) arg()", "eigen_tensors.html#autotoc_md67", null ],
        [ "(Operation) real()", "eigen_tensors.html#autotoc_md68", null ],
        [ "(Operation) imag()", "eigen_tensors.html#autotoc_md69", null ],
        [ "(Operation) pow(Scalar exponent)", "eigen_tensors.html#autotoc_md70", null ],
        [ "(Operation) operator * (Scalar scale)", "eigen_tensors.html#autotoc_md71", null ],
        [ "(Operation) cwiseMax(Scalar threshold)", "eigen_tensors.html#autotoc_md72", null ],
        [ "(Operation) cwiseMin(Scalar threshold)", "eigen_tensors.html#autotoc_md73", null ],
        [ "(Operation) unaryExpr(const CustomUnaryOp& func)", "eigen_tensors.html#autotoc_md74", null ]
      ] ],
      [ "Binary Element Wise Operations", "eigen_tensors.html#autotoc_md75", [
        [ "(Operation) operator+(const OtherDerived& other)", "eigen_tensors.html#autotoc_md76", null ],
        [ "(Operation) operator-(const OtherDerived& other)", "eigen_tensors.html#autotoc_md77", null ],
        [ "(Operation) operator*(const OtherDerived& other)", "eigen_tensors.html#autotoc_md78", null ],
        [ "(Operation) operator/(const OtherDerived& other)", "eigen_tensors.html#autotoc_md79", null ],
        [ "(Operation) cwiseMax(const OtherDerived& other)", "eigen_tensors.html#autotoc_md80", null ],
        [ "(Operation) cwiseMin(const OtherDerived& other)", "eigen_tensors.html#autotoc_md81", null ],
        [ "(Operation) Logical operators", "eigen_tensors.html#autotoc_md82", null ]
      ] ],
      [ "Selection (select(const ThenDerived& thenTensor, const ElseDerived& elseTensor)", "eigen_tensors.html#autotoc_md83", null ],
      [ "Contraction", "eigen_tensors.html#autotoc_md84", null ],
      [ "Reduction Operations", "eigen_tensors.html#autotoc_md85", [
        [ "Reduction Dimensions", "eigen_tensors.html#autotoc_md86", [
          [ "Reduction along all dimensions", "eigen_tensors.html#autotoc_md87", null ]
        ] ],
        [ "(Operation) sum(const Dimensions& new_dims)", "eigen_tensors.html#autotoc_md88", null ],
        [ "(Operation) sum()", "eigen_tensors.html#autotoc_md89", null ],
        [ "(Operation) mean(const Dimensions& new_dims)", "eigen_tensors.html#autotoc_md90", null ],
        [ "(Operation) mean()", "eigen_tensors.html#autotoc_md91", null ],
        [ "(Operation) maximum(const Dimensions& new_dims)", "eigen_tensors.html#autotoc_md92", null ],
        [ "(Operation) maximum()", "eigen_tensors.html#autotoc_md93", null ],
        [ "(Operation) minimum(const Dimensions& new_dims)", "eigen_tensors.html#autotoc_md94", null ],
        [ "(Operation) minimum()", "eigen_tensors.html#autotoc_md95", null ],
        [ "(Operation) prod(const Dimensions& new_dims)", "eigen_tensors.html#autotoc_md96", null ],
        [ "(Operation) prod()", "eigen_tensors.html#autotoc_md97", null ],
        [ "(Operation) all(const Dimensions& new_dims)", "eigen_tensors.html#autotoc_md98", null ],
        [ "(Operation) all()", "eigen_tensors.html#autotoc_md99", null ],
        [ "(Operation) any(const Dimensions& new_dims)", "eigen_tensors.html#autotoc_md100", null ],
        [ "(Operation) any()", "eigen_tensors.html#autotoc_md101", null ],
        [ "(Operation) reduce(const Dimensions& new_dims, const Reducer& reducer)", "eigen_tensors.html#autotoc_md102", null ]
      ] ],
      [ "Trace", "eigen_tensors.html#autotoc_md103", [
        [ "(Operation) trace(const Dimensions& new_dims)", "eigen_tensors.html#autotoc_md104", null ],
        [ "(Operation) trace()", "eigen_tensors.html#autotoc_md105", null ]
      ] ],
      [ "Scan Operations", "eigen_tensors.html#autotoc_md106", [
        [ "(Operation) cumsum(const Index& axis)", "eigen_tensors.html#autotoc_md107", null ],
        [ "(Operation) cumprod(const Index& axis)", "eigen_tensors.html#autotoc_md108", null ]
      ] ],
      [ "Convolutions", "eigen_tensors.html#autotoc_md109", [
        [ "(Operation) convolve(const Kernel& kernel, const Dimensions& dims)", "eigen_tensors.html#autotoc_md110", null ]
      ] ],
      [ "Geometrical Operations", "eigen_tensors.html#autotoc_md111", [
        [ "(Operation) reshape(const Dimensions& new_dims)", "eigen_tensors.html#autotoc_md112", null ],
        [ "(Operation) shuffle(const Shuffle& shuffle)", "eigen_tensors.html#autotoc_md113", null ],
        [ "(Operation) stride(const Strides& strides)", "eigen_tensors.html#autotoc_md114", null ],
        [ "(Operation) slice(const StartIndices& offsets, const Sizes& extents)", "eigen_tensors.html#autotoc_md115", null ],
        [ "(Operation) chip(const Index offset, const Index dim)", "eigen_tensors.html#autotoc_md116", null ],
        [ "(Operation) reverse(const ReverseDimensions& reverse)", "eigen_tensors.html#autotoc_md117", null ],
        [ "(Operation) broadcast(const Broadcast& broadcast)", "eigen_tensors.html#autotoc_md118", null ],
        [ "(Operation) concatenate(const OtherDerived& other, Axis axis)", "eigen_tensors.html#autotoc_md119", null ],
        [ "(Operation) pad(const PaddingDimensions& padding)", "eigen_tensors.html#autotoc_md120", null ],
        [ "(Operation) extract_patches(const PatchDims& patch_dims)", "eigen_tensors.html#autotoc_md121", null ],
        [ "(Operation) extract_image_patches(const Index patch_rows, const Index patch_cols, const Index row_str...", "eigen_tensors.html#autotoc_md122", null ]
      ] ],
      [ "Special Operations", "eigen_tensors.html#autotoc_md123", [
        [ "(Operation) cast<T>()", "eigen_tensors.html#autotoc_md124", null ],
        [ "(Operation) eval()", "eigen_tensors.html#autotoc_md125", null ]
      ] ],
      [ "Tensor Printing", "eigen_tensors.html#autotoc_md126", null ],
      [ "Representation of scalar values", "eigen_tensors.html#autotoc_md127", null ],
      [ "Limitations", "eigen_tensors.html#autotoc_md128", null ]
    ] ],
    [ "Chapters", "topics.html", "topics" ],
    [ "Classes", "annotated.html", [
      [ "Class List", "annotated.html", "annotated_dup" ],
      [ "Class Hierarchy", "hierarchy.html", "hierarchy" ],
      [ "Class Members", "functions.html", [
        [ "All", "functions.html", "functions_dup" ],
        [ "Functions", "functions_func.html", "functions_func" ],
        [ "Variables", "functions_vars.html", null ],
        [ "Typedefs", "functions_type.html", null ],
        [ "Enumerator", "functions_eval.html", null ]
      ] ]
    ] ],
    [ "Files", "files.html", [
      [ "File List", "files.html", "files_dup" ]
    ] ],
    [ "Examples", "examples.html", "examples" ]
  ] ]
];

var NAVTREEINDEX =
[
"AVX512_2BesselFunctions_8h_source.html",
"classEigen_1_1EulerSystem.html",
"dir_d5faab7401d265d64d0cce285d80e6ed.html",
"group__Splines__Module.html#gae10a6f9b6ab7fb400a2526b6382c533b"
];

var SYNCONMSG = 'click to disable panel synchronization';
var SYNCOFFMSG = 'click to enable panel synchronization';