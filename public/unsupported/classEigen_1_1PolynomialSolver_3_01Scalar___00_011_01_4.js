var classEigen_1_1PolynomialSolver_3_01Scalar___00_011_01_4 =
[
    [ "CompanionMatrixType", "classEigen_1_1PolynomialSolver.html#aafca754afe93dfd8a76bfcc28a797035", null ],
    [ "ComplexScalar", "classEigen_1_1PolynomialSolver.html#ae9108b3f3fefc295d95d0f4e6b54b881", null ],
    [ "EigenSolverType", "classEigen_1_1PolynomialSolver.html#ae69ddd9a3b9cfb4d8d9043ffa752cdcb", null ],
    [ "PS_Base", "classEigen_1_1PolynomialSolver.html#a7e466dcf48e27fc06011aa55e2dc317e", null ],
    [ "PS_Base", "classEigen_1_1PolynomialSolver_3_01Scalar___00_011_01_4.html#a84ed14fd29da633ed6a60991aacf2f5e", null ],
    [ "PolynomialSolver", "classEigen_1_1PolynomialSolver_3_01Scalar___00_011_01_4.html#a6df575d4e5bc5cae3f7e8cc00f588260", null ],
    [ "PolynomialSolver", "classEigen_1_1PolynomialSolver_3_01Scalar___00_011_01_4.html#a4a4474ca69b97af81baeec77c064db47", null ],
    [ "PolynomialSolver", "classEigen_1_1PolynomialSolver.html#acd79de11f3351595e5ebfb0bfe3807ee", null ],
    [ "PolynomialSolver", "classEigen_1_1PolynomialSolver.html#acff814b4e1b8fb41cb692da2abbf0ea3", null ],
    [ "compute", "classEigen_1_1PolynomialSolver.html#a93d305d8529be0de76019514eb87b086", null ],
    [ "compute", "classEigen_1_1PolynomialSolver_3_01Scalar___00_011_01_4.html#a0f1a060634caa3a17a56fb8c5477a782", null ],
    [ "m_eigenSolver", "classEigen_1_1PolynomialSolver.html#a41188061ac1585daeb7c99e2d3a342a5", null ],
    [ "m_roots", "classEigen_1_1PolynomialSolver.html#ad995a8122ef108adbb408cbd11aefe4f", null ],
    [ "m_roots", "classEigen_1_1PolynomialSolver.html#ad995a8122ef108adbb408cbd11aefe4f", null ]
];