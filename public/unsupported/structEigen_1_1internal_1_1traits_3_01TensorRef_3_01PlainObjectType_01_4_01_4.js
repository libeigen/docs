var structEigen_1_1internal_1_1traits_3_01TensorRef_3_01PlainObjectType_01_4_01_4 =
[
    [ "BaseTraits", "structEigen_1_1internal_1_1traits_3_01TensorRef_3_01PlainObjectType_01_4_01_4.html#a5679093a2ec20c3e1b293604b44c791c", null ],
    [ "Index", "structEigen_1_1internal_1_1traits_3_01TensorRef_3_01PlainObjectType_01_4_01_4.html#a7144d55c55aba77e0d10263c39f4965d", null ],
    [ "PointerType", "structEigen_1_1internal_1_1traits_3_01TensorRef_3_01PlainObjectType_01_4_01_4.html#ac4a6724232d160d6e571f9104d7cdef0", null ],
    [ "Scalar", "structEigen_1_1internal_1_1traits_3_01TensorRef_3_01PlainObjectType_01_4_01_4.html#afb85d86ca1a50cd1f0aa13ad6512b126", null ],
    [ "StorageKind", "structEigen_1_1internal_1_1traits_3_01TensorRef_3_01PlainObjectType_01_4_01_4.html#a1481e20e9380ec1195a428f90368ba56", null ],
    [ "Layout", "structEigen_1_1internal_1_1traits_3_01TensorRef_3_01PlainObjectType_01_4_01_4.html#ab5c41ba126f3f7f29bb5a036e480aafa", null ],
    [ "NumDimensions", "structEigen_1_1internal_1_1traits_3_01TensorRef_3_01PlainObjectType_01_4_01_4.html#a784eb93656ba6c077fd31a406d14c6d8", null ]
];