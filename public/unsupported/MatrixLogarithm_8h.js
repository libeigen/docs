var MatrixLogarithm_8h =
[
    [ "Eigen::internal::matrix_log_max_pade_degree< Scalar >", "structEigen_1_1internal_1_1matrix__log__max__pade__degree.html", "structEigen_1_1internal_1_1matrix__log__max__pade__degree" ],
    [ "Eigen::internal::matrix_log_min_pade_degree< Scalar >", "structEigen_1_1internal_1_1matrix__log__min__pade__degree.html", "structEigen_1_1internal_1_1matrix__log__min__pade__degree" ],
    [ "Eigen::internal::traits< MatrixLogarithmReturnValue< Derived > >", "../structEigen_1_1internal_1_1traits_3_01MatrixLogarithmReturnValue_3_01Derived_01_4_01_4.html", null ],
    [ "Eigen::internal::matrix_log_compute_2x2", "../namespaceEigen_1_1internal.html#a1cb8d312c017f94570a52317fd1ece5f", null ],
    [ "Eigen::internal::matrix_log_compute_big", "../namespaceEigen_1_1internal.html#ac2de7acbbff34ec236ee5e9fdb2eee38", null ],
    [ "Eigen::internal::matrix_log_compute_pade", "../namespaceEigen_1_1internal.html#a366cdd93d3035b0053cc8b3bff77f2a4", null ],
    [ "Eigen::internal::matrix_log_get_pade_degree", "../namespaceEigen_1_1internal.html#a7cdde7861546c148b2067fd94b3933f0", null ],
    [ "Eigen::internal::matrix_log_get_pade_degree", "../namespaceEigen_1_1internal.html#ae0091d6d89ad60b1117bbb1b4b9901d0", null ],
    [ "Eigen::internal::matrix_log_get_pade_degree", "../namespaceEigen_1_1internal.html#ab6cb311dc4965edfa087bc35a9ee99ed", null ]
];