var structEigen_1_1TensorSycl_1_1internal_1_1ThreadProperties =
[
    [ "ThreadProperties", "structEigen_1_1TensorSycl_1_1internal_1_1ThreadProperties.html#aa59143cd0135b7bbbcfb9e6482bf8f6d", null ],
    [ "is_internal", "structEigen_1_1TensorSycl_1_1internal_1_1ThreadProperties.html#aea00fd9e831efc96452c37ef36b0e45a", null ],
    [ "kGroupId", "structEigen_1_1TensorSycl_1_1internal_1_1ThreadProperties.html#a1f8c141965df12c841a4537e2de4e641", null ],
    [ "kGroupOffset", "structEigen_1_1TensorSycl_1_1internal_1_1ThreadProperties.html#abb2688fe3effd2dec35b9643105efbc4", null ],
    [ "kSize", "structEigen_1_1TensorSycl_1_1internal_1_1ThreadProperties.html#ab6650dd7c0719699e34b5c0cffeaa18a", null ],
    [ "linearLocalThreadId", "structEigen_1_1TensorSycl_1_1internal_1_1ThreadProperties.html#a8d448fab09dea361e6b863c2b909f4cd", null ],
    [ "mGlobalOffset", "structEigen_1_1TensorSycl_1_1internal_1_1ThreadProperties.html#acf655f29dbb176db71b8c60c4dc35eae", null ],
    [ "mGroupOffset", "structEigen_1_1TensorSycl_1_1internal_1_1ThreadProperties.html#a015de15d574896ff77e8a1c37c5d044b", null ],
    [ "mLocalOffset", "structEigen_1_1TensorSycl_1_1internal_1_1ThreadProperties.html#a15df9cd6e86ec280c34d58814409e636", null ],
    [ "nGlobalOffset", "structEigen_1_1TensorSycl_1_1internal_1_1ThreadProperties.html#af64a6acf6172994487cbdc08d7926482", null ],
    [ "nGroupOffset", "structEigen_1_1TensorSycl_1_1internal_1_1ThreadProperties.html#a052d05954f4a43631d48130cb9878ff4", null ],
    [ "nLocalOffset", "structEigen_1_1TensorSycl_1_1internal_1_1ThreadProperties.html#a8dc4f4316bde5e4078f0128feef9482f", null ]
];