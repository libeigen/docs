var structEigen_1_1internal_1_1fftw__plan_3_01double_01_4 =
[
    [ "complex_type", "structEigen_1_1internal_1_1fftw__plan_3_01double_01_4.html#adca18bb5ffdf1cfa50ef97d070a4bc64", null ],
    [ "scalar_type", "structEigen_1_1internal_1_1fftw__plan_3_01double_01_4.html#a112cdb0303d534669a43ae772323998f", null ],
    [ "fftw_plan", "structEigen_1_1internal_1_1fftw__plan_3_01double_01_4.html#a831586c3027fc718a243f1a2f415f841", null ],
    [ "fwd", "structEigen_1_1internal_1_1fftw__plan_3_01double_01_4.html#a48968e6abdf5c5e3fcdd5ba53117aafb", null ],
    [ "fwd", "structEigen_1_1internal_1_1fftw__plan_3_01double_01_4.html#aa5f91d54fad64fbe4f5b7d8969c77542", null ],
    [ "fwd2", "structEigen_1_1internal_1_1fftw__plan_3_01double_01_4.html#ace4c5f1b0687b0e85fdc1ce9e188aaa3", null ],
    [ "inv", "structEigen_1_1internal_1_1fftw__plan_3_01double_01_4.html#a8bcceee3ae70cf398d7e00a07109a2e2", null ],
    [ "inv", "structEigen_1_1internal_1_1fftw__plan_3_01double_01_4.html#a2c8abc28e2847bf0fd03f28be2273d86", null ],
    [ "inv2", "structEigen_1_1internal_1_1fftw__plan_3_01double_01_4.html#a90bfe92a1e64201b30f2a9f1a075044e", null ],
    [ "set_plan", "structEigen_1_1internal_1_1fftw__plan_3_01double_01_4.html#a62d56aecbab32273ba7c643ca8b9d5ad", null ],
    [ "m_plan", "structEigen_1_1internal_1_1fftw__plan_3_01double_01_4.html#aca1a11c1ba4f1ceba50f37d709c7205f", null ]
];