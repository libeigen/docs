var structEigen_1_1TensorSycl_1_1internal_1_1ScanInfo =
[
    [ "ScanInfo", "structEigen_1_1TensorSycl_1_1internal_1_1ScanInfo.html#aaffd0a781ea5973fd2120f8a51ec7d4f", null ],
    [ "get_scan_parameter", "structEigen_1_1TensorSycl_1_1internal_1_1ScanInfo.html#ac487aefc80ea7f45e276e7840de37f16", null ],
    [ "get_thread_range", "structEigen_1_1TensorSycl_1_1internal_1_1ScanInfo.html#aa5639d0d468712ccd003309062a14db4", null ],
    [ "block_size", "structEigen_1_1TensorSycl_1_1internal_1_1ScanInfo.html#a897b9e3d88f9cce0615b9d5a70a0432d", null ],
    [ "block_threads", "structEigen_1_1TensorSycl_1_1internal_1_1ScanInfo.html#a7fd86de2bbc0b61de73eb8a68f8d4550", null ],
    [ "dev", "structEigen_1_1TensorSycl_1_1internal_1_1ScanInfo.html#a4bbb0fba675eb075debfdfec100716a9", null ],
    [ "elements_per_block", "structEigen_1_1TensorSycl_1_1internal_1_1ScanInfo.html#a7f22d09ca3211cd0c637c887e72e097a", null ],
    [ "elements_per_group", "structEigen_1_1TensorSycl_1_1internal_1_1ScanInfo.html#a8ea157d68329d55378c91ad3fbd4f35c", null ],
    [ "global_range", "structEigen_1_1TensorSycl_1_1internal_1_1ScanInfo.html#adf2579a57b2e96044f80ad6a1c39d75d", null ],
    [ "group_threads", "structEigen_1_1TensorSycl_1_1internal_1_1ScanInfo.html#a2a5552e0b5657610ecdf54e7980d7708", null ],
    [ "local_range", "structEigen_1_1TensorSycl_1_1internal_1_1ScanInfo.html#a54abff638cd2a216ef125d97bcc0af7d", null ],
    [ "loop_range", "structEigen_1_1TensorSycl_1_1internal_1_1ScanInfo.html#a8fb629559629b1b6e9fb29a629ada10a", null ],
    [ "max_elements_per_block", "structEigen_1_1TensorSycl_1_1internal_1_1ScanInfo.html#a0213c58f40d9ff151982cd6e95c28102", null ],
    [ "non_scan_size", "structEigen_1_1TensorSycl_1_1internal_1_1ScanInfo.html#a394dc7fe0d4c360593abface152c39b9", null ],
    [ "non_scan_stride", "structEigen_1_1TensorSycl_1_1internal_1_1ScanInfo.html#a0f18cc7cdbbd7b1bff1dd67d06959e42", null ],
    [ "panel_size", "structEigen_1_1TensorSycl_1_1internal_1_1ScanInfo.html#aa88e6f167729bd401c2d03512cd232d7", null ],
    [ "panel_threads", "structEigen_1_1TensorSycl_1_1internal_1_1ScanInfo.html#a8a360c66bf40a8a1c53b8b60910bc76c", null ],
    [ "scan_size", "structEigen_1_1TensorSycl_1_1internal_1_1ScanInfo.html#aac010051181948285c0549393b73c35b", null ],
    [ "scan_stride", "structEigen_1_1TensorSycl_1_1internal_1_1ScanInfo.html#a3d5f35513dac51a1878d1f2167a12742", null ],
    [ "total_size", "structEigen_1_1TensorSycl_1_1internal_1_1ScanInfo.html#a081074af95f268bf503ece59f41b9984", null ]
];