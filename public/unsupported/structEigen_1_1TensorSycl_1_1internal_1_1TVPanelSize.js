var structEigen_1_1TensorSycl_1_1internal_1_1TVPanelSize =
[
    [ "BC", "structEigen_1_1TensorSycl_1_1internal_1_1TVPanelSize.html#a1d3e09f5618553c9ffb00cadb7575e18", null ],
    [ "LocalThreadSizeC", "structEigen_1_1TensorSycl_1_1internal_1_1TVPanelSize.html#afaa0b7e61511cb40c011ebdfee932975", null ],
    [ "LocalThreadSizeNC", "structEigen_1_1TensorSycl_1_1internal_1_1TVPanelSize.html#ae08ceb53f84fd32fbbf978fb52b15446", null ],
    [ "TileSizeDimC", "structEigen_1_1TensorSycl_1_1internal_1_1TVPanelSize.html#aab22f20911be0a9a51eed7fc5f27ebf5", null ],
    [ "TileSizeDimNC", "structEigen_1_1TensorSycl_1_1internal_1_1TVPanelSize.html#a306e72dde7171705f5cf5fef7aff4c41", null ],
    [ "WorkLoadPerThreadC", "structEigen_1_1TensorSycl_1_1internal_1_1TVPanelSize.html#ab78373582a86ae581b10f708c2d38382", null ],
    [ "WorkLoadPerThreadNC", "structEigen_1_1TensorSycl_1_1internal_1_1TVPanelSize.html#abdbefcc1dcecdb9b96c323ba5988e081", null ]
];