var structEigen_1_1SplineTraits_3_01Spline_3_01Scalar___00_01Dim___00_01Degree___01_4_00_01Dynamic_01_4 =
[
    [ "BasisDerivativeType", "structEigen_1_1SplineTraits_3_01Spline_3_01Scalar___00_01Dim___00_01Degree___01_4_00_01Dynamic_01_4.html#a18f74648544d3ec76af5764b5ae44032", null ],
    [ "BasisVectorType", "structEigen_1_1SplineTraits_3_01Spline_3_01Scalar___00_01Dim___00_01Degree___01_4_00_01Dynamic_01_4.html#a3b3dbdb68634c2d1b4e9080e018687eb", null ],
    [ "ControlPointVectorType", "structEigen_1_1SplineTraits_3_01Spline_3_01Scalar___00_01Dim___00_01Degree___01_4_00_01Dynamic_01_4.html#a6dff54ef43fb4080b3794290b38d3efe", null ],
    [ "DerivativeType", "structEigen_1_1SplineTraits_3_01Spline_3_01Scalar___00_01Dim___00_01Degree___01_4_00_01Dynamic_01_4.html#ac538fb6f640140ecc93f207cbc62950b", null ],
    [ "KnotVectorType", "structEigen_1_1SplineTraits_3_01Spline_3_01Scalar___00_01Dim___00_01Degree___01_4_00_01Dynamic_01_4.html#af75a3b2dd9bca842ea12dedefcdf3e39", null ],
    [ "ParameterVectorType", "structEigen_1_1SplineTraits_3_01Spline_3_01Scalar___00_01Dim___00_01Degree___01_4_00_01Dynamic_01_4.html#a213611723985e7cd56415b553f19bfad", null ],
    [ "PointType", "structEigen_1_1SplineTraits_3_01Spline_3_01Scalar___00_01Dim___00_01Degree___01_4_00_01Dynamic_01_4.html#a2ffdcf626600bf9a45c4a5d7ac11a04c", null ],
    [ "Scalar", "structEigen_1_1SplineTraits_3_01Spline_3_01Scalar___00_01Dim___00_01Degree___01_4_00_01Dynamic_01_4.html#ac7ad5c8cce020be6923054f125e1c050", null ]
];