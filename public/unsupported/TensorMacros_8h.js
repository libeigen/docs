var TensorMacros_8h =
[
    [ "EIGEN_DEVICE_REF", "TensorMacros_8h.html#ab1d48616b1c0fcce6dc3f01cad41b1d8", null ],
    [ "EIGEN_SFINAE_ENABLE_IF", "TensorMacros_8h.html#a1629067fba6a085b247552d00d475e3e", null ],
    [ "EIGEN_SYCL_LOCAL_MEM_UNSET_OR_OFF", "TensorMacros_8h.html#aec905b82a3aa7df71c9e9d06e2ed3aed", null ],
    [ "EIGEN_SYCL_LOCAL_MEM_UNSET_OR_ON", "TensorMacros_8h.html#aaed4c3a59fa23ded7e3bdc11870872c2", null ],
    [ "EIGEN_SYCL_TRY_CATCH", "TensorMacros_8h.html#ab17c14be2b62eb409a37c145ae57576b", null ],
    [ "EIGEN_TENSOR_INHERIT_ASSIGNMENT_EQUAL_OPERATOR", "TensorMacros_8h.html#ab078eebbbcb8d30da565ee0314691a8f", null ],
    [ "EIGEN_TENSOR_INHERIT_ASSIGNMENT_OPERATORS", "TensorMacros_8h.html#a1e30a7fbcbc068839710dec022507b33", null ]
];