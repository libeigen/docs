var structEigen_1_1TensorSycl_1_1internal_1_1PartialReductionKernel =
[
    [ "CoeffReturnType", "structEigen_1_1TensorSycl_1_1internal_1_1PartialReductionKernel.html#aea3d307e4ec9cfd833b7d03d032fbac5", null ],
    [ "EvaluatorPointerType", "structEigen_1_1TensorSycl_1_1internal_1_1PartialReductionKernel.html#a7f360fd012b6a7874e369b134d9ecad3", null ],
    [ "Index", "structEigen_1_1TensorSycl_1_1internal_1_1PartialReductionKernel.html#a0d1e1c56e9c433fab988c91d0d694cb4", null ],
    [ "Op", "structEigen_1_1TensorSycl_1_1internal_1_1PartialReductionKernel.html#a72f51e7d783cdab1e1d456834d0aceaf", null ],
    [ "OpDef", "structEigen_1_1TensorSycl_1_1internal_1_1PartialReductionKernel.html#afc2eb5fa474a837a459bcbef7cf9e7cd", null ],
    [ "ScratchAcc", "structEigen_1_1TensorSycl_1_1internal_1_1PartialReductionKernel.html#a476dc12a1f85e3d52b922ee6281d030d", null ],
    [ "PartialReductionKernel", "structEigen_1_1TensorSycl_1_1internal_1_1PartialReductionKernel.html#a8ba67e4155a9fe4574fa645f30921a4b", null ],
    [ "element_wise_reduce", "structEigen_1_1TensorSycl_1_1internal_1_1PartialReductionKernel.html#a0356048fc589f838e0e615e87ffbbdb7", null ],
    [ "operator()", "structEigen_1_1TensorSycl_1_1internal_1_1PartialReductionKernel.html#a993557b012cb1246a92ce33610d17850", null ],
    [ "evaluator", "structEigen_1_1TensorSycl_1_1internal_1_1PartialReductionKernel.html#ad7c82e6c23ad4f55d2c7c6b0315522a4", null ],
    [ "num_coeffs_to_preserve", "structEigen_1_1TensorSycl_1_1internal_1_1PartialReductionKernel.html#a64a7c8f4ed01d4fef120194682e2032b", null ],
    [ "num_coeffs_to_reduce", "structEigen_1_1TensorSycl_1_1internal_1_1PartialReductionKernel.html#a1c317bc5be55b4b5055553bb9d336ac4", null ],
    [ "op", "structEigen_1_1TensorSycl_1_1internal_1_1PartialReductionKernel.html#a95a99ef9cc59c7be52efb7f0ffebc119", null ],
    [ "output_accessor", "structEigen_1_1TensorSycl_1_1internal_1_1PartialReductionKernel.html#a6c089a18afafbd7c43fa8c4e7e98712f", null ],
    [ "preserve_elements_num_groups", "structEigen_1_1TensorSycl_1_1internal_1_1PartialReductionKernel.html#a01b3cf225cfff070e16b408ea8633c66", null ],
    [ "reduce_elements_num_groups", "structEigen_1_1TensorSycl_1_1internal_1_1PartialReductionKernel.html#a7e4a5409f9f795d6d0da102a8930f82d", null ],
    [ "scratch", "structEigen_1_1TensorSycl_1_1internal_1_1PartialReductionKernel.html#a6110b54ec84bfe3a1df1ca3145eedc3c", null ]
];