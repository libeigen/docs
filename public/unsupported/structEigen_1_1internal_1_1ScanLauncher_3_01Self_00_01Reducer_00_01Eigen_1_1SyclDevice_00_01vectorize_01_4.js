var structEigen_1_1internal_1_1ScanLauncher_3_01Self_00_01Reducer_00_01Eigen_1_1SyclDevice_00_01vectorize_01_4 =
[
    [ "CoeffReturnType", "structEigen_1_1internal_1_1ScanLauncher_3_01Self_00_01Reducer_00_01Eigen_1_1SyclDevice_00_01vectorize_01_4.html#a6fa9df548da3371fecff3480abe7ca7b", null ],
    [ "EvaluatorPointerType", "structEigen_1_1internal_1_1ScanLauncher_3_01Self_00_01Reducer_00_01Eigen_1_1SyclDevice_00_01vectorize_01_4.html#a16259df9968dc4386cdd0d6c879ff869", null ],
    [ "Index", "structEigen_1_1internal_1_1ScanLauncher_3_01Self_00_01Reducer_00_01Eigen_1_1SyclDevice_00_01vectorize_01_4.html#a230f00233f6bd9cda0e633317a8c4b04", null ],
    [ "Storage", "structEigen_1_1internal_1_1ScanLauncher_3_01Self_00_01Reducer_00_01Eigen_1_1SyclDevice_00_01vectorize_01_4.html#ab41c7f37f017db65baf4571d7e83da8d", null ],
    [ "operator()", "structEigen_1_1internal_1_1ScanLauncher_3_01Self_00_01Reducer_00_01Eigen_1_1SyclDevice_00_01vectorize_01_4.html#aa146cf8c8bc29b55112d45dffa1abbe3", null ],
    [ "operator()", "structEigen_1_1internal_1_1ScanLauncher.html#a176b607630827062a6d538ce4e2aa029", null ]
];