var structEigen_1_1internal_1_1fftw__plan =
[
    [ "complex_type", "structEigen_1_1internal_1_1fftw__plan.html#aa3b17911c60e66d3a4fb0b6344d84ed2", null ],
    [ "scalar_type", "structEigen_1_1internal_1_1fftw__plan.html#af8085066b390c525d500c59e0ca6ae1e", null ],
    [ "fftw_plan", "structEigen_1_1internal_1_1fftw__plan.html#a3bf51ff9a55fe0b9154c9b90b4323104", null ],
    [ "fwd", "structEigen_1_1internal_1_1fftw__plan.html#a1ad67049b7a90281784302b12defecb9", null ],
    [ "fwd", "structEigen_1_1internal_1_1fftw__plan.html#a5c0056e14380d16029aa3063cf4b4e1f", null ],
    [ "fwd2", "structEigen_1_1internal_1_1fftw__plan.html#a237259a8eab474291fe97e27a86b18c2", null ],
    [ "inv", "structEigen_1_1internal_1_1fftw__plan.html#aa1107ed22a7c71ba282eb6879ed36310", null ],
    [ "inv", "structEigen_1_1internal_1_1fftw__plan.html#a244be09f7fb66aeaa325d33f760c2248", null ],
    [ "inv2", "structEigen_1_1internal_1_1fftw__plan.html#a15dfec869ac1461cd0764e366d61180e", null ],
    [ "set_plan", "structEigen_1_1internal_1_1fftw__plan.html#a9d0b9c05d07cda3f593c94e90de3a950", null ],
    [ "m_plan", "structEigen_1_1internal_1_1fftw__plan.html#aa428dcfe98b6ccca70762e6527db9bde", null ]
];