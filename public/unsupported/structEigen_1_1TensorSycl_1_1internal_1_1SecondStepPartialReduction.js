var structEigen_1_1TensorSycl_1_1internal_1_1SecondStepPartialReduction =
[
    [ "Op", "structEigen_1_1TensorSycl_1_1internal_1_1SecondStepPartialReduction.html#af2cab387aaab3fe23bd60498d6eb415e", null ],
    [ "OpDef", "structEigen_1_1TensorSycl_1_1internal_1_1SecondStepPartialReduction.html#a93955c7a79b6a68934238e591ee0e7b3", null ],
    [ "ScratchAccessor", "structEigen_1_1TensorSycl_1_1internal_1_1SecondStepPartialReduction.html#a81989ba8bddcb4795492ba197ed0ea5c", null ],
    [ "SecondStepPartialReduction", "structEigen_1_1TensorSycl_1_1internal_1_1SecondStepPartialReduction.html#a3785b5178e56ee9f1091e568002bf66e", null ],
    [ "operator()", "structEigen_1_1TensorSycl_1_1internal_1_1SecondStepPartialReduction.html#a818a632e090e0c3ac8387e043ba5bbd0", null ],
    [ "input_accessor", "structEigen_1_1TensorSycl_1_1internal_1_1SecondStepPartialReduction.html#acf36492898fdcc8c13aa1082331b98d2", null ],
    [ "num_coeffs_to_preserve", "structEigen_1_1TensorSycl_1_1internal_1_1SecondStepPartialReduction.html#a7fd8455afbdd93d74995b7a3d451da5e", null ],
    [ "num_coeffs_to_reduce", "structEigen_1_1TensorSycl_1_1internal_1_1SecondStepPartialReduction.html#a9d1ed8de69f631f82ffa0fc5a9b47620", null ],
    [ "op", "structEigen_1_1TensorSycl_1_1internal_1_1SecondStepPartialReduction.html#a9e67c8e6a1d4349a1d2fdb0cef72d2ea", null ],
    [ "output_accessor", "structEigen_1_1TensorSycl_1_1internal_1_1SecondStepPartialReduction.html#a02c40f01e752e3c2f09ce464e2ebcd74", null ]
];