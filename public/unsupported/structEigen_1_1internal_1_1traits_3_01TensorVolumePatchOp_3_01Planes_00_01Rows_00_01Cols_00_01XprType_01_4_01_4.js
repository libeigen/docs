var structEigen_1_1internal_1_1traits_3_01TensorVolumePatchOp_3_01Planes_00_01Rows_00_01Cols_00_01XprType_01_4_01_4 =
[
    [ "Index", "structEigen_1_1internal_1_1traits_3_01TensorVolumePatchOp_3_01Planes_00_01Rows_00_01Cols_00_01XprType_01_4_01_4.html#ade57b825e75c4cdc1a16f016dec11dba", null ],
    [ "Nested", "structEigen_1_1internal_1_1traits_3_01TensorVolumePatchOp_3_01Planes_00_01Rows_00_01Cols_00_01XprType_01_4_01_4.html#a217904d580d7971d741e125cb274bf7f", null ],
    [ "Nested_", "structEigen_1_1internal_1_1traits_3_01TensorVolumePatchOp_3_01Planes_00_01Rows_00_01Cols_00_01XprType_01_4_01_4.html#ad72a2cc84d7ce8429ee8fea13a1aa4e8", null ],
    [ "PointerType", "structEigen_1_1internal_1_1traits_3_01TensorVolumePatchOp_3_01Planes_00_01Rows_00_01Cols_00_01XprType_01_4_01_4.html#a8bb688950981ed0bbdcec8700eca1ebb", null ],
    [ "Scalar", "structEigen_1_1internal_1_1traits_3_01TensorVolumePatchOp_3_01Planes_00_01Rows_00_01Cols_00_01XprType_01_4_01_4.html#ac9b8cd44e2fb87668b1e46cc2dfba5e4", null ],
    [ "StorageKind", "structEigen_1_1internal_1_1traits_3_01TensorVolumePatchOp_3_01Planes_00_01Rows_00_01Cols_00_01XprType_01_4_01_4.html#a5cc2355b3d2d645546ca795dc56288e2", null ],
    [ "XprTraits", "structEigen_1_1internal_1_1traits_3_01TensorVolumePatchOp_3_01Planes_00_01Rows_00_01Cols_00_01XprType_01_4_01_4.html#a52834a69014797a7bb5367d89a8d2f28", null ],
    [ "Layout", "structEigen_1_1internal_1_1traits_3_01TensorVolumePatchOp_3_01Planes_00_01Rows_00_01Cols_00_01XprType_01_4_01_4.html#ad1a76ec756ed0b42dacd11f6483567e8", null ],
    [ "NumDimensions", "structEigen_1_1internal_1_1traits_3_01TensorVolumePatchOp_3_01Planes_00_01Rows_00_01Cols_00_01XprType_01_4_01_4.html#afabdde4acb518b1b2ae78ac0c2391e6a", null ]
];