var structEigen_1_1internal_1_1group__theory_1_1dimino__add__generator_3_01Multiply_00_01Equality_0028a3890c008746e97c4cd91e9da33d5d =
[
    [ "_helper", "structEigen_1_1internal_1_1group__theory_1_1dimino__add__generator.html#afe0cbd616ff11cc5d7144454985ac6c3", null ],
    [ "multiplied_elements", "structEigen_1_1internal_1_1group__theory_1_1dimino__add__generator.html#a75eb0e8b924e2e3bf734c75d43b825ee", null ],
    [ "new_elements", "structEigen_1_1internal_1_1group__theory_1_1dimino__add__generator.html#aa9203ba4903591d0fbe3680ff03f40aa", null ],
    [ "type", "structEigen_1_1internal_1_1group__theory_1_1dimino__add__generator.html#a5fa10aaef000f60fb08eeec36e97183b", null ],
    [ "type", "structEigen_1_1internal_1_1group__theory_1_1dimino__add__generator_3_01Multiply_00_01Equality_0028a3890c008746e97c4cd91e9da33d5d.html#a297ee7bee83e99dcb6c76c711e190b94", null ],
    [ "global_flags", "structEigen_1_1internal_1_1group__theory_1_1dimino__add__generator.html#a7dcd08b9ae7b1254005d79af1363b535", null ],
    [ "global_flags", "structEigen_1_1internal_1_1group__theory_1_1dimino__add__generator_3_01Multiply_00_01Equality_0028a3890c008746e97c4cd91e9da33d5d.html#a937db6032c636599ad975a7479fa70e4", null ],
    [ "rep_pos", "structEigen_1_1internal_1_1group__theory_1_1dimino__add__generator.html#a6ba640074f77cbb30bb5261acececf24", null ]
];