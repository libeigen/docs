var structEigen_1_1internal_1_1fftw__plan_3_01float_01_4 =
[
    [ "complex_type", "structEigen_1_1internal_1_1fftw__plan_3_01float_01_4.html#ae53b9a38713d59bf7915148d976db05b", null ],
    [ "scalar_type", "structEigen_1_1internal_1_1fftw__plan_3_01float_01_4.html#a007a59683cdeda85084438dbc25fbc2d", null ],
    [ "fftw_plan", "structEigen_1_1internal_1_1fftw__plan_3_01float_01_4.html#a0c550b90f9ea24c9ba4f0d2715cb0299", null ],
    [ "fwd", "structEigen_1_1internal_1_1fftw__plan_3_01float_01_4.html#a5b61a0f3bb0e5eace0598398b8cba6d0", null ],
    [ "fwd", "structEigen_1_1internal_1_1fftw__plan_3_01float_01_4.html#ac396c8a47534e2445f49dbedd6a9a215", null ],
    [ "fwd2", "structEigen_1_1internal_1_1fftw__plan_3_01float_01_4.html#a7e1dc8f284956388a99057679279d239", null ],
    [ "inv", "structEigen_1_1internal_1_1fftw__plan_3_01float_01_4.html#a78a8f341140274c6f774249a036cb0ec", null ],
    [ "inv", "structEigen_1_1internal_1_1fftw__plan_3_01float_01_4.html#aed2d74d018c7b8855ce15610352876ff", null ],
    [ "inv2", "structEigen_1_1internal_1_1fftw__plan_3_01float_01_4.html#a73bdf2658c77f24e75b244d4b2c9c2a4", null ],
    [ "set_plan", "structEigen_1_1internal_1_1fftw__plan_3_01float_01_4.html#a34fa6c9d5a36505357d7249805db3d95", null ],
    [ "m_plan", "structEigen_1_1internal_1_1fftw__plan_3_01float_01_4.html#a59dd0a3095c7035cd129979968649c73", null ]
];