var structEigen_1_1TensorSycl_1_1internal_1_1PartialReducerLauncher =
[
    [ "CoeffReturnType", "structEigen_1_1TensorSycl_1_1internal_1_1PartialReducerLauncher.html#a3ed4172bf11d65e4bf697fb43837011a", null ],
    [ "EvaluatorPointerType", "structEigen_1_1TensorSycl_1_1internal_1_1PartialReducerLauncher.html#a045f487fbf3639221bd171e6250979f8", null ],
    [ "Index", "structEigen_1_1TensorSycl_1_1internal_1_1PartialReducerLauncher.html#a2970e40e97b4981a60e4595228650894", null ],
    [ "PannelParameters", "structEigen_1_1TensorSycl_1_1internal_1_1PartialReducerLauncher.html#af7cbab6fb424f72dedc3cc991770e0e4", null ],
    [ "Storage", "structEigen_1_1TensorSycl_1_1internal_1_1PartialReducerLauncher.html#ad41f9cfe67316ba60337f97bd2912406", null ],
    [ "SyclReducerKerneType", "structEigen_1_1TensorSycl_1_1internal_1_1PartialReducerLauncher.html#a7f2ea43b0d9a485e9340323e4d5c3b4c", null ],
    [ "run", "structEigen_1_1TensorSycl_1_1internal_1_1PartialReducerLauncher.html#a0fe1c8d60b85490dfabde2b076dd15aa", null ]
];