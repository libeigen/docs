var classEigen_1_1internal_1_1companion =
[
    [ "BottomLeftBlock", "classEigen_1_1internal_1_1companion.html#ae5ab24ed5cef4a12ac7a978859cf7e06", null ],
    [ "BottomLeftDiagonal", "classEigen_1_1internal_1_1companion.html#ab9918b1d9c82660d6d0e78570cad986d", null ],
    [ "DenseCompanionMatrixType", "classEigen_1_1internal_1_1companion.html#ac4e68e244cbb3b46283c96dd767c1d0c", null ],
    [ "Index", "classEigen_1_1internal_1_1companion.html#a369a586f27453b4aa99770469ea9d738", null ],
    [ "LeftBlock", "classEigen_1_1internal_1_1companion.html#a2a771c855387612c983e85e9fa921431", null ],
    [ "LeftBlockFirstRow", "classEigen_1_1internal_1_1companion.html#a2381e95a0af3dd71528cc6b31bd1c4f1", null ],
    [ "RealScalar", "classEigen_1_1internal_1_1companion.html#a0532807c975672c19867ebadb3e627c9", null ],
    [ "RightColumn", "classEigen_1_1internal_1_1companion.html#a5134712a11c3746f8462bae8b955b55f", null ],
    [ "Scalar", "classEigen_1_1internal_1_1companion.html#af728e873e18073d0d53d2155f2338c16", null ],
    [ "companion", "classEigen_1_1internal_1_1companion.html#a17071e55718b306df698bd9832d8584b", null ],
    [ "balance", "classEigen_1_1internal_1_1companion.html#ad9a4763f11e67d223b393ae133edc487", null ],
    [ "balanced", "classEigen_1_1internal_1_1companion.html#a5c57b709e666e4a98fdb38980f9b4ed8", null ],
    [ "balancedR", "classEigen_1_1internal_1_1companion.html#ac56749378e639c5ca978fef2c22e16f3", null ],
    [ "denseMatrix", "classEigen_1_1internal_1_1companion.html#aae735dc4463234bff300585185a23c97", null ],
    [ "operator()", "classEigen_1_1internal_1_1companion.html#a40411d16db788aa90f00500187cbd6dc", null ],
    [ "setPolynomial", "classEigen_1_1internal_1_1companion.html#ade738cbd321bcf27a720b989a359eec2", null ],
    [ "m_bl_diag", "classEigen_1_1internal_1_1companion.html#aef5a7769b74bdb5aecf8f16c4027ed92", null ],
    [ "m_monic", "classEigen_1_1internal_1_1companion.html#ae6ce1a6cb017c56f2baf53de014db4c7", null ]
];