var structEigen_1_1internal_1_1traits_3_01TensorPatchOp_3_01PatchDim_00_01XprType_01_4_01_4 =
[
    [ "Index", "structEigen_1_1internal_1_1traits_3_01TensorPatchOp_3_01PatchDim_00_01XprType_01_4_01_4.html#a412a08a194c45fb7cb2ccab4fc94f6b6", null ],
    [ "Nested", "structEigen_1_1internal_1_1traits_3_01TensorPatchOp_3_01PatchDim_00_01XprType_01_4_01_4.html#a57f32677d85f3a2c10dc47692cb86905", null ],
    [ "Nested_", "structEigen_1_1internal_1_1traits_3_01TensorPatchOp_3_01PatchDim_00_01XprType_01_4_01_4.html#aa17c6fa2cd1cb71a6e9ea8012af364f6", null ],
    [ "PointerType", "structEigen_1_1internal_1_1traits_3_01TensorPatchOp_3_01PatchDim_00_01XprType_01_4_01_4.html#a7dfc094a0ffce02e6348f014b948ca0b", null ],
    [ "Scalar", "structEigen_1_1internal_1_1traits_3_01TensorPatchOp_3_01PatchDim_00_01XprType_01_4_01_4.html#a0c94ec613f1855fd6e8d3965a58edc90", null ],
    [ "StorageKind", "structEigen_1_1internal_1_1traits_3_01TensorPatchOp_3_01PatchDim_00_01XprType_01_4_01_4.html#a14afb0eceaa2e077097d0fe343bf8fa2", null ],
    [ "XprTraits", "structEigen_1_1internal_1_1traits_3_01TensorPatchOp_3_01PatchDim_00_01XprType_01_4_01_4.html#a8eb1957af61c05d00a3237789de0234c", null ],
    [ "Layout", "structEigen_1_1internal_1_1traits_3_01TensorPatchOp_3_01PatchDim_00_01XprType_01_4_01_4.html#ab786776017b7d3e8d31b69006a984795", null ],
    [ "NumDimensions", "structEigen_1_1internal_1_1traits_3_01TensorPatchOp_3_01PatchDim_00_01XprType_01_4_01_4.html#a62ce09172945593d72ef320c8cd025bc", null ]
];