var structEigen_1_1internal_1_1fftw__impl =
[
    [ "Complex", "structEigen_1_1internal_1_1fftw__impl.html#a0f31ce2d78cbd117399b46e7b480a357", null ],
    [ "int64_t", "structEigen_1_1internal_1_1fftw__impl.html#ab55bf1ce1b5ca0bcaf8c0e2ba31e52dc", null ],
    [ "PlanData", "structEigen_1_1internal_1_1fftw__impl.html#adf4d02498691e81cab1909901623f67e", null ],
    [ "PlanMap", "structEigen_1_1internal_1_1fftw__impl.html#a0255fd2336a44d0bd9b9866d97f8a16d", null ],
    [ "Scalar", "structEigen_1_1internal_1_1fftw__impl.html#acaa98dbd5ff38e42be23a4ed14115dd8", null ],
    [ "clear", "structEigen_1_1internal_1_1fftw__impl.html#a515b67dd2b0daf44ab0eeea624160251", null ],
    [ "fwd", "structEigen_1_1internal_1_1fftw__impl.html#a0bc4c3d6b9e9b0f0b6b8d788b1ab8b01", null ],
    [ "fwd", "structEigen_1_1internal_1_1fftw__impl.html#a982c6e874d1bb9a5c325311142d3b7c3", null ],
    [ "fwd2", "structEigen_1_1internal_1_1fftw__impl.html#a5fe75d3247063b27639d0508363cc1c1", null ],
    [ "get_plan", "structEigen_1_1internal_1_1fftw__impl.html#a7f8cfc2e4a67dfca8dce3b9342705230", null ],
    [ "get_plan", "structEigen_1_1internal_1_1fftw__impl.html#a4e8e8f2e1c7926cccd699aa91e6dd6f5", null ],
    [ "inv", "structEigen_1_1internal_1_1fftw__impl.html#a3ac30f22c01c87da68ac0619fea499bd", null ],
    [ "inv", "structEigen_1_1internal_1_1fftw__impl.html#af0c45ec46fc9b901ebc3261db5f1184f", null ],
    [ "inv2", "structEigen_1_1internal_1_1fftw__impl.html#a0b372dec0723f44da9110c7c1292bb3d", null ],
    [ "m_plans", "structEigen_1_1internal_1_1fftw__impl.html#aba98b45eaa980068a8104deb5521c365", null ]
];