var TensorImagePatch_8h =
[
    [ "Eigen::internal::eval< TensorImagePatchOp< Rows, Cols, XprType >, Eigen::Dense >", "../structEigen_1_1internal_1_1eval_3_01TensorImagePatchOp_3_01Rows_00_01Cols_00_01XprType_01_4_00_01Eigen_1_1Dense_01_4.html", null ],
    [ "Eigen::internal::ImagePatchCopyOp< Self, Vectorizable >", "structEigen_1_1internal_1_1ImagePatchCopyOp.html", "structEigen_1_1internal_1_1ImagePatchCopyOp" ],
    [ "Eigen::internal::ImagePatchCopyOp< Self, true >", "structEigen_1_1internal_1_1ImagePatchCopyOp_3_01Self_00_01true_01_4.html", "structEigen_1_1internal_1_1ImagePatchCopyOp_3_01Self_00_01true_01_4" ],
    [ "Eigen::internal::ImagePatchPaddingOp< Self >", "structEigen_1_1internal_1_1ImagePatchPaddingOp.html", "structEigen_1_1internal_1_1ImagePatchPaddingOp" ],
    [ "Eigen::internal::nested< TensorImagePatchOp< Rows, Cols, XprType >, 1, typename eval< TensorImagePatchOp< Rows, Cols, XprType > >::type >", "structEigen_1_1internal_1_1nested_3_01TensorImagePatchOp_3_01Rows_00_01Cols_00_01XprType_01_4_0045e83793bea174a49910656804ba267a.html", "structEigen_1_1internal_1_1nested_3_01TensorImagePatchOp_3_01Rows_00_01Cols_00_01XprType_01_4_0045e83793bea174a49910656804ba267a" ],
    [ "Eigen::TensorEvaluator< const TensorImagePatchOp< Rows, Cols, ArgType >, Device >", "structEigen_1_1TensorEvaluator_3_01const_01TensorImagePatchOp_3_01Rows_00_01Cols_00_01ArgType_01_4_00_01Device_01_4.html", "structEigen_1_1TensorEvaluator_3_01const_01TensorImagePatchOp_3_01Rows_00_01Cols_00_01ArgType_01_4_00_01Device_01_4" ],
    [ "Eigen::TensorImagePatchOp< Rows, Cols, XprType >", "classEigen_1_1TensorImagePatchOp.html", "classEigen_1_1TensorImagePatchOp" ],
    [ "Eigen::internal::traits< TensorImagePatchOp< Rows, Cols, XprType > >", "../structEigen_1_1internal_1_1traits_3_01TensorImagePatchOp_3_01Rows_00_01Cols_00_01XprType_01_4_01_4.html", null ]
];