var structEigen_1_1internal_1_1group__theory_1_1dimino__add__all__coset__spaces =
[
    [ "_ac4r", "structEigen_1_1internal_1_1group__theory_1_1dimino__add__all__coset__spaces.html#ab7f1ea8eef204f71db4bb48ac7493b71", null ],
    [ "_helper", "structEigen_1_1internal_1_1group__theory_1_1dimino__add__all__coset__spaces.html#a04f7a40093100eb9dd249b8d8044dcb0", null ],
    [ "new_elements", "structEigen_1_1internal_1_1group__theory_1_1dimino__add__all__coset__spaces.html#af0fa4e28d09354873ed510ad7b4740dd", null ],
    [ "rep_element", "structEigen_1_1internal_1_1group__theory_1_1dimino__add__all__coset__spaces.html#ac3f58e92ee2436e051e62a52256653d1", null ],
    [ "type", "structEigen_1_1internal_1_1group__theory_1_1dimino__add__all__coset__spaces.html#a75320f83307a37a34ab998b899c0af2c", null ],
    [ "global_flags", "structEigen_1_1internal_1_1group__theory_1_1dimino__add__all__coset__spaces.html#a0ac8d3c4da71e1675c1cf543214698e3", null ],
    [ "new_rep_pos", "structEigen_1_1internal_1_1group__theory_1_1dimino__add__all__coset__spaces.html#a2413d5b82186685a8947f5cb07ba2c50", null ],
    [ "new_stop_condition", "structEigen_1_1internal_1_1group__theory_1_1dimino__add__all__coset__spaces.html#a01f137f70209ed6fb7f2364205b9fb25", null ]
];