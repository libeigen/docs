var structEigen_1_1internal_1_1imklfft_1_1plan =
[
    [ "complex_type", "structEigen_1_1internal_1_1imklfft_1_1plan.html#a43eb83747513b33fe42ead3ccfc34e02", null ],
    [ "scalar_type", "structEigen_1_1internal_1_1imklfft_1_1plan.html#aec52bbd95c0392c0f33092fb3b6155de", null ],
    [ "plan", "structEigen_1_1internal_1_1imklfft_1_1plan.html#a7fa9019b57c0f4d7a24c2742cb6a8ee8", null ],
    [ "forward", "structEigen_1_1internal_1_1imklfft_1_1plan.html#ad30de0edb5e65d69889ef8288033c739", null ],
    [ "forward", "structEigen_1_1internal_1_1imklfft_1_1plan.html#adf839cf1fb6dcd1ba4f5ba002304b096", null ],
    [ "forward2", "structEigen_1_1internal_1_1imklfft_1_1plan.html#ac6b460707ea986e018e9e833ae23faa8", null ],
    [ "inverse", "structEigen_1_1internal_1_1imklfft_1_1plan.html#a80f6f4af89d8bd66f799b4025720a30f", null ],
    [ "inverse", "structEigen_1_1internal_1_1imklfft_1_1plan.html#a6110a59b6c80885a1872d04105f48343", null ],
    [ "inverse2", "structEigen_1_1internal_1_1imklfft_1_1plan.html#a0ad9bd2692e7495abbd18156deff967e", null ],
    [ "m_plan", "structEigen_1_1internal_1_1imklfft_1_1plan.html#a45745758527894d53e48474d03c7cbf5", null ],
    [ "precision", "structEigen_1_1internal_1_1imklfft_1_1plan.html#a2ac7d2ac0f71b8f73fd62aed8320069c", null ]
];