var structEigen_1_1TensorSycl_1_1internal_1_1OpDefiner_3_01Eigen_1_1internal_1_1MeanReducer_3_01Coef229f0933a14933e81873ef457ddf9c89 =
[
    [ "PacketReturnType", "structEigen_1_1TensorSycl_1_1internal_1_1OpDefiner.html#ac3377a7382a65e1be4362ed0aac75ce8", null ],
    [ "PacketReturnType", "structEigen_1_1TensorSycl_1_1internal_1_1OpDefiner_3_01Eigen_1_1internal_1_1MeanReducer_3_01Coef229f0933a14933e81873ef457ddf9c89.html#a2581ccbf2c7556f6c2e876ba9b3853ae", null ],
    [ "type", "structEigen_1_1TensorSycl_1_1internal_1_1OpDefiner.html#aab5c1eaabba46b27d16d897c615f31d7", null ],
    [ "type", "structEigen_1_1TensorSycl_1_1internal_1_1OpDefiner_3_01Eigen_1_1internal_1_1MeanReducer_3_01Coef229f0933a14933e81873ef457ddf9c89.html#a80ffb99372626223ea3a6aa4c2cc96c3", null ],
    [ "finalise_op", "structEigen_1_1TensorSycl_1_1internal_1_1OpDefiner.html#ac3f17530a4a3f2a9bf4824004ae66bec", null ],
    [ "finalise_op", "structEigen_1_1TensorSycl_1_1internal_1_1OpDefiner_3_01Eigen_1_1internal_1_1MeanReducer_3_01Coef229f0933a14933e81873ef457ddf9c89.html#a013dc403a1a24be6e2cb4e1c9671ce34", null ],
    [ "get_op", "structEigen_1_1TensorSycl_1_1internal_1_1OpDefiner_3_01Eigen_1_1internal_1_1MeanReducer_3_01Coef229f0933a14933e81873ef457ddf9c89.html#a9725641ada47c5563d71a5b38472f56d", null ],
    [ "get_op", "structEigen_1_1TensorSycl_1_1internal_1_1OpDefiner.html#abbc30f819161d59de33efc955f98c536", null ]
];