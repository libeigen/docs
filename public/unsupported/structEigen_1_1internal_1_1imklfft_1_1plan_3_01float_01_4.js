var structEigen_1_1internal_1_1imklfft_1_1plan_3_01float_01_4 =
[
    [ "complex_type", "structEigen_1_1internal_1_1imklfft_1_1plan_3_01float_01_4.html#a9a3e1036d62f742dd40b18c8d1342d3a", null ],
    [ "scalar_type", "structEigen_1_1internal_1_1imklfft_1_1plan_3_01float_01_4.html#a3bafb6894d91514580bc8ecd2420778d", null ],
    [ "plan", "structEigen_1_1internal_1_1imklfft_1_1plan_3_01float_01_4.html#a4188e28c2f256e9473052392b33ffebc", null ],
    [ "forward", "structEigen_1_1internal_1_1imklfft_1_1plan_3_01float_01_4.html#a9044fb418dc668dcc2edfc7664d650a2", null ],
    [ "forward", "structEigen_1_1internal_1_1imklfft_1_1plan_3_01float_01_4.html#a0b6ca5ac35c9d6acf1046ef90abc1a6a", null ],
    [ "forward2", "structEigen_1_1internal_1_1imklfft_1_1plan_3_01float_01_4.html#adc4c0b20e27d4b7ef9994f6ca98525b8", null ],
    [ "inverse", "structEigen_1_1internal_1_1imklfft_1_1plan_3_01float_01_4.html#ad61c2d1334ecf9f0aa08c4d61a82e48d", null ],
    [ "inverse", "structEigen_1_1internal_1_1imklfft_1_1plan_3_01float_01_4.html#ad6e7a62fffb9a24d88ea5be7cca4eb8e", null ],
    [ "inverse2", "structEigen_1_1internal_1_1imklfft_1_1plan_3_01float_01_4.html#a426d4fe9aa449122f88b5da91bf685f8", null ],
    [ "m_plan", "structEigen_1_1internal_1_1imklfft_1_1plan_3_01float_01_4.html#a07f2bbe0081ada804d62df63713cf180", null ],
    [ "precision", "structEigen_1_1internal_1_1imklfft_1_1plan_3_01float_01_4.html#a19977f397a5d6a179b5fb0d3e4cc9739", null ]
];