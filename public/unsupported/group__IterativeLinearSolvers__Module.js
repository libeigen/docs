var group__IterativeLinearSolvers__Module =
[
    [ "Eigen::DGMRES< MatrixType_, Preconditioner_ >", "classEigen_1_1DGMRES.html", [
      [ "DGMRES", "classEigen_1_1DGMRES.html#a60b9241a708ddfd7eb4ff4950faea18c", null ],
      [ "DGMRES", "classEigen_1_1DGMRES.html#a38cf741dbc55f3fa4f1a8e0d16713aab", null ],
      [ "deflSize", "classEigen_1_1DGMRES.html#ab1b05e529615d45f3b8c070edfeb82cc", null ],
      [ "dgmres", "classEigen_1_1DGMRES.html#a8a47ed3ddd5ac039f9d368c87287a807", null ],
      [ "dgmresCycle", "classEigen_1_1DGMRES.html#a1db4ed4c02a85b9479b67be692ade42f", null ],
      [ "restart", "classEigen_1_1DGMRES.html#a2bd9a6bfc4530cc6d1c1e3312c731bcc", null ],
      [ "set_restart", "classEigen_1_1DGMRES.html#a9732e699838ca2745486fc355eda36e7", null ],
      [ "setEigenv", "classEigen_1_1DGMRES.html#abc45dbf1b2dc85405047157c753db90e", null ],
      [ "setMaxEigenv", "classEigen_1_1DGMRES.html#a5fe9096f6b96a387b521a866fe344de1", null ]
    ] ],
    [ "Eigen::GMRES< MatrixType_, Preconditioner_ >", "classEigen_1_1GMRES.html", [
      [ "GMRES", "classEigen_1_1GMRES.html#aed7949295a45e7ccdd29bcd4c7876f95", null ],
      [ "GMRES", "classEigen_1_1GMRES.html#a423bdc8dc8fcb9f7ce90c07b118721bb", null ],
      [ "get_restart", "classEigen_1_1GMRES.html#aa5a8e920807eb25d8ab21922ae132e1f", null ],
      [ "set_restart", "classEigen_1_1GMRES.html#afcf731e53a16a93077efdd63118c27a5", null ]
    ] ],
    [ "Eigen::IDRS< MatrixType_, Preconditioner_ >", "classEigen_1_1IDRS.html", [
      [ "IDRS", "classEigen_1_1IDRS.html#a029d9925395d811e62939d5aacc3dbc3", null ],
      [ "IDRS", "classEigen_1_1IDRS.html#a3407644df005d4a74ce7e8ca9f03c320", null ],
      [ "_solve_vector_with_guess_impl", "classEigen_1_1IDRS.html#ab75c5ed081568173f7c32b18c1e7c013", null ],
      [ "setAngle", "classEigen_1_1IDRS.html#afb154a59c37ef4411391cb3b19aed4c9", null ],
      [ "setResidualUpdate", "classEigen_1_1IDRS.html#a002e59270919dcb1ad6f70de3e900dcf", null ],
      [ "setS", "classEigen_1_1IDRS.html#aeb11b800a0216ebf60560ec7f7e0e76a", null ],
      [ "setSmoothing", "classEigen_1_1IDRS.html#ad6f2fc19379d881e15d8c63f6ca8951a", null ]
    ] ],
    [ "Eigen::IDRSTABL< MatrixType_, Preconditioner_ >", "classEigen_1_1IDRSTABL.html", [
      [ "IDRSTABL", "classEigen_1_1IDRSTABL.html#a3c25cf3ea47ea97a8c1b52cf465f1ae0", null ],
      [ "IDRSTABL", "classEigen_1_1IDRSTABL.html#a7618647d726f62a5850ff5355980ffd1", null ],
      [ "_solve_vector_with_guess_impl", "classEigen_1_1IDRSTABL.html#ad77080d2d09e6e71c1302f535e26f03b", null ],
      [ "setL", "classEigen_1_1IDRSTABL.html#a1215cc6c5165fc7cd33a6de056e79fd5", null ],
      [ "setS", "classEigen_1_1IDRSTABL.html#a57d1018bcbff8e316caa486a42537615", null ]
    ] ],
    [ "Eigen::IterScaling< MatrixType_ >", "classEigen_1_1IterScaling.html", [
      [ "compute", "classEigen_1_1IterScaling.html#a20438dc249686aa7002e6b2ff1d69ac1", null ],
      [ "computeRef", "classEigen_1_1IterScaling.html#a4622fbf83a895d87bb93538279325715", null ],
      [ "LeftScaling", "classEigen_1_1IterScaling.html#a415d147b5da2ada15546ca1f585cc91f", null ],
      [ "RightScaling", "classEigen_1_1IterScaling.html#afe29d4c1a146eda68882369f5a72d428", null ],
      [ "setTolerance", "classEigen_1_1IterScaling.html#a16486601c80084f64814d8fc6046de99", null ]
    ] ],
    [ "Eigen::MINRES< MatrixType_, UpLo_, Preconditioner_ >", "classEigen_1_1MINRES.html", [
      [ "MINRES", "classEigen_1_1MINRES.html#a11841032b43f03d27dd2817c944428f5", null ],
      [ "MINRES", "classEigen_1_1MINRES.html#a772d4c93859e7b26ab974820e37beddd", null ],
      [ "~MINRES", "classEigen_1_1MINRES.html#aae1a169d6dd1cd81f769c6bae0f450be", null ]
    ] ]
];