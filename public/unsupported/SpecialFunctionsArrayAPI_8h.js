var SpecialFunctionsArrayAPI_8h =
[
    [ "Eigen::betainc", "namespaceEigen.html#a81f796c95561b9847eb8a6b8ccfa3e7d", null ],
    [ "Eigen::gamma_sample_der_alpha", "namespaceEigen.html#aaee481f967313d3a29e9f6dd3d3c0de3", null ],
    [ "Eigen::igamma", "namespaceEigen.html#af5aa651137636b1cdbd27de1cfe91148", null ],
    [ "Eigen::igamma_der_a", "namespaceEigen.html#a1d51aa508a6f5d6791754fc48fb44f88", null ],
    [ "Eigen::igammac", "namespaceEigen.html#a1abaa2ff8c7b1871eaf026a47c6bbf3b", null ],
    [ "Eigen::polygamma", "namespaceEigen.html#ae3b47a13a0699f5dbaa0623c11333dca", null ],
    [ "Eigen::zeta", "namespaceEigen.html#af9555e27540da78d2c4bdd17d3b750b1", null ]
];