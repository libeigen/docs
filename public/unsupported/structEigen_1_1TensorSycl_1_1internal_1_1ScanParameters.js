var structEigen_1_1TensorSycl_1_1internal_1_1ScanParameters =
[
    [ "ScanParameters", "structEigen_1_1TensorSycl_1_1internal_1_1ScanParameters.html#aafcb320c6bf19de83ebfbd927758aae6", null ],
    [ "block_threads", "structEigen_1_1TensorSycl_1_1internal_1_1ScanParameters.html#a74ebddcc69d839ce1f5c6bd53ab07969", null ],
    [ "elements_per_block", "structEigen_1_1TensorSycl_1_1internal_1_1ScanParameters.html#acbe86f47242c3a9085b59b42d9878a44", null ],
    [ "elements_per_group", "structEigen_1_1TensorSycl_1_1internal_1_1ScanParameters.html#ac47a190fbd65b3bcb720c20c02e4d161", null ],
    [ "group_threads", "structEigen_1_1TensorSycl_1_1internal_1_1ScanParameters.html#a7be3fe65de259a9109a1d422de5cf872", null ],
    [ "loop_range", "structEigen_1_1TensorSycl_1_1internal_1_1ScanParameters.html#af9566d2e7df452e6476b95d99bf5184d", null ],
    [ "non_scan_size", "structEigen_1_1TensorSycl_1_1internal_1_1ScanParameters.html#a6e2d04df337c8b10e2efed200c6b3d8f", null ],
    [ "non_scan_stride", "structEigen_1_1TensorSycl_1_1internal_1_1ScanParameters.html#a2788dc1fe85de3b0936864a826281f3a", null ],
    [ "panel_threads", "structEigen_1_1TensorSycl_1_1internal_1_1ScanParameters.html#aab0eefd6164d0674ded0963ebd7fbeda", null ],
    [ "scan_size", "structEigen_1_1TensorSycl_1_1internal_1_1ScanParameters.html#af182575deae2e1b2847b458ddc39222e", null ],
    [ "scan_stride", "structEigen_1_1TensorSycl_1_1internal_1_1ScanParameters.html#a5b533fcfa4eaa737622bda1bbaf49a8f", null ],
    [ "ScanPerThread", "structEigen_1_1TensorSycl_1_1internal_1_1ScanParameters.html#a7695d32c90294fc7b2ec8a89a06aa681", null ],
    [ "total_size", "structEigen_1_1TensorSycl_1_1internal_1_1ScanParameters.html#a5cb4100a3872f5927bfd2eb2627a7e12", null ]
];