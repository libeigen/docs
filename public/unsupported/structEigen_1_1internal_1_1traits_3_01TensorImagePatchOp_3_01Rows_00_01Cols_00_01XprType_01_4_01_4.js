var structEigen_1_1internal_1_1traits_3_01TensorImagePatchOp_3_01Rows_00_01Cols_00_01XprType_01_4_01_4 =
[
    [ "Index", "structEigen_1_1internal_1_1traits_3_01TensorImagePatchOp_3_01Rows_00_01Cols_00_01XprType_01_4_01_4.html#a2c78b5f3ccd20cf49beca1be78ae780d", null ],
    [ "Nested", "structEigen_1_1internal_1_1traits_3_01TensorImagePatchOp_3_01Rows_00_01Cols_00_01XprType_01_4_01_4.html#a193eab2356f8716c5b485c55eda69145", null ],
    [ "Nested_", "structEigen_1_1internal_1_1traits_3_01TensorImagePatchOp_3_01Rows_00_01Cols_00_01XprType_01_4_01_4.html#add7748c7cee1ba28d006b5fb7922e108", null ],
    [ "PointerType", "structEigen_1_1internal_1_1traits_3_01TensorImagePatchOp_3_01Rows_00_01Cols_00_01XprType_01_4_01_4.html#a482a43182c8a43961958158fe8240248", null ],
    [ "Scalar", "structEigen_1_1internal_1_1traits_3_01TensorImagePatchOp_3_01Rows_00_01Cols_00_01XprType_01_4_01_4.html#a11e2107f55132ec112c0a31b20c491a4", null ],
    [ "StorageKind", "structEigen_1_1internal_1_1traits_3_01TensorImagePatchOp_3_01Rows_00_01Cols_00_01XprType_01_4_01_4.html#a065d9bb51137b04164f2bf45a1a64959", null ],
    [ "XprTraits", "structEigen_1_1internal_1_1traits_3_01TensorImagePatchOp_3_01Rows_00_01Cols_00_01XprType_01_4_01_4.html#a441dac5fa8f874f943ac794d733356fe", null ],
    [ "Layout", "structEigen_1_1internal_1_1traits_3_01TensorImagePatchOp_3_01Rows_00_01Cols_00_01XprType_01_4_01_4.html#afe81aefcd93f45f5eed9e5beee0326c8", null ],
    [ "NumDimensions", "structEigen_1_1internal_1_1traits_3_01TensorImagePatchOp_3_01Rows_00_01Cols_00_01XprType_01_4_01_4.html#acbfde5735685e04333b6682076ea8d0a", null ]
];