var structEigen_1_1internal_1_1FullReducer_3_01Self_00_01Op_00_01Eigen_1_1SyclDevice_00_01Vectorizable_01_4 =
[
    [ "CoeffReturnType", "structEigen_1_1internal_1_1FullReducer_3_01Self_00_01Op_00_01Eigen_1_1SyclDevice_00_01Vectorizable_01_4.html#a73dc74d6aa46bc829668d7e21755b436", null ],
    [ "EvaluatorPointerType", "structEigen_1_1internal_1_1FullReducer_3_01Self_00_01Op_00_01Eigen_1_1SyclDevice_00_01Vectorizable_01_4.html#ac2ba1dfbb2281a3d9c6d9adaec7067ca", null ],
    [ "run", "structEigen_1_1internal_1_1FullReducer.html#a749627426e41e1961e0c18ac2c55a51a", null ],
    [ "run", "structEigen_1_1internal_1_1FullReducer_3_01Self_00_01Op_00_01Eigen_1_1SyclDevice_00_01Vectorizable_01_4.html#a96402e935d45557d0cf0be1eefc03eb0", null ],
    [ "HasOptimizedImplementation", "structEigen_1_1internal_1_1FullReducer.html#a9d2a99ad6a2eca84e68c74fb81b6a5fb", null ],
    [ "HasOptimizedImplementation", "structEigen_1_1internal_1_1FullReducer_3_01Self_00_01Op_00_01Eigen_1_1SyclDevice_00_01Vectorizable_01_4.html#ad86c9fea303e7f2bed3ff6a0e463dd7e", null ],
    [ "PacketSize", "structEigen_1_1internal_1_1FullReducer_3_01Self_00_01Op_00_01Eigen_1_1SyclDevice_00_01Vectorizable_01_4.html#a5b510b32ed916c701ebf00fd6b4bf3a8", null ]
];