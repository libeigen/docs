var structEigen_1_1internal_1_1group__theory_1_1dimino__add__cosets__for__rep_3_01Multiply_00_01Equae60b56781226d2dad4792f645af0641e =
[
    [ "_cil", "structEigen_1_1internal_1_1group__theory_1_1dimino__add__cosets__for__rep_3_01Multiply_00_01Equae60b56781226d2dad4792f645af0641e.html#aa1ad822a7c954f50c68f4816d51a5379", null ],
    [ "_helper", "structEigen_1_1internal_1_1group__theory_1_1dimino__add__cosets__for__rep_3_01Multiply_00_01Equae60b56781226d2dad4792f645af0641e.html#a66de775f910909c2caf735175e2d8a41", null ],
    [ "coset_elements", "structEigen_1_1internal_1_1group__theory_1_1dimino__add__cosets__for__rep_3_01Multiply_00_01Equae60b56781226d2dad4792f645af0641e.html#ac0f308f8370263b654564531601b883a", null ],
    [ "new_coset_rep", "structEigen_1_1internal_1_1group__theory_1_1dimino__add__cosets__for__rep_3_01Multiply_00_01Equae60b56781226d2dad4792f645af0641e.html#a25321743f0c783656afc3661325077bc", null ],
    [ "type", "structEigen_1_1internal_1_1group__theory_1_1dimino__add__cosets__for__rep_3_01Multiply_00_01Equae60b56781226d2dad4792f645af0641e.html#aacbd0c209eacd22d83f250546df29512", null ],
    [ "add_coset", "structEigen_1_1internal_1_1group__theory_1_1dimino__add__cosets__for__rep_3_01Multiply_00_01Equae60b56781226d2dad4792f645af0641e.html#a4f974330b86e92baa19b5f87026f78ae", null ],
    [ "global_flags", "structEigen_1_1internal_1_1group__theory_1_1dimino__add__cosets__for__rep_3_01Multiply_00_01Equae60b56781226d2dad4792f645af0641e.html#aa71c801e8e4310022bab5c3c70b4331b", null ]
];