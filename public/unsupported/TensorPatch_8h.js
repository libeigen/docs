var TensorPatch_8h =
[
    [ "Eigen::internal::eval< TensorPatchOp< PatchDim, XprType >, Eigen::Dense >", "../structEigen_1_1internal_1_1eval_3_01TensorPatchOp_3_01PatchDim_00_01XprType_01_4_00_01Eigen_1_1Dense_01_4.html", null ],
    [ "Eigen::internal::nested< TensorPatchOp< PatchDim, XprType >, 1, typename eval< TensorPatchOp< PatchDim, XprType > >::type >", "structEigen_1_1internal_1_1nested_3_01TensorPatchOp_3_01PatchDim_00_01XprType_01_4_00_011_00_01t47333c9dea79634dbd71ad5da0485159.html", "structEigen_1_1internal_1_1nested_3_01TensorPatchOp_3_01PatchDim_00_01XprType_01_4_00_011_00_01t47333c9dea79634dbd71ad5da0485159" ],
    [ "Eigen::TensorEvaluator< const TensorPatchOp< PatchDim, ArgType >, Device >", "structEigen_1_1TensorEvaluator_3_01const_01TensorPatchOp_3_01PatchDim_00_01ArgType_01_4_00_01Device_01_4.html", "structEigen_1_1TensorEvaluator_3_01const_01TensorPatchOp_3_01PatchDim_00_01ArgType_01_4_00_01Device_01_4" ],
    [ "Eigen::TensorPatchOp< PatchDim, XprType >", "classEigen_1_1TensorPatchOp.html", "classEigen_1_1TensorPatchOp" ],
    [ "Eigen::internal::traits< TensorPatchOp< PatchDim, XprType > >", "../structEigen_1_1internal_1_1traits_3_01TensorPatchOp_3_01PatchDim_00_01XprType_01_4_01_4.html", null ]
];