var structEigen_1_1internal_1_1kissfft__impl =
[
    [ "Complex", "structEigen_1_1internal_1_1kissfft__impl.html#ac9a4bf7a3b32442233170a35ba6a3be5", null ],
    [ "PlanData", "structEigen_1_1internal_1_1kissfft__impl.html#af4db2b24f0c3c8edc46f7f7e31f244f0", null ],
    [ "PlanMap", "structEigen_1_1internal_1_1kissfft__impl.html#a82cf35cc1f1e081db5c80da08d8d86fb", null ],
    [ "Scalar", "structEigen_1_1internal_1_1kissfft__impl.html#a096869d0ace9a6b0b7b9c27632eae20e", null ],
    [ "clear", "structEigen_1_1internal_1_1kissfft__impl.html#a90c50bb2c4068baaaabd9fdd0c3d920a", null ],
    [ "fwd", "structEigen_1_1internal_1_1kissfft__impl.html#a31cb6de44d34fae5e70ea155bb9d1106", null ],
    [ "fwd", "structEigen_1_1internal_1_1kissfft__impl.html#a152b5e4e81ab3d987c3e6c195c8e87b3", null ],
    [ "fwd2", "structEigen_1_1internal_1_1kissfft__impl.html#a41276379c35ca29ac95c5d56429cfa00", null ],
    [ "get_plan", "structEigen_1_1internal_1_1kissfft__impl.html#a7158dc80e4eedec876182885b5255b5d", null ],
    [ "inv", "structEigen_1_1internal_1_1kissfft__impl.html#a4f42820f2c4293cd5f3f662af7507bb2", null ],
    [ "inv", "structEigen_1_1internal_1_1kissfft__impl.html#a8ef60026044c4b437456939ceeab3a1b", null ],
    [ "inv2", "structEigen_1_1internal_1_1kissfft__impl.html#a8c3dde47f49f72e8c6262b49ea1a9bd6", null ],
    [ "PlanKey", "structEigen_1_1internal_1_1kissfft__impl.html#a30c663be77ea0b5c0effaa0237b200d1", null ],
    [ "real_twiddles", "structEigen_1_1internal_1_1kissfft__impl.html#ac76c0ebec683deda60d8575045d144e6", null ],
    [ "m_plans", "structEigen_1_1internal_1_1kissfft__impl.html#a1d51a9533cfcf40a54a6333f5b3c2877", null ],
    [ "m_realTwiddles", "structEigen_1_1internal_1_1kissfft__impl.html#a8e34a759d19e143597a0b462db0ba16f", null ],
    [ "m_tmpBuf1", "structEigen_1_1internal_1_1kissfft__impl.html#ac332234332a3350a182296e097418428", null ],
    [ "m_tmpBuf2", "structEigen_1_1internal_1_1kissfft__impl.html#a25153e933dd0b6feb5935898a90a6383", null ]
];