var structEigen_1_1internal_1_1traits_3_01TensorRollOp_3_01RollDimensions_00_01XprType_01_4_01_4 =
[
    [ "Index", "structEigen_1_1internal_1_1traits_3_01TensorRollOp_3_01RollDimensions_00_01XprType_01_4_01_4.html#aee555bc5b38e350dc49b3a3fa24f158e", null ],
    [ "Nested", "structEigen_1_1internal_1_1traits_3_01TensorRollOp_3_01RollDimensions_00_01XprType_01_4_01_4.html#ab0693b75fd5a022306d4a6cff89c9cd9", null ],
    [ "Nested_", "structEigen_1_1internal_1_1traits_3_01TensorRollOp_3_01RollDimensions_00_01XprType_01_4_01_4.html#ab29b63006883bf69f0912b035bfa664b", null ],
    [ "PointerType", "structEigen_1_1internal_1_1traits_3_01TensorRollOp_3_01RollDimensions_00_01XprType_01_4_01_4.html#a2a31afd88ccb3ece09740507391dfbe1", null ],
    [ "Scalar", "structEigen_1_1internal_1_1traits_3_01TensorRollOp_3_01RollDimensions_00_01XprType_01_4_01_4.html#a98dccf95d8d6ae175b91bd77acdfa9db", null ],
    [ "StorageKind", "structEigen_1_1internal_1_1traits_3_01TensorRollOp_3_01RollDimensions_00_01XprType_01_4_01_4.html#aeecec59f1216ddf117e291e53f7a8ce5", null ],
    [ "XprTraits", "structEigen_1_1internal_1_1traits_3_01TensorRollOp_3_01RollDimensions_00_01XprType_01_4_01_4.html#ac549d1696b670bd4db458a0627c5d0ef", null ],
    [ "Layout", "structEigen_1_1internal_1_1traits_3_01TensorRollOp_3_01RollDimensions_00_01XprType_01_4_01_4.html#a0b259be39ff067f6093ac3c9043112c8", null ],
    [ "NumDimensions", "structEigen_1_1internal_1_1traits_3_01TensorRollOp_3_01RollDimensions_00_01XprType_01_4_01_4.html#a12ef43deda462e8a3aaf361fa99d8ad2", null ]
];