var structEigen_1_1internal_1_1traits_3_01TensorMap_3_01PlainObjectType_00_01Options___00_01MakePointer___01_4_01_4 =
[
    [ "MakePointer", "structEigen_1_1internal_1_1traits_3_01TensorMap_3_01PlainObjectType_00_01Options___00_01MakePointer___01_4_01_4_1_1MakePointer.html", "structEigen_1_1internal_1_1traits_3_01TensorMap_3_01PlainObjectType_00_01Options___00_01MakePointer___01_4_01_4_1_1MakePointer" ],
    [ "BaseTraits", "structEigen_1_1internal_1_1traits_3_01TensorMap_3_01PlainObjectType_00_01Options___00_01MakePointer___01_4_01_4.html#a260e8c89efd842905ac9900b1c09dca7", null ],
    [ "Index", "structEigen_1_1internal_1_1traits_3_01TensorMap_3_01PlainObjectType_00_01Options___00_01MakePointer___01_4_01_4.html#a459f9b033aeea0188a31fbf841ab12da", null ],
    [ "PointerType", "structEigen_1_1internal_1_1traits_3_01TensorMap_3_01PlainObjectType_00_01Options___00_01MakePointer___01_4_01_4.html#ada2dafc566584bcf89895e6cd03cf763", null ],
    [ "Scalar", "structEigen_1_1internal_1_1traits_3_01TensorMap_3_01PlainObjectType_00_01Options___00_01MakePointer___01_4_01_4.html#a283d98f1192003ea9488a32019358cc4", null ],
    [ "StorageKind", "structEigen_1_1internal_1_1traits_3_01TensorMap_3_01PlainObjectType_00_01Options___00_01MakePointer___01_4_01_4.html#a3e8117ad34e23efd91e29f321ce40ad5", null ],
    [ "Layout", "structEigen_1_1internal_1_1traits_3_01TensorMap_3_01PlainObjectType_00_01Options___00_01MakePointer___01_4_01_4.html#ae1cdd14aa46b782be290abd1b20d1034", null ],
    [ "NumDimensions", "structEigen_1_1internal_1_1traits_3_01TensorMap_3_01PlainObjectType_00_01Options___00_01MakePointer___01_4_01_4.html#aeed9d3bc531a638d0700c1e346370b54", null ]
];