var structEigen_1_1NumTraits_3_01AutoDiffScalar_3_01DerType_01_4_01_4 =
[
    [ "DerTypeCleaned", "structEigen_1_1NumTraits_3_01AutoDiffScalar_3_01DerType_01_4_01_4.html#a5ba94d98482c00664be769f5a11ee2eb", null ],
    [ "Literal", "structEigen_1_1NumTraits_3_01AutoDiffScalar_3_01DerType_01_4_01_4.html#a46644eb8537112df749ccc97d54057bc", null ],
    [ "Nested", "structEigen_1_1NumTraits_3_01AutoDiffScalar_3_01DerType_01_4_01_4.html#ada3e807a032847e41f78cdc845b68572", null ],
    [ "NonInteger", "structEigen_1_1NumTraits_3_01AutoDiffScalar_3_01DerType_01_4_01_4.html#a189d1370ef670215ec26db536be0575a", null ],
    [ "Real", "structEigen_1_1NumTraits_3_01AutoDiffScalar_3_01DerType_01_4_01_4.html#a98a33e2b7fcb0b3ea3faa982952a4c35", null ]
];