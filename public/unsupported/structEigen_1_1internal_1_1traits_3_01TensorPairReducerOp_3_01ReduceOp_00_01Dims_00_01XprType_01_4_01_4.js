var structEigen_1_1internal_1_1traits_3_01TensorPairReducerOp_3_01ReduceOp_00_01Dims_00_01XprType_01_4_01_4 =
[
    [ "Index", "structEigen_1_1internal_1_1traits_3_01TensorPairReducerOp_3_01ReduceOp_00_01Dims_00_01XprType_01_4_01_4.html#a1d95ebdfe71e7300476c25384afc8f2a", null ],
    [ "Nested", "structEigen_1_1internal_1_1traits_3_01TensorPairReducerOp_3_01ReduceOp_00_01Dims_00_01XprType_01_4_01_4.html#afa635fe1a6f6abc109a587561be26274", null ],
    [ "Nested_", "structEigen_1_1internal_1_1traits_3_01TensorPairReducerOp_3_01ReduceOp_00_01Dims_00_01XprType_01_4_01_4.html#a63ce1806b52e13230703a876947ff20b", null ],
    [ "Scalar", "structEigen_1_1internal_1_1traits_3_01TensorPairReducerOp_3_01ReduceOp_00_01Dims_00_01XprType_01_4_01_4.html#af42cfa0fe095c04c87fccf225733fecc", null ],
    [ "StorageKind", "structEigen_1_1internal_1_1traits_3_01TensorPairReducerOp_3_01ReduceOp_00_01Dims_00_01XprType_01_4_01_4.html#ab06fdc93c454c7a4eec7518a74a32820", null ],
    [ "XprTraits", "structEigen_1_1internal_1_1traits_3_01TensorPairReducerOp_3_01ReduceOp_00_01Dims_00_01XprType_01_4_01_4.html#af03694c51b8a87fc532d69d98a23c6b9", null ],
    [ "Layout", "structEigen_1_1internal_1_1traits_3_01TensorPairReducerOp_3_01ReduceOp_00_01Dims_00_01XprType_01_4_01_4.html#a7ba49c1dbe5f9ecbc2530987664449d1", null ],
    [ "NumDimensions", "structEigen_1_1internal_1_1traits_3_01TensorPairReducerOp_3_01ReduceOp_00_01Dims_00_01XprType_01_4_01_4.html#a5a95745a23cac539d77f33e994fb0bca", null ]
];