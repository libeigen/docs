var structEigen_1_1internal_1_1traits_3_01TensorReshapingOp_3_01NewDimensions_00_01XprType_01_4_01_4 =
[
    [ "Index", "structEigen_1_1internal_1_1traits_3_01TensorReshapingOp_3_01NewDimensions_00_01XprType_01_4_01_4.html#af47b156e7a0bad8ab80cb42baaffaf44", null ],
    [ "Nested", "structEigen_1_1internal_1_1traits_3_01TensorReshapingOp_3_01NewDimensions_00_01XprType_01_4_01_4.html#a489fd1e4eeb9ee9edc7a1405e7981066", null ],
    [ "Nested_", "structEigen_1_1internal_1_1traits_3_01TensorReshapingOp_3_01NewDimensions_00_01XprType_01_4_01_4.html#a13001bfeb403f350686fc950869705c4", null ],
    [ "PointerType", "structEigen_1_1internal_1_1traits_3_01TensorReshapingOp_3_01NewDimensions_00_01XprType_01_4_01_4.html#a56bec71f0310ffc172b54eeacbed65ec", null ],
    [ "Scalar", "structEigen_1_1internal_1_1traits_3_01TensorReshapingOp_3_01NewDimensions_00_01XprType_01_4_01_4.html#a019870df9fdbae7cd3d1cd4662d0b753", null ],
    [ "StorageKind", "structEigen_1_1internal_1_1traits_3_01TensorReshapingOp_3_01NewDimensions_00_01XprType_01_4_01_4.html#a45cb9ac172aaae96f4cb9af179acf3d2", null ],
    [ "XprTraits", "structEigen_1_1internal_1_1traits_3_01TensorReshapingOp_3_01NewDimensions_00_01XprType_01_4_01_4.html#abd36fb85e2efc874e10e3a582fa02c1a", null ],
    [ "Layout", "structEigen_1_1internal_1_1traits_3_01TensorReshapingOp_3_01NewDimensions_00_01XprType_01_4_01_4.html#aebdb0c56080b1c5bad4f224b253bf34e", null ],
    [ "NumDimensions", "structEigen_1_1internal_1_1traits_3_01TensorReshapingOp_3_01NewDimensions_00_01XprType_01_4_01_4.html#a588a270a149206942a9a1f304c8cbfea", null ]
];