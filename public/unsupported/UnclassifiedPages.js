var UnclassifiedPages =
[
    [ "Resizing", "../TopicResizing.html", null ],
    [ "Vectorization", "../TopicVectorization.html", null ],
    [ "Expression templates in Eigen", "../TopicEigenExpressionTemplates.html", null ],
    [ "Scalar types", "../TopicScalarTypes.html", null ],
    [ "TutorialSparse_example_details", "../TutorialSparse_example_details.html", null ],
    [ "Writing efficient matrix product expressions", "../TopicWritingEfficientProductExpression.html", null ],
    [ "Experimental parts of Eigen", "../Experimental.html", null ]
];