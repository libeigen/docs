var searchData=
[
  ['w_0',['w',['../../classEigen_1_1QuaternionBase.html#a6737fbcb7a6a7181ca65fd4cde8da1f7',1,'Eigen::QuaternionBase::w()'],['../../classEigen_1_1QuaternionBase.html#a86b4fdb33309789f5bc4707e4e0d269a',1,'Eigen::QuaternionBase::w() const']]],
  ['wedge_1',['wedge',['../../classEigen_1_1SkewSymmetricBase.html#aac27f695b2a571ee116cc63909fedd1f',1,'Eigen::SkewSymmetricBase']]],
  ['what_20happens_20inside_20eigen_20on_20a_20simple_20example_2',['What happens inside Eigen, on a simple example',['../../TopicInsideEigenExample.html',1,'UserManual_UnderstandingEigen']]],
  ['when_20expression_20are_20evaluated_3',['Controlling When Expression are Evaluated',['../eigen_tensors.html#autotoc_md14',1,'']]],
  ['wise_20math_20functions_4',['Catalog of coefficient-wise math functions',['../../group__CoeffwiseMathFunctions.html',1,'']]],
  ['wise_20operations_5',['Wise Operations',['../eigen_tensors.html#autotoc_md75',1,'Binary Element Wise Operations'],['../eigen_tensors.html#autotoc_md53',1,'Unary Element Wise Operations']]],
  ['wise_20operations_6',['The Array class and coefficient-wise operations',['../../group__TutorialArrayClass.html',1,'']]],
  ['with_20a_20thread_20pool_7',['Evaluating with a Thread Pool',['../eigen_tensors.html#autotoc_md24',1,'']]],
  ['with_20eigen_8',['Using STL Containers with Eigen',['../../group__TopicStlContainers.html',1,'']]],
  ['with_20raw_20buffers_3a_20the_20map_20class_9',['Interfacing with raw buffers: the Map class',['../../group__TutorialMapClass.html',1,'']]],
  ['with_20the_20defaultdevice_10',['Evaluating With the DefaultDevice',['../eigen_tensors.html#autotoc_md23',1,'']]],
  ['withformat_11',['WithFormat',['../../classEigen_1_1WithFormat.html',1,'Eigen']]],
  ['writeaccessors_12',['WriteAccessors',['../../group__enums.html#gga9f93eac38eb83deb0e8dbd42ddf11d5dabcadf08230fb1a5ef7b3195745d3a458',1,'Eigen::WriteAccessors'],['../../group__enums.html#gga9f93eac38eb83deb0e8dbd42ddf11d5dabcadf08230fb1a5ef7b3195745d3a458',1,'Eigen::WriteAccessors']]],
  ['writing_20efficient_20matrix_20product_20expressions_13',['Writing efficient matrix product expressions',['../../TopicWritingEfficientProductExpression.html',1,'UnclassifiedPages']]],
  ['writing_20functions_20taking_20eigen_20types_20as_20parameters_14',['Writing Functions Taking Eigen Types as Parameters',['../../TopicFunctionTakingEigenTypes.html',1,'UserManual_Generalities']]],
  ['wrong_20assumption_20on_20stack_20alignment_15',['Compiler making a wrong assumption on stack alignment',['../../group__TopicWrongStackAlignment.html',1,'']]]
];
