var searchData=
[
  ['fast_20fourier_20transform_20module_0',['Fast Fourier Transform module',['../group__FFT__Module.html',1,'']]],
  ['fitting_20module_1',['Spline and spline fitting module',['../group__Splines__Module.html',1,'']]],
  ['fixed_20size_20vectorizable_20eigen_20objects_2',['Fixed-size vectorizable Eigen objects',['../../group__TopicFixedSizeVectorizable.html',1,'']]],
  ['flags_3',['Flags',['../../group__flags.html',1,'']]],
  ['for_20sparse_20matrices_4',['Quick reference guide for sparse matrices',['../../group__SparseQuickRefPage.html',1,'']]],
  ['forward_20module_5',['Adolc forward module',['../group__AdolcForward__Module.html',1,'']]],
  ['fourier_20transform_20module_6',['Fast Fourier Transform module',['../group__FFT__Module.html',1,'']]],
  ['free_20solvers_7',['Matrix-free solvers',['../../group__MatrixfreeSolverExample.html',1,'']]],
  ['functions_8',['functions',['../../group__CoeffwiseMathFunctions.html',1,'Catalog of coefficient-wise math functions'],['../../group__TopicPassingByValue.html',1,'Passing Eigen objects by value to functions']]],
  ['functions_20module_9',['functions module',['../group__MatrixFunctions__Module.html',1,'Matrix functions module'],['../group__SpecialFunctions__Module.html',1,'Special math functions module']]]
];
