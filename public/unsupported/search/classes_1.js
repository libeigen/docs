var searchData=
[
  ['bandmatrix_0',['BandMatrix',['../../classEigen_1_1internal_1_1BandMatrix.html',1,'Eigen::internal']]],
  ['bandmatrix_3c_20scalar_2c_20size_2c_20size_2c_20options_20_26selfadjoint_20_3f_200_20_3a1_2c_201_2c_20options_7crowmajor_20_3e_1',['BandMatrix&lt; Scalar, Size, Size, Options &amp;SelfAdjoint ? 0 :1, 1, Options|RowMajor &gt;',['../../classEigen_1_1internal_1_1BandMatrix.html',1,'Eigen::internal']]],
  ['baseexpr_2',['BaseExpr',['../../classEigen_1_1symbolic_1_1BaseExpr.html',1,'Eigen::symbolic']]],
  ['baseexpr_3c_20symbolexpr_3c_20tag_20_3e_20_3e_3',['BaseExpr&lt; SymbolExpr&lt; tag &gt; &gt;',['../../classEigen_1_1symbolic_1_1BaseExpr.html',1,'Eigen::symbolic']]],
  ['baseexpr_3c_20symbolvalue_3c_20tag_2c_20type_20_3e_20_3e_4',['BaseExpr&lt; SymbolValue&lt; Tag, Type &gt; &gt;',['../../classEigen_1_1symbolic_1_1BaseExpr.html',1,'Eigen::symbolic']]],
  ['bdcsvd_5',['BDCSVD',['../../classEigen_1_1BDCSVD.html',1,'Eigen']]],
  ['bicgstab_6',['BiCGSTAB',['../../classEigen_1_1BiCGSTAB.html',1,'Eigen']]],
  ['block_7',['Block',['../../classEigen_1_1Block.html',1,'Eigen']]],
  ['block_3c_20matrixtype_2c_20dim_2c_201_2c_20_21_28internal_3a_3atraits_3c_20matrixtype_20_3e_3a_3aflags_20_26rowmajorbit_29_3e_8',['Block&lt; MatrixType, Dim, 1, !(internal::traits&lt; MatrixType &gt;::Flags &amp;RowMajorBit)&gt;',['../../classEigen_1_1Block.html',1,'Eigen']]],
  ['block_3c_20matrixtype_2c_20dim_2c_20dim_2c_20int_28mode_29_3d_3d_28affinecompact_29_20_26_26_28int_28options_29_20_26rowmajor_29_3d_3d0_20_3e_9',['Block&lt; MatrixType, Dim, Dim, int(Mode)==(AffineCompact) &amp;&amp;(int(Options) &amp;RowMajor)==0 &gt;',['../../classEigen_1_1Block.html',1,'Eigen']]],
  ['block_3c_20matrixtype_2c_20dynamic_2c_20dynamic_20_3e_10',['Block&lt; MatrixType, Dynamic, Dynamic &gt;',['../../classEigen_1_1Block.html',1,'Eigen']]],
  ['block_3c_20vectortype_2c_20internal_3a_3atraits_3c_20vectortype_20_3e_3a_3aflags_20_26rowmajorbit_20_3f_201_20_3asize_2c_20internal_3a_3atraits_3c_20vectortype_20_3e_3a_3aflags_20_26rowmajorbit_20_3f_20size_20_3a1_20_3e_11',['Block&lt; VectorType, internal::traits&lt; VectorType &gt;::Flags &amp;RowMajorBit ? 1 :Size, internal::traits&lt; VectorType &gt;::Flags &amp;RowMajorBit ? Size :1 &gt;',['../../classEigen_1_1Block.html',1,'Eigen']]],
  ['blockimpl_3c_20xprtype_2c_20blockrows_2c_20blockcols_2c_20innerpanel_2c_20sparse_20_3e_12',['BlockImpl&lt; XprType, BlockRows, BlockCols, InnerPanel, Sparse &gt;',['../../classEigen_1_1BlockImpl_3_01XprType_00_01BlockRows_00_01BlockCols_00_01InnerPanel_00_01Sparse_01_4.html',1,'Eigen']]],
  ['blockproperties_13',['BlockProperties',['../structEigen_1_1TensorSycl_1_1internal_1_1BlockProperties.html',1,'Eigen::TensorSycl::internal']]],
  ['blockproperties_3c_20is_5flhs_5ftransposed_2c_20false_2c_20input_5fmapper_5fproperties_3a_3ais_5flhs_5fmatrix_20_26_26vectorizable_2c_20packetreturntype_20_3e_14',['BlockProperties&lt; is_lhs_transposed, false, input_mapper_properties::is_lhs_matrix &amp;&amp;Vectorizable, PacketReturnType &gt;',['../structEigen_1_1TensorSycl_1_1internal_1_1BlockProperties.html',1,'Eigen::TensorSycl::internal']]],
  ['blockproperties_3c_20is_5flhs_5fvec_20_3f_20false_20_3atrue_2c_20is_5flhs_5fvec_20_3f_20false_20_3atrue_2c_20vectorizable_2c_20packetreturntype_20_3e_15',['BlockProperties&lt; is_lhs_vec ? false :true, is_lhs_vec ? false :true, Vectorizable, PacketReturnType &gt;',['../structEigen_1_1TensorSycl_1_1internal_1_1BlockProperties.html',1,'Eigen::TensorSycl::internal']]],
  ['blockproperties_3c_20is_5frhs_5ftransposed_2c_20true_2c_20input_5fmapper_5fproperties_3a_3ais_5frhs_5fmatrix_20_26_26vectorizable_2c_20packetreturntype_20_3e_16',['BlockProperties&lt; is_rhs_transposed, true, input_mapper_properties::is_rhs_matrix &amp;&amp;Vectorizable, PacketReturnType &gt;',['../structEigen_1_1TensorSycl_1_1internal_1_1BlockProperties.html',1,'Eigen::TensorSycl::internal']]],
  ['blocksparsematrix_17',['BlockSparseMatrix',['../classEigen_1_1BlockSparseMatrix.html',1,'Eigen']]],
  ['blocksparsematrix_3c_20scalar_2c_20blocksize_2c_20iscolmajor_20_3f_20colmajor_20_3arowmajor_2c_20storageindex_20_3e_18',['BlockSparseMatrix&lt; Scalar, BlockSize, IsColMajor ? ColMajor :RowMajor, StorageIndex &gt;',['../classEigen_1_1BlockSparseMatrix.html',1,'Eigen']]],
  ['blocksparsematrix_3c_20scalar_5f_2c_20_5fblockatcompiletime_2c_20options_5f_2c_20storageindex_20_3e_19',['BlockSparseMatrix&lt; Scalar_, _BlockAtCompileTime, Options_, StorageIndex &gt;',['../classEigen_1_1BlockSparseMatrix.html',1,'Eigen']]]
];
