var searchData=
[
  ['quaternion_0',['Quaternion',['../../classEigen_1_1Quaternion.html',1,'Eigen']]],
  ['quaternion_3c_20double_20_3e_1',['Quaternion&lt; double &gt;',['../../classEigen_1_1Quaternion.html',1,'Eigen']]],
  ['quaternion_3c_20float_20_3e_2',['Quaternion&lt; float &gt;',['../../classEigen_1_1Quaternion.html',1,'Eigen']]],
  ['quaternion_3c_20scalar_20_3e_3',['Quaternion&lt; Scalar &gt;',['../../classEigen_1_1Quaternion.html',1,'Eigen']]],
  ['quaternionbase_4',['QuaternionBase',['../../classEigen_1_1QuaternionBase.html',1,'Eigen']]],
  ['quaternionbase_3c_20map_3c_20const_20quaternion_3c_20scalar_5f_20_3e_2c_20options_5f_20_3e_20_3e_5',['QuaternionBase&lt; Map&lt; const Quaternion&lt; Scalar_ &gt;, Options_ &gt; &gt;',['../../classEigen_1_1QuaternionBase.html',1,'Eigen']]],
  ['quaternionbase_3c_20map_3c_20quaternion_3c_20scalar_5f_20_3e_2c_20options_5f_20_3e_20_3e_6',['QuaternionBase&lt; Map&lt; Quaternion&lt; Scalar_ &gt;, Options_ &gt; &gt;',['../../classEigen_1_1QuaternionBase.html',1,'Eigen']]],
  ['quaternionbase_3c_20quaternion_3c_20scalar_5f_2c_20options_5f_20_3e_20_3e_7',['QuaternionBase&lt; Quaternion&lt; Scalar_, Options_ &gt; &gt;',['../../classEigen_1_1QuaternionBase.html',1,'Eigen']]]
];
