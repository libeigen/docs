var searchData=
[
  ['negative_20least_20squares_20nnls_20module_0',['Non-Negative Least Squares (NNLS) Module',['../group__nnls.html',1,'']]],
  ['nnls_20module_1',['Non-Negative Least Squares (NNLS) Module',['../group__nnls.html',1,'']]],
  ['non_20linear_20optimization_20module_2',['Non linear optimization module',['../group__NonLinearOptimization__Module.html',1,'']]],
  ['non_20negative_20least_20squares_20nnls_20module_3',['Non-Negative Least Squares (NNLS) Module',['../group__nnls.html',1,'']]],
  ['numerical_20differentiation_20module_4',['Numerical differentiation module',['../group__NumericalDiff__Module.html',1,'']]]
];
