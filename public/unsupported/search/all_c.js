var searchData=
[
  ['kahansum_0',['KahanSum',['../classEigen_1_1KahanSum.html',1,'Eigen']]],
  ['kahansum_3c_20eigen_3a_3atensorevaluator_3c_20expr_2c_20device_20_3e_3a_3ascalar_20_3e_1',['KahanSum&lt; Eigen::TensorEvaluator&lt; Expr, Device &gt;::Scalar &gt;',['../classEigen_1_1KahanSum.html',1,'Eigen']]],
  ['kdbvh_2',['KdBVH',['../classEigen_1_1KdBVH.html',1,'Eigen::KdBVH&lt; Scalar_, Dim_, Object_ &gt;'],['../classEigen_1_1KdBVH.html#aa1a6982d01caafe7d8aa571b888bdd72',1,'Eigen::KdBVH::KdBVH(Iter begin, Iter end)'],['../classEigen_1_1KdBVH.html#a82d92ab2d3d518c0117dbfc30b59b6e3',1,'Eigen::KdBVH::KdBVH(OIter begin, OIter end, BIter boxBegin, BIter boxEnd)']]],
  ['keep_5fdiag_3',['keep_diag',['../../structEigen_1_1IncompleteLUT_1_1keep__diag.html',1,'Eigen::IncompleteLUT&lt; typename Scalar_, typename StorageIndex_ &gt;::keep_diag'],['../../structEigen_1_1SimplicialCholeskyBase_1_1keep__diag.html',1,'Eigen::SimplicialCholeskyBase::keep_diag']]],
  ['kernel_4',['kernel',['../../classEigen_1_1FullPivLU.html#aa27f4b6be627987145705644de7b9d69',1,'Eigen::FullPivLU']]],
  ['kernel_20const_20dimensions_20dims_5',['(Operation) convolve(const Kernel&amp; kernel, const Dimensions&amp; dims)',['../eigen_tensors.html#autotoc_md110',1,'']]],
  ['kernel_20kernel_20const_20dimensions_20dims_6',['(Operation) convolve(const Kernel&amp; kernel, const Dimensions&amp; dims)',['../eigen_tensors.html#autotoc_md110',1,'']]],
  ['kernels_7',['Using Eigen in CUDA kernels',['../../TopicCUDA.html',1,'UserManual_Generalities']]],
  ['keywords_20in_20c_8',['The template and typename keywords in C++',['../../TopicTemplateKeyword.html',1,'UserManual_Generalities']]],
  ['klu_5fsolve_9',['klu_solve',['../../group__KLUSupport__Module.html#gad6d0ed07a6ee97fcef4fe3bce6a674d4',1,'Eigen::klu_solve(klu_symbolic *Symbolic, klu_numeric *Numeric, Index ldim, Index nrhs, double B[], klu_common *Common, double)'],['../../group__KLUSupport__Module.html#gad6d0ed07a6ee97fcef4fe3bce6a674d4',1,'Eigen::klu_solve(klu_symbolic *Symbolic, klu_numeric *Numeric, Index ldim, Index nrhs, double B[], klu_common *Common, double)']]],
  ['klusupport_20module_10',['KLUSupport module',['../../group__KLUSupport__Module.html',1,'']]],
  ['knotaveraging_11',['KnotAveraging',['../group__Splines__Module.html#ga9474da5ed68bbd9a6788a999330416d6',1,'Eigen']]],
  ['knotaveragingwithderivatives_12',['KnotAveragingWithDerivatives',['../group__Splines__Module.html#gae10a6f9b6ab7fb400a2526b6382c533b',1,'Eigen']]],
  ['knots_13',['knots',['../classEigen_1_1Spline.html#ae39dc894ebd2ddff151958145164eaaf',1,'Eigen::Spline']]],
  ['knotvectortype_14',['KnotVectorType',['../classEigen_1_1Spline.html#afd3ce4a146f805551f0312fe17eb0dab',1,'Eigen::Spline::KnotVectorType'],['../structEigen_1_1SplineTraits_3_01Spline_3_01Scalar___00_01Dim___00_01Degree___01_4_00_01Dynamic_01_4.html#af75a3b2dd9bca842ea12dedefcdf3e39',1,'Eigen::SplineTraits&lt; Spline&lt; Scalar_, Dim_, Degree_ &gt;, Dynamic &gt;::KnotVectorType']]],
  ['kroneckerproduct_15',['KroneckerProduct',['../classEigen_1_1KroneckerProduct.html',1,'Eigen::KroneckerProduct&lt; Lhs, Rhs &gt;'],['../classEigen_1_1KroneckerProduct.html#a0b01b6d5ae2413cef8fd91fe7a98a0d7',1,'Eigen::KroneckerProduct::KroneckerProduct()']]],
  ['kroneckerproduct_16',['kroneckerProduct',['../group__KroneckerProduct__Module.html#gaedd4b7cd1e324ed0769cac2701f4d050',1,'Eigen::kroneckerProduct(const MatrixBase&lt; A &gt; &amp;a, const MatrixBase&lt; B &gt; &amp;b)'],['../group__KroneckerProduct__Module.html#ga4e6cd3acfea39bcff3fa38e0de1226f5',1,'Eigen::kroneckerProduct(const EigenBase&lt; A &gt; &amp;a, const EigenBase&lt; B &gt; &amp;b)']]],
  ['kroneckerproduct_20module_17',['KroneckerProduct module',['../group__KroneckerProduct__Module.html',1,'']]],
  ['kroneckerproductbase_18',['KroneckerProductBase',['../classEigen_1_1KroneckerProductBase.html',1,'Eigen::KroneckerProductBase&lt; Derived &gt;'],['../classEigen_1_1KroneckerProductBase.html#a0cb05eaa978b9fdc0285b48a6e0ecfb1',1,'Eigen::KroneckerProductBase::KroneckerProductBase()']]],
  ['kroneckerproductbase_3c_20kroneckerproduct_20_3e_19',['KroneckerProductBase&lt; KroneckerProduct &gt;',['../classEigen_1_1KroneckerProductBase.html',1,'Eigen']]],
  ['kroneckerproductbase_3c_20kroneckerproduct_3c_20lhs_2c_20rhs_20_3e_20_3e_20',['KroneckerProductBase&lt; KroneckerProduct&lt; Lhs, Rhs &gt; &gt;',['../classEigen_1_1KroneckerProductBase.html',1,'Eigen']]],
  ['kroneckerproductbase_3c_20kroneckerproductsparse_20_3e_21',['KroneckerProductBase&lt; KroneckerProductSparse &gt;',['../classEigen_1_1KroneckerProductBase.html',1,'Eigen']]],
  ['kroneckerproductbase_3c_20kroneckerproductsparse_3c_20lhs_2c_20rhs_20_3e_20_3e_22',['KroneckerProductBase&lt; KroneckerProductSparse&lt; Lhs, Rhs &gt; &gt;',['../classEigen_1_1KroneckerProductBase.html',1,'Eigen']]],
  ['kroneckerproductsparse_23',['KroneckerProductSparse',['../classEigen_1_1KroneckerProductSparse.html',1,'Eigen::KroneckerProductSparse&lt; Lhs, Rhs &gt;'],['../classEigen_1_1KroneckerProductSparse.html#ac0a69ba844415fbe79e6514f32b41fb5',1,'Eigen::KroneckerProductSparse::KroneckerProductSparse()']]]
];
