var searchData=
[
  ['sample_5fderivative_0',['SAMPLE_DERIVATIVE',['../../namespaceEigen_1_1internal.html#a50ad5b58e5bd16695bdb61af60992107aac2896d5dd58bdc4ea847a684ef8e32e',1,'Eigen::internal']]],
  ['scatter_1',['Scatter',['../classEigen_1_1internal_1_1StridedLinearBufferCopy.html#a9b33c9925cacf0deff3334bc2f2d389da09870720ca8134284e4e305ac6ce5f19',1,'Eigen::internal::StridedLinearBufferCopy']]],
  ['setterrowmajor_2',['SetterRowMajor',['../classEigen_1_1RandomSetter.html#a667ca7d39d783fdf39f2bf51182b7697a6a73d77a863ed90cc4daddd177c68e60',1,'Eigen::RandomSetter']]],
  ['shardbycol_3',['ShardByCol',['../../namespaceEigen_1_1internal.html#ad2ce556118538734796b09e47dc57a5ba4880b90fa231e2d819d4e8303c09ccdc',1,'Eigen::internal']]],
  ['shardbyrow_4',['ShardByRow',['../../namespaceEigen_1_1internal.html#ad2ce556118538734796b09e47dc57a5baf9cde5417288c6e829f4d62a2033f93a',1,'Eigen::internal']]],
  ['sizeatcompiletime_5',['SizeAtCompileTime',['../structEigen_1_1internal_1_1traits_3_01CoherentPadOp_3_01XprType_00_01SizeAtCompileTime___01_4_01_4.html#ace79e7367e3e47fadf6ababb2f9edeccaa1de091517f79e2b16af79e34b4fc9c6',1,'Eigen::internal::traits&lt; CoherentPadOp&lt; XprType, SizeAtCompileTime_ &gt; &gt;']]],
  ['spd_6',['SPD',['../namespaceEigen.html#ae5023a301f2163f837389a12b39336aeac8f4a3e1f6216d1190f8cfcb2a5106f6',1,'Eigen']]],
  ['supportedaccesspatterns_7',['SupportedAccessPatterns',['../structEigen_1_1internal_1_1traits_3_01BlockSparseMatrix_3_01Scalar___00_01__BlockAtCompileTime_0b1d022c0eadc903e81f8eb0c4ae06a00.html#a071441d038632357c3a23512bef9de79a41ddbab9039031ed54e51f929d459614',1,'Eigen::internal::traits&lt; BlockSparseMatrix&lt; Scalar_, _BlockAtCompileTime, Options_, Index_ &gt; &gt;']]],
  ['swapstorage_8',['SwapStorage',['../classEigen_1_1RandomSetter.html#a667ca7d39d783fdf39f2bf51182b7697a07c15db6221c525aaf7afde30960bcf0',1,'Eigen::RandomSetter']]]
];
