var searchData=
[
  ['least_20squares_20nnls_20module_0',['Non-Negative Least Squares (NNLS) Module',['../group__nnls.html',1,'']]],
  ['least_20squares_20systems_1',['Solving linear least squares systems',['../../group__LeastSquares.html',1,'']]],
  ['levenberg_20marquardt_20module_2',['Levenberg-Marquardt module',['../group__LevenbergMarquardt__Module.html',1,'']]],
  ['linear_20algebra_3',['Sparse linear algebra',['../../group__Sparse__chapter.html',1,'']]],
  ['linear_20algebra_20and_20decompositions_4',['Linear algebra and decompositions',['../../group__TutorialLinearAlgebra.html',1,'']]],
  ['linear_20least_20squares_20systems_5',['Solving linear least squares systems',['../../group__LeastSquares.html',1,'']]],
  ['linear_20optimization_20module_6',['Non linear optimization module',['../group__NonLinearOptimization__Module.html',1,'']]],
  ['linear_20problems_20and_20decompositions_7',['Dense linear problems and decompositions',['../../group__DenseLinearSolvers__chapter.html',1,'']]],
  ['linear_20systems_8',['Solving Sparse Linear Systems',['../../group__TopicSparseSystems.html',1,'']]],
  ['lu_20module_9',['LU module',['../../group__LU__Module.html',1,'']]]
];
