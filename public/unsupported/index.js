var index =
[
    [ "Getting started", "../GettingStarted.html", null ],
    [ "Extending/Customizing Eigen", "../UserManual_CustomizingEigen.html", "UserManual_CustomizingEigen" ],
    [ "General topics", "../UserManual_Generalities.html", "UserManual_Generalities" ],
    [ "Understanding Eigen", "../UserManual_UnderstandingEigen.html", "UserManual_UnderstandingEigen" ],
    [ "Unclassified pages", "../UnclassifiedPages.html", "UnclassifiedPages" ],
    [ "Eigen SYCL Backend", "SYCL_EIGEN.html", null ]
];