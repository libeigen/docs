var structEigen_1_1internal_1_1group__theory_1_1dimino__add__remaining__generators =
[
    [ "_cil", "structEigen_1_1internal_1_1group__theory_1_1dimino__add__remaining__generators.html#ab87e81a8995394df6d4955b8dc368966", null ],
    [ "_helper", "structEigen_1_1internal_1_1group__theory_1_1dimino__add__remaining__generators.html#a429d141777f7ac847deb8f09370114c6", null ],
    [ "_next_iter", "structEigen_1_1internal_1_1group__theory_1_1dimino__add__remaining__generators.html#ae8d3382863033577d1e2027700d706e5", null ],
    [ "first_generator", "structEigen_1_1internal_1_1group__theory_1_1dimino__add__remaining__generators.html#ad8a7372c3d8e3d6b3c4ca1548e7c3468", null ],
    [ "new_elements", "structEigen_1_1internal_1_1group__theory_1_1dimino__add__remaining__generators.html#a2750668ee04c44d42663f0f17460c0b0", null ],
    [ "next_generators", "structEigen_1_1internal_1_1group__theory_1_1dimino__add__remaining__generators.html#a04ba8af4f16365890cdc78e64c40c3ea", null ],
    [ "type", "structEigen_1_1internal_1_1group__theory_1_1dimino__add__remaining__generators.html#a446f68a9af3665750427f885a44e0633", null ],
    [ "global_flags", "structEigen_1_1internal_1_1group__theory_1_1dimino__add__remaining__generators.html#aefcd0bed273cc5d7c88ab7eb1774ded8", null ]
];