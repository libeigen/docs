var classEigen_1_1BlockImpl_3_01XprType_00_01BlockRows_00_01BlockCols_00_01true_00_01Sparse_01_4 =
[
    [ "Base", "classEigen_1_1BlockImpl_3_01XprType_00_01BlockRows_00_01BlockCols_00_01true_00_01Sparse_01_4.html#abc75ece9d24fbb6462c73710779dd18a", null ],
    [ "BlockType", "classEigen_1_1BlockImpl_3_01XprType_00_01BlockRows_00_01BlockCols_00_01true_00_01Sparse_01_4.html#a3027a9f5d132a09a248a3c95f0b8d5c5", null ],
    [ "MatrixTypeNested_", "classEigen_1_1BlockImpl_3_01XprType_00_01BlockRows_00_01BlockCols_00_01true_00_01Sparse_01_4.html#af1c2fbde30e6ffacc5fccc9aa54e9e78", null ],
    [ "BlockImpl", "classEigen_1_1BlockImpl_3_01XprType_00_01BlockRows_00_01BlockCols_00_01true_00_01Sparse_01_4.html#aee0d79245f190c6527c6497ccdf5a64c", null ],
    [ "BlockImpl", "classEigen_1_1BlockImpl_3_01XprType_00_01BlockRows_00_01BlockCols_00_01true_00_01Sparse_01_4.html#a972403c829721b87392efb052a15520f", null ],
    [ "blockCols", "classEigen_1_1BlockImpl_3_01XprType_00_01BlockRows_00_01BlockCols_00_01true_00_01Sparse_01_4.html#aa4fa38172f380c5fc0b4f0384b7323fa", null ],
    [ "blockRows", "classEigen_1_1BlockImpl_3_01XprType_00_01BlockRows_00_01BlockCols_00_01true_00_01Sparse_01_4.html#ac195ca9f0eb4457f30909ec875f5bb2b", null ],
    [ "coeff", "classEigen_1_1BlockImpl_3_01XprType_00_01BlockRows_00_01BlockCols_00_01true_00_01Sparse_01_4.html#afd51fb207e281ddeef2862b545c114d0", null ],
    [ "coeff", "classEigen_1_1BlockImpl_3_01XprType_00_01BlockRows_00_01BlockCols_00_01true_00_01Sparse_01_4.html#a6629ca31ab4fa286135d565e814e065a", null ],
    [ "cols", "classEigen_1_1BlockImpl_3_01XprType_00_01BlockRows_00_01BlockCols_00_01true_00_01Sparse_01_4.html#a5386f41599199dfca8e63c54650df8c3", null ],
    [ "convert_index", "classEigen_1_1BlockImpl_3_01XprType_00_01BlockRows_00_01BlockCols_00_01true_00_01Sparse_01_4.html#ac94a0559670e5f5de67532f937346fbe", null ],
    [ "nestedExpression", "classEigen_1_1BlockImpl_3_01XprType_00_01BlockRows_00_01BlockCols_00_01true_00_01Sparse_01_4.html#a0210e3fcfa494fc97addfd90f0bb0249", null ],
    [ "nestedExpression", "classEigen_1_1BlockImpl_3_01XprType_00_01BlockRows_00_01BlockCols_00_01true_00_01Sparse_01_4.html#aaa85bdb8fedc31ca76a9c11bc55991a2", null ],
    [ "nonZeros", "classEigen_1_1BlockImpl_3_01XprType_00_01BlockRows_00_01BlockCols_00_01true_00_01Sparse_01_4.html#ae05b62da37ab4a46c33f1e9cce6e423e", null ],
    [ "operator=", "classEigen_1_1BlockImpl_3_01XprType_00_01BlockRows_00_01BlockCols_00_01true_00_01Sparse_01_4.html#a1c79ccb5283b0648cf2ba43d6f76c418", null ],
    [ "rows", "classEigen_1_1BlockImpl_3_01XprType_00_01BlockRows_00_01BlockCols_00_01true_00_01Sparse_01_4.html#adb851869c35901c37d5593a1302d88e9", null ],
    [ "startCol", "classEigen_1_1BlockImpl_3_01XprType_00_01BlockRows_00_01BlockCols_00_01true_00_01Sparse_01_4.html#a3b882c4ba689dae08fb73c4488f69247", null ],
    [ "startRow", "classEigen_1_1BlockImpl_3_01XprType_00_01BlockRows_00_01BlockCols_00_01true_00_01Sparse_01_4.html#ae74f2c170925e6e38b9b3892efac1a1b", null ],
    [ "m_matrix", "classEigen_1_1BlockImpl_3_01XprType_00_01BlockRows_00_01BlockCols_00_01true_00_01Sparse_01_4.html#a6e18c50a424065ac274db7c80e1c8093", null ],
    [ "m_outerSize", "classEigen_1_1BlockImpl_3_01XprType_00_01BlockRows_00_01BlockCols_00_01true_00_01Sparse_01_4.html#a6525976971d94e822a9942ad4e7214c2", null ],
    [ "m_outerStart", "classEigen_1_1BlockImpl_3_01XprType_00_01BlockRows_00_01BlockCols_00_01true_00_01Sparse_01_4.html#a3938dc4edb0eba663df891828d5fe30d", null ]
];