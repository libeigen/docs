function generateTableOfContents() {
    const headings = document.querySelectorAll("h1, h2, h3");
    const tocContainer = document.getElementById("nav-tree");
  
    if (!tocContainer) return;
  
    const tocList = document.createElement("ul");
  
    headings.forEach((heading) => {
      const listItem = document.createElement("li");
      const anchor = document.createElement("a");
  
      anchor.href = `#${heading.getElementsByTagName('a')[0].id}`;
      anchor.textContent = heading.textContent;
      listItem.appendChild(anchor);
      tocList.appendChild(listItem);
    });
  
    tocContainer.appendChild(tocList);
  }
  
  // Call the function when the document is fully loaded
  window.addEventListener('DOMContentLoaded', generateTableOfContents);