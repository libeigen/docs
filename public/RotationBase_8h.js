var RotationBase_8h =
[
    [ "Eigen::internal::rotation_base_generic_product_selector< RotationDerived, DiagonalMatrix< Scalar, Dim, MaxDim >, false >", "structEigen_1_1internal_1_1rotation__base__generic__product__selector_3_01RotationDerived_00_01Dce6ab4d4618a8a7d711ee4fd66ebeba4.html", "structEigen_1_1internal_1_1rotation__base__generic__product__selector_3_01RotationDerived_00_01Dce6ab4d4618a8a7d711ee4fd66ebeba4" ],
    [ "Eigen::internal::rotation_base_generic_product_selector< RotationDerived, MatrixType, false >", "structEigen_1_1internal_1_1rotation__base__generic__product__selector_3_01RotationDerived_00_01MatrixType_00_01false_01_4.html", "structEigen_1_1internal_1_1rotation__base__generic__product__selector_3_01RotationDerived_00_01MatrixType_00_01false_01_4" ],
    [ "Eigen::internal::rotation_base_generic_product_selector< RotationDerived, OtherVectorType, true >", "structEigen_1_1internal_1_1rotation__base__generic__product__selector_3_01RotationDerived_00_01OtherVectorType_00_01true_01_4.html", "structEigen_1_1internal_1_1rotation__base__generic__product__selector_3_01RotationDerived_00_01OtherVectorType_00_01true_01_4" ],
    [ "Eigen::RotationBase< Derived, Dim_ >", "classEigen_1_1RotationBase.html", "classEigen_1_1RotationBase" ],
    [ "Eigen::internal::toRotationMatrix", "namespaceEigen_1_1internal.html#a1a3ce3e3df135ef07af6a2e7420d8272", null ],
    [ "Eigen::internal::toRotationMatrix", "namespaceEigen_1_1internal.html#aa528ff5c2afc8ad2677106fecfaec464", null ],
    [ "Eigen::internal::toRotationMatrix", "namespaceEigen_1_1internal.html#a539a2b08a0cdfbd869cb7fd648170d8b", null ]
];