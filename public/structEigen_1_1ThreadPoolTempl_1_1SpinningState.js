var structEigen_1_1ThreadPoolTempl_1_1SpinningState =
[
    [ "Decode", "structEigen_1_1ThreadPoolTempl_1_1SpinningState.html#af41daaf1d6d544982c027ff2f5dabf89", null ],
    [ "Encode", "structEigen_1_1ThreadPoolTempl_1_1SpinningState.html#aa152890f8f6e615c14f4e5c520de51a1", null ],
    [ "kNumNoNotifyMask", "structEigen_1_1ThreadPoolTempl_1_1SpinningState.html#a04439ddf1e0e1152d27aa4172670a129", null ],
    [ "kNumNoNotifyShift", "structEigen_1_1ThreadPoolTempl_1_1SpinningState.html#a54662a1edad9d43656eb3ab4dcf7003c", null ],
    [ "kNumSpinningMask", "structEigen_1_1ThreadPoolTempl_1_1SpinningState.html#a00100e12f670597b5d505317e16a7d5b", null ],
    [ "num_no_notification", "structEigen_1_1ThreadPoolTempl_1_1SpinningState.html#ae1e5fccc16a38ef8e4eeed064b36a299", null ],
    [ "num_spinning", "structEigen_1_1ThreadPoolTempl_1_1SpinningState.html#a5a790ff374e74c8f90c4e28356108b58", null ]
];