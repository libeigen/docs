var structEigen_1_1NumTraits_3_01Eigen_1_1bfloat16_01_4 =
[
    [ "dummy_precision", "structEigen_1_1NumTraits_3_01Eigen_1_1bfloat16_01_4.html#a0540936c1a1d4263ca32f218c16b8487", null ],
    [ "epsilon", "structEigen_1_1NumTraits_3_01Eigen_1_1bfloat16_01_4.html#af4ce32b598e61fce43135e930db69c6b", null ],
    [ "highest", "structEigen_1_1NumTraits_3_01Eigen_1_1bfloat16_01_4.html#a6caf303c877cf9153db3574a2bfecc59", null ],
    [ "infinity", "structEigen_1_1NumTraits_3_01Eigen_1_1bfloat16_01_4.html#a68875f23b40d907bf20d0aa4b6fde692", null ],
    [ "lowest", "structEigen_1_1NumTraits_3_01Eigen_1_1bfloat16_01_4.html#a1c21c2598712f2d7db2d10a36e1125d7", null ],
    [ "quiet_NaN", "structEigen_1_1NumTraits_3_01Eigen_1_1bfloat16_01_4.html#a3e2269923dd2e7862dda4d4283315a6c", null ]
];