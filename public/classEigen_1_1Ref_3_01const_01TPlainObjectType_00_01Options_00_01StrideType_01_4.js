var classEigen_1_1Ref_3_01const_01TPlainObjectType_00_01Options_00_01StrideType_01_4 =
[
    [ "Base", "classEigen_1_1Ref.html#a6df268d7056480b5377f0295bc6c28ab", null ],
    [ "Base", "classEigen_1_1Ref_3_01const_01TPlainObjectType_00_01Options_00_01StrideType_01_4.html#a761b257d77371a3cfbdacda03211fd91", null ],
    [ "Traits", "classEigen_1_1Ref.html#a49325e714d6d855ac077a67f11d535a1", null ],
    [ "Traits", "classEigen_1_1Ref_3_01const_01TPlainObjectType_00_01Options_00_01StrideType_01_4.html#a31111a184ffadfccbeaf1fe5fc3ec329", null ],
    [ "Ref", "classEigen_1_1Ref_3_01const_01TPlainObjectType_00_01Options_00_01StrideType_01_4.html#afcf0ed8a55e9820ddac5035a66542f8a", null ],
    [ "Ref", "classEigen_1_1Ref_3_01const_01TPlainObjectType_00_01Options_00_01StrideType_01_4.html#a81f42b3ee02635e0ea162eb7b8f07c51", null ],
    [ "Ref", "classEigen_1_1Ref_3_01const_01TPlainObjectType_00_01Options_00_01StrideType_01_4.html#a63034249db4466b2b9685d920070f029", null ],
    [ "Ref", "classEigen_1_1Ref_3_01const_01TPlainObjectType_00_01Options_00_01StrideType_01_4.html#aafa495bd11359eba7f4d0ccbdecefcbc", null ],
    [ "Ref", "classEigen_1_1Ref.html#a6dbd3c166ffa7e06a6314b2dc0b5f387", null ],
    [ "Ref", "classEigen_1_1Ref.html#a037addaa81f13e5765e30a92d2c4f2b1", null ],
    [ "construct", "classEigen_1_1Ref_3_01const_01TPlainObjectType_00_01Options_00_01StrideType_01_4.html#a5c4fec441d010c7ca399f23ac9ed200c", null ],
    [ "construct", "classEigen_1_1Ref_3_01const_01TPlainObjectType_00_01Options_00_01StrideType_01_4.html#ae0ab9174859d521dd899827bb257c294", null ],
    [ "m_object", "classEigen_1_1Ref_3_01const_01TPlainObjectType_00_01Options_00_01StrideType_01_4.html#a717dd5ce1a92f509394fe4159cef5161", null ],
    [ "may_map_m_object_successfully", "classEigen_1_1Ref_3_01const_01TPlainObjectType_00_01Options_00_01StrideType_01_4.html#ae1625e8ddd35478a42e903b37b2440e4", null ]
];