var structEigen_1_1internal_1_1pastix__traits_3_01PastixLLT_3_01MatrixType___00_01Options_01_4_01_4 =
[
    [ "MatrixType", "structEigen_1_1internal_1_1pastix__traits_3_01PastixLLT_3_01MatrixType___00_01Options_01_4_01_4.html#abb784ad2ae9fd5c4b1dd088c4ff2eb57", null ],
    [ "RealScalar", "structEigen_1_1internal_1_1pastix__traits_3_01PastixLLT_3_01MatrixType___00_01Options_01_4_01_4.html#aeb10928c34661954f342388bd7cc1335", null ],
    [ "Scalar", "structEigen_1_1internal_1_1pastix__traits_3_01PastixLLT_3_01MatrixType___00_01Options_01_4_01_4.html#aa1bbf6d9bdd66c299326f53a700b56b9", null ],
    [ "StorageIndex", "structEigen_1_1internal_1_1pastix__traits_3_01PastixLLT_3_01MatrixType___00_01Options_01_4_01_4.html#acad0290d71ede9136bf1c06be961c5b8", null ]
];