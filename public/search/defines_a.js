var searchData=
[
  ['make_5fcomp_0',['MAKE_COMP',['../SparseCompressedBase_8h.html#a48eefa8c82c066584120022c5759dd74',1,'SparseCompressedBase.h']]],
  ['max_5fbfloat16_5facc_1',['MAX_BFLOAT16_ACC',['../MatrixProductMMAbfloat16_8h.html#a2e40ecf43d16e00dc4cda9783ddfc408',1,'MatrixProductMMAbfloat16.h']]],
  ['max_5fbfloat16_5facc_5fvsx_2',['MAX_BFLOAT16_ACC_VSX',['../MatrixProduct_8h.html#aa5a55f2e10ccaad784f848e2ac177f01',1,'MatrixProduct.h']]],
  ['max_5fbfloat16_5fvec_5facc_3',['MAX_BFLOAT16_VEC_ACC',['../MatrixProductMMAbfloat16_8h.html#a9d78f22f0fe63f4785a1ba49c64240f1',1,'MatrixProductMMAbfloat16.h']]],
  ['max_5fbfloat16_5fvec_5facc_5fvsx_4',['MAX_BFLOAT16_VEC_ACC_VSX',['../MatrixProduct_8h.html#a0f4665bc47678a4db8f8348c00ffe824',1,'MatrixProduct.h']]],
  ['max_5fcomplex_5fmma_5funroll_5',['MAX_COMPLEX_MMA_UNROLL',['../MatrixProductMMA_8h.html#a6f76f67ece14c0b55c15bff559045f25',1,'MatrixProductMMA.h']]],
  ['max_5fcomplex_5funroll_6',['MAX_COMPLEX_UNROLL',['../MatrixProduct_8h.html#ad872f93399c8a77bdfcfc145c60a8d92',1,'MatrixProduct.h']]],
  ['max_5fmma_5funroll_7',['MAX_MMA_UNROLL',['../MatrixProductMMA_8h.html#a9fb65b6250a5a458e10d471d40863670',1,'MatrixProductMMA.h']]],
  ['max_5funroll_8',['MAX_UNROLL',['../MatrixProduct_8h.html#a406bc102d6d906db6c92b7d2700e1066',1,'MatrixProduct.h']]],
  ['micro_5fadd_9',['MICRO_ADD',['../MatrixProduct_8h.html#a4d4378f4cd4737c3b1702ca7594ca430',1,'MatrixProduct.h']]],
  ['micro_5fadd_5fpeel_10',['MICRO_ADD_PEEL',['../MatrixProduct_8h.html#a49628bcca20f8d99f4c51ca84cde3536',1,'MatrixProduct.h']]],
  ['micro_5fadd_5fpeel_5frow_11',['MICRO_ADD_PEEL_ROW',['../MatrixProduct_8h.html#a890dba15f4a19d4febbafd1ac032582b',1,'MatrixProduct.h']]],
  ['micro_5fadd_5frows_12',['MICRO_ADD_ROWS',['../MatrixProduct_8h.html#aa5ac0e75dc661f4ec10ec4e22dc3ffb3',1,'MatrixProduct.h']]],
  ['micro_5fbroadcast_13',['MICRO_BROADCAST',['../MatrixProduct_8h.html#ad57e9f3050e5abc6cea8054c7c099c37',1,'MatrixProduct.h']]],
  ['micro_5fbroadcast1_14',['MICRO_BROADCAST1',['../MatrixProduct_8h.html#a2922f455b2c9a8b3c9801b1c2cc08269',1,'MatrixProduct.h']]],
  ['micro_5fbroadcast_5fextra_15',['MICRO_BROADCAST_EXTRA',['../MatrixProduct_8h.html#add30fdc9d48daa1aa1a727cd8850c44d',1,'MatrixProduct.h']]],
  ['micro_5fbroadcast_5fextra1_16',['MICRO_BROADCAST_EXTRA1',['../MatrixProduct_8h.html#af04173049e2d87d29d3f0155ad69eae1',1,'MatrixProduct.h']]],
  ['micro_5fcomplex_5fadd_5fcols_17',['MICRO_COMPLEX_ADD_COLS',['../MatrixProduct_8h.html#adf233b836d63d784b14276d019cbcf04',1,'MatrixProduct.h']]],
  ['micro_5fcomplex_5fadd_5fpeel_18',['MICRO_COMPLEX_ADD_PEEL',['../MatrixProduct_8h.html#a2b5dd035ed70df5d0cb03a1db706232a',1,'MatrixProduct.h']]],
  ['micro_5fcomplex_5fadd_5fpeel_5frow_19',['MICRO_COMPLEX_ADD_PEEL_ROW',['../MatrixProduct_8h.html#a95e556c838b006d7b994d298ce955827',1,'MatrixProduct.h']]],
  ['micro_5fcomplex_5fadd_5frows_20',['MICRO_COMPLEX_ADD_ROWS',['../MatrixProduct_8h.html#aa5873d780d13463a512269c3797ad9bd',1,'MatrixProduct.h']]],
  ['micro_5fcomplex_5fbroadcast_21',['MICRO_COMPLEX_BROADCAST',['../MatrixProduct_8h.html#ae78d9ea0710ab55785d2cfb63370e9a1',1,'MatrixProduct.h']]],
  ['micro_5fcomplex_5fbroadcast_5fextra_22',['MICRO_COMPLEX_BROADCAST_EXTRA',['../MatrixProduct_8h.html#a758fc8d0831765a57d780485b26c55bf',1,'MatrixProduct.h']]],
  ['micro_5fcomplex_5fdst_5fptr_23',['MICRO_COMPLEX_DST_PTR',['../MatrixProduct_8h.html#a0af649172fdf415a04821b2c755230da',1,'MatrixProduct.h']]],
  ['micro_5fcomplex_5fdst_5fptr_5fone_24',['MICRO_COMPLEX_DST_PTR_ONE',['../MatrixProduct_8h.html#a36800a3747e2ca72239be935d37aef23',1,'MatrixProduct.h']]],
  ['micro_5fcomplex_5fextra_5fcols_25',['MICRO_COMPLEX_EXTRA_COLS',['../MatrixProduct_8h.html#a084138566bd7a01eef42d04843e74717',1,'MatrixProduct.h']]],
  ['micro_5fcomplex_5fextra_5frows_26',['MICRO_COMPLEX_EXTRA_ROWS',['../MatrixProduct_8h.html#a694d13bcddf92cedafc157a27eaa8083',1,'MatrixProduct.h']]],
  ['micro_5fcomplex_5fload_5fone_27',['MICRO_COMPLEX_LOAD_ONE',['../MatrixProductCommon_8h.html#af7c815a524c3bbfe4efcfb1329311c5c',1,'MatrixProductCommon.h']]],
  ['micro_5fcomplex_5fmma_5fcols_28',['MICRO_COMPLEX_MMA_COLS',['../MatrixProductMMA_8h.html#aa73becaf375958f188a2f4fe1a85194e',1,'MatrixProductMMA.h']]],
  ['micro_5fcomplex_5fmma_5fdst_5fptr_29',['MICRO_COMPLEX_MMA_DST_PTR',['../MatrixProductMMA_8h.html#a3a7a96bb00c7c882a7bc92ede07a1301',1,'MatrixProductMMA.h']]],
  ['micro_5fcomplex_5fmma_5fdst_5fptr_5fone_30',['MICRO_COMPLEX_MMA_DST_PTR_ONE',['../MatrixProductMMA_8h.html#a79d0b7176cca561318974da4667f6242',1,'MatrixProductMMA.h']]],
  ['micro_5fcomplex_5fmma_5fextra_5frows_31',['MICRO_COMPLEX_MMA_EXTRA_ROWS',['../MatrixProductMMA_8h.html#a566da900c2be6e64e31cb07225cd0154',1,'MatrixProductMMA.h']]],
  ['micro_5fcomplex_5fmma_5fextra_5frows1_32',['MICRO_COMPLEX_MMA_EXTRA_ROWS1',['../MatrixProductMMA_8h.html#a1ee9152f916451e5f8274680fd70bbaf',1,'MatrixProductMMA.h']]],
  ['micro_5fcomplex_5fmma_5fiter_5funroll_33',['MICRO_COMPLEX_MMA_ITER_UNROLL',['../MatrixProductMMA_8h.html#a3ef820fb21f8d53e73b57d39d8c246b5',1,'MatrixProductMMA.h']]],
  ['micro_5fcomplex_5fmma_5fload_5fone_5frhs_34',['MICRO_COMPLEX_MMA_LOAD_ONE_RHS',['../MatrixProductMMA_8h.html#a223dc267ba051e9c827896b603a26ffd',1,'MatrixProductMMA.h']]],
  ['micro_5fcomplex_5fmma_5fload_5frhs1_35',['MICRO_COMPLEX_MMA_LOAD_RHS1',['../MatrixProductMMA_8h.html#aad467563d53b0c1440ae77fd6074a9ce',1,'MatrixProductMMA.h']]],
  ['micro_5fcomplex_5fmma_5fone_36',['MICRO_COMPLEX_MMA_ONE',['../MatrixProductMMA_8h.html#a1c0fd041ff323f5b62a7a98c2c046e37',1,'MatrixProductMMA.h']]],
  ['micro_5fcomplex_5fmma_5fone_5fpeel_37',['MICRO_COMPLEX_MMA_ONE_PEEL',['../MatrixProductMMA_8h.html#a8a3ce05bced034758180b951a0b62ac6',1,'MatrixProductMMA.h']]],
  ['micro_5fcomplex_5fmma_5fprefetch_38',['MICRO_COMPLEX_MMA_PREFETCH',['../MatrixProductMMA_8h.html#afc7abe6d1bae2f8fb91c294fc48cf6bc',1,'MatrixProductMMA.h']]],
  ['micro_5fcomplex_5fmma_5frows_39',['MICRO_COMPLEX_MMA_ROWS',['../MatrixProductMMA_8h.html#a20885ab6c31b4ab79ff136fdc63bf31f',1,'MatrixProductMMA.h']]],
  ['micro_5fcomplex_5fmma_5fsrc_5fptr_40',['MICRO_COMPLEX_MMA_SRC_PTR',['../MatrixProductMMA_8h.html#a3d439e488aeaf89e3934121872631487',1,'MatrixProductMMA.h']]],
  ['micro_5fcomplex_5fmma_5fstore_41',['MICRO_COMPLEX_MMA_STORE',['../MatrixProductMMA_8h.html#a5be5ea25ccff9b4fe96e398ac0311468',1,'MatrixProductMMA.h']]],
  ['micro_5fcomplex_5fmma_5fstore_5fone_42',['MICRO_COMPLEX_MMA_STORE_ONE',['../MatrixProductMMA_8h.html#a6eaaeb7a672695560d719e76efaf893e',1,'MatrixProductMMA.h']]],
  ['micro_5fcomplex_5fmma_5ftype_5fpeel_43',['MICRO_COMPLEX_MMA_TYPE_PEEL',['../MatrixProductMMA_8h.html#a25d9aade6b8ff79cbe05a394a79eb972',1,'MatrixProductMMA.h']]],
  ['micro_5fcomplex_5fmma_5funroll_44',['MICRO_COMPLEX_MMA_UNROLL',['../MatrixProductMMA_8h.html#abfe82c0a125e0ad1dbc76b5360fec8db',1,'MatrixProductMMA.h']]],
  ['micro_5fcomplex_5fmma_5funroll_5fiter2_45',['MICRO_COMPLEX_MMA_UNROLL_ITER2',['../MatrixProductMMA_8h.html#ae6561d09770f7cbb9af7e64e23d3dff0',1,'MatrixProductMMA.h']]],
  ['micro_5fcomplex_5fmma_5funroll_5ftype_46',['MICRO_COMPLEX_MMA_UNROLL_TYPE',['../MatrixProductMMA_8h.html#a9c6a1315ec8bf48c023cfa934e807fc2',1,'MatrixProductMMA.h']]],
  ['micro_5fcomplex_5fmma_5funroll_5ftype_5fone_47',['MICRO_COMPLEX_MMA_UNROLL_TYPE_ONE',['../MatrixProductMMA_8h.html#ae0fde1c4a2dc5c942b85d6843296ebe1',1,'MatrixProductMMA.h']]],
  ['micro_5fcomplex_5fmma_5funroll_5ftype_5fpeel_48',['MICRO_COMPLEX_MMA_UNROLL_TYPE_PEEL',['../MatrixProductMMA_8h.html#ac1dc7c3b5fbf64c8c7a2f50e00ab8e19',1,'MatrixProductMMA.h']]],
  ['micro_5fcomplex_5fmma_5fupdate_5frhs_49',['MICRO_COMPLEX_MMA_UPDATE_RHS',['../MatrixProductMMA_8h.html#aa3ea26631cadc2e16a3a74b9f29b9a6d',1,'MatrixProductMMA.h']]],
  ['micro_5fcomplex_5fmma_5fupdate_5frhs1_50',['MICRO_COMPLEX_MMA_UPDATE_RHS1',['../MatrixProductMMA_8h.html#ad09fa031ac0f73a9159611b4bfa4f244',1,'MatrixProductMMA.h']]],
  ['micro_5fcomplex_5fmma_5fwork_51',['MICRO_COMPLEX_MMA_WORK',['../MatrixProductMMA_8h.html#a90104e8ac78943dba1c5f14efb614e8e',1,'MatrixProductMMA.h']]],
  ['micro_5fcomplex_5fmma_5fwork_5fone_52',['MICRO_COMPLEX_MMA_WORK_ONE',['../MatrixProductMMA_8h.html#a041c24f00735c2775a8b251a9a4850aa',1,'MatrixProductMMA.h']]],
  ['micro_5fcomplex_5fone4_53',['MICRO_COMPLEX_ONE4',['../MatrixProduct_8h.html#a9452cc1395ccfae5c29f9f3faf800fb2',1,'MatrixProduct.h']]],
  ['micro_5fcomplex_5fone_5fpeel4_54',['MICRO_COMPLEX_ONE_PEEL4',['../MatrixProduct_8h.html#a60c414fda8741dd577806157e72e2ead',1,'MatrixProduct.h']]],
  ['micro_5fcomplex_5fprefetch_55',['MICRO_COMPLEX_PREFETCH',['../MatrixProduct_8h.html#a6875ace3aa306dea4d5e8912ef4bf1ec',1,'MatrixProduct.h']]],
  ['micro_5fcomplex_5fprefetch_5fone_56',['MICRO_COMPLEX_PREFETCH_ONE',['../MatrixProductCommon_8h.html#ae3a82eb7ca0030f8767a75fe6da7781d',1,'MatrixProductCommon.h']]],
  ['micro_5fcomplex_5fprefetchn_57',['MICRO_COMPLEX_PREFETCHN',['../MatrixProduct_8h.html#a0c4dbc4085436626b6101c877d84a611',1,'MatrixProduct.h']]],
  ['micro_5fcomplex_5fsrc2_5fptr_58',['MICRO_COMPLEX_SRC2_PTR',['../MatrixProduct_8h.html#a16945e16279494d082f6d9484a881747',1,'MatrixProduct.h']]],
  ['micro_5fcomplex_5fsrc_5fptr_59',['MICRO_COMPLEX_SRC_PTR',['../MatrixProduct_8h.html#ac60d5c44c3d0bb3c110277e7577bc7d0',1,'MatrixProduct.h']]],
  ['micro_5fcomplex_5fsrc_5fptr_5fone_60',['MICRO_COMPLEX_SRC_PTR_ONE',['../MatrixProductCommon_8h.html#a5d6ca989ec708a0f1249c1c89c9eccce',1,'MatrixProductCommon.h']]],
  ['micro_5fcomplex_5fstore_61',['MICRO_COMPLEX_STORE',['../MatrixProduct_8h.html#ab0d4ea3a09c2644ce674aafb1583f8ac',1,'MatrixProduct.h']]],
  ['micro_5fcomplex_5fstore_5fone_62',['MICRO_COMPLEX_STORE_ONE',['../MatrixProduct_8h.html#ad0aa7a2a60c1ec5566c006ed67540e06',1,'MatrixProduct.h']]],
  ['micro_5fcomplex_5ftype_5fpeel4_63',['MICRO_COMPLEX_TYPE_PEEL4',['../MatrixProduct_8h.html#a57af29207db195e4b16b9b7afda1faa4',1,'MatrixProduct.h']]],
  ['micro_5fcomplex_5funroll_64',['MICRO_COMPLEX_UNROLL',['../MatrixProduct_8h.html#ac5d9b4263f02bef2307de8ee486d32ff',1,'MatrixProduct.h']]],
  ['micro_5fcomplex_5funroll_5fiter_65',['MICRO_COMPLEX_UNROLL_ITER',['../MatrixProductCommon_8h.html#a09495ea5f78299c8fc112e1bbfa1db5b',1,'MatrixProductCommon.h']]],
  ['micro_5fcomplex_5funroll_5fiter2_66',['MICRO_COMPLEX_UNROLL_ITER2',['../MatrixProduct_8h.html#aa9ec40ffe5cd74e9b80052bbfb4e318c',1,'MatrixProduct.h']]],
  ['micro_5fcomplex_5funroll_5ftype_67',['MICRO_COMPLEX_UNROLL_TYPE',['../MatrixProduct_8h.html#ae789ad986fdc8f38feb3890e7ab4d961',1,'MatrixProduct.h']]],
  ['micro_5fcomplex_5funroll_5ftype_5fone_68',['MICRO_COMPLEX_UNROLL_TYPE_ONE',['../MatrixProduct_8h.html#a496bdb90e8c2a10d81c056fede578657',1,'MatrixProduct.h']]],
  ['micro_5fcomplex_5funroll_5ftype_5fpeel_69',['MICRO_COMPLEX_UNROLL_TYPE_PEEL',['../MatrixProduct_8h.html#aa6e51123012cd598f493f47fa4d9ad64',1,'MatrixProduct.h']]],
  ['micro_5fcomplex_5funroll_5fwork_70',['MICRO_COMPLEX_UNROLL_WORK',['../MatrixProduct_8h.html#a4a591e3040107a4250e100ea2a4909cc',1,'MatrixProduct.h']]],
  ['micro_5fcomplex_5fupdate_71',['MICRO_COMPLEX_UPDATE',['../MatrixProductCommon_8h.html#aba6b1b3419ab4ff15621f5df1f653eca',1,'MatrixProductCommon.h']]],
  ['micro_5fcomplex_5fwork_5fone4_72',['MICRO_COMPLEX_WORK_ONE4',['../MatrixProduct_8h.html#af0ebf5fd003c834a91ad9bd138cbefd6',1,'MatrixProduct.h']]],
  ['micro_5fcomplex_5fwork_5fpeel_73',['MICRO_COMPLEX_WORK_PEEL',['../MatrixProduct_8h.html#a0e7c3e2b44691d2b6ab9e6f0b1d95951',1,'MatrixProduct.h']]],
  ['micro_5fcomplex_5fwork_5fpeel_5frow_74',['MICRO_COMPLEX_WORK_PEEL_ROW',['../MatrixProduct_8h.html#a8390516af3585610a9bd8b00f4ed698d',1,'MatrixProduct.h']]],
  ['micro_5fcomplex_5fzero_5fpeel_75',['MICRO_COMPLEX_ZERO_PEEL',['../MatrixProduct_8h.html#a072c1f705d1dbe1e7366850bddcc12d4',1,'MatrixProduct.h']]],
  ['micro_5fcomplex_5fzero_5fpeel_5frow_76',['MICRO_COMPLEX_ZERO_PEEL_ROW',['../MatrixProduct_8h.html#a8e58b6350fda2e748e2281cefbae8fdd',1,'MatrixProduct.h']]],
  ['micro_5fdst_5fptr_77',['MICRO_DST_PTR',['../MatrixProduct_8h.html#a3e6085133f40ceeddc18e98ad2be9062',1,'MatrixProduct.h']]],
  ['micro_5fdst_5fptr_5fone_78',['MICRO_DST_PTR_ONE',['../MatrixProduct_8h.html#a96ab2809bef6df627d5de622a053bfbc',1,'MatrixProduct.h']]],
  ['micro_5fextra_79',['MICRO_EXTRA',['../MatrixProduct_8h.html#a12e15ccb1ef41497877e00861e15f332',1,'MatrixProduct.h']]],
  ['micro_5fextra_5fcols_80',['MICRO_EXTRA_COLS',['../MatrixProduct_8h.html#a551363c2d00826d4d1f7497fdf6e7414',1,'MatrixProduct.h']]],
  ['micro_5fextra_5frows_81',['MICRO_EXTRA_ROWS',['../MatrixProduct_8h.html#a42b032cb370f9ba54620eecac13e372d',1,'MatrixProduct.h']]],
  ['micro_5fload1_82',['MICRO_LOAD1',['../MatrixProductCommon_8h.html#a9efa059209990fd529ffa3ef446d02f6',1,'MatrixProductCommon.h']]],
  ['micro_5fload_5fone_83',['MICRO_LOAD_ONE',['../MatrixProductCommon_8h.html#ad56e0617e0f0dbc2c8650643426c08de',1,'MatrixProductCommon.h']]],
  ['micro_5fmma_5fcols_84',['MICRO_MMA_COLS',['../MatrixProductMMA_8h.html#ae02b1e73dfb1afc117ffffce5ad853f2',1,'MatrixProductMMA.h']]],
  ['micro_5fmma_5fdst_5fptr_85',['MICRO_MMA_DST_PTR',['../MatrixProductMMA_8h.html#a0aa08525a716db895179d6f03b468a23',1,'MatrixProductMMA.h']]],
  ['micro_5fmma_5fdst_5fptr_5fone_86',['MICRO_MMA_DST_PTR_ONE',['../MatrixProductMMA_8h.html#ad2ffaeaeb709a09198495eaea4840f61',1,'MatrixProductMMA.h']]],
  ['micro_5fmma_5fextra_5frows_87',['MICRO_MMA_EXTRA_ROWS',['../MatrixProductMMA_8h.html#a4e195898245c543be4de9ad68a5234fe',1,'MatrixProductMMA.h']]],
  ['micro_5fmma_5fextra_5frows1_88',['MICRO_MMA_EXTRA_ROWS1',['../MatrixProductMMA_8h.html#ae626c1f74dd4c5dfae4e50eb9db1341c',1,'MatrixProductMMA.h']]],
  ['micro_5fmma_5fiter_5funroll_89',['MICRO_MMA_ITER_UNROLL',['../MatrixProductMMA_8h.html#a5d5c918b241a5ae0d624255c9e478bf0',1,'MatrixProductMMA.h']]],
  ['micro_5fmma_5fload_5fone_5frhs_90',['MICRO_MMA_LOAD_ONE_RHS',['../MatrixProductMMA_8h.html#ae52364e0b7b4df91961128e075d619b8',1,'MatrixProductMMA.h']]],
  ['micro_5fmma_5fload_5fone_5frhs1_91',['MICRO_MMA_LOAD_ONE_RHS1',['../MatrixProductMMA_8h.html#ac25abcb412d5c253c774fcd1ddc678a5',1,'MatrixProductMMA.h']]],
  ['micro_5fmma_5fone_92',['MICRO_MMA_ONE',['../MatrixProductMMA_8h.html#a653a9fa9d2731daab93f51de32907f02',1,'MatrixProductMMA.h']]],
  ['micro_5fmma_5fone_5fpeel_93',['MICRO_MMA_ONE_PEEL',['../MatrixProductMMA_8h.html#a9adc034eee7e3f16dddb1c8b5d713c20',1,'MatrixProductMMA.h']]],
  ['micro_5fmma_5fprefetch_94',['MICRO_MMA_PREFETCH',['../MatrixProductMMA_8h.html#a4a4a19fad3a0c4f35aeb0c993af39bf7',1,'MatrixProductMMA.h']]],
  ['micro_5fmma_5frows_95',['MICRO_MMA_ROWS',['../MatrixProductMMA_8h.html#a60883ff1ee64ee243e594e5c922eaf2d',1,'MatrixProductMMA.h']]],
  ['micro_5fmma_5fsrc_5fptr_96',['MICRO_MMA_SRC_PTR',['../MatrixProductMMA_8h.html#a709be76b4f333a1fe4f6824dd6abd3d6',1,'MatrixProductMMA.h']]],
  ['micro_5fmma_5fstore_97',['MICRO_MMA_STORE',['../MatrixProductMMA_8h.html#a321865ed9356d5053247a00b97b8c205',1,'MatrixProductMMA.h']]],
  ['micro_5fmma_5fstore_5fone_98',['MICRO_MMA_STORE_ONE',['../MatrixProductMMA_8h.html#add5bddcf82071feb866a5b534130a6e9',1,'MatrixProductMMA.h']]],
  ['micro_5fmma_5ftype_5fpeel_99',['MICRO_MMA_TYPE_PEEL',['../MatrixProductMMA_8h.html#ae28bf521ac3e124bcfb94771fe88769d',1,'MatrixProductMMA.h']]],
  ['micro_5fmma_5funroll_100',['MICRO_MMA_UNROLL',['../MatrixProductMMA_8h.html#aa1a18afd02293baa009b943dbc44f963',1,'MatrixProductMMA.h']]],
  ['micro_5fmma_5funroll_5fiter_101',['MICRO_MMA_UNROLL_ITER',['../MatrixProductMMA_8h.html#ad705bffaf4a31ca6d0217910febabee5',1,'MatrixProductMMA.h']]],
  ['micro_5fmma_5funroll_5fiter2_102',['MICRO_MMA_UNROLL_ITER2',['../MatrixProductMMA_8h.html#aafc0abffdb370373c37a1e23e68ff864',1,'MatrixProductMMA.h']]],
  ['micro_5fmma_5funroll_5ftype_103',['MICRO_MMA_UNROLL_TYPE',['../MatrixProductMMA_8h.html#ae20a80433efd042dd41dbdd909fd9ee7',1,'MatrixProductMMA.h']]],
  ['micro_5fmma_5funroll_5ftype_5fone_104',['MICRO_MMA_UNROLL_TYPE_ONE',['../MatrixProductMMA_8h.html#a4e71aaf91e335bfa936eb67bcb0c332d',1,'MatrixProductMMA.h']]],
  ['micro_5fmma_5funroll_5ftype_5fpeel_105',['MICRO_MMA_UNROLL_TYPE_PEEL',['../MatrixProductMMA_8h.html#ad78172c9e1a6fa2a7f44b9ae615a0168',1,'MatrixProductMMA.h']]],
  ['micro_5fmma_5fupdate_5frhs_106',['MICRO_MMA_UPDATE_RHS',['../MatrixProductMMA_8h.html#a8329363f6d84444ce3354c7ca3db884d',1,'MatrixProductMMA.h']]],
  ['micro_5fmma_5fupdate_5frhs1_107',['MICRO_MMA_UPDATE_RHS1',['../MatrixProductMMA_8h.html#aa7f44b6f11a463f74c47deec32cb20fd',1,'MatrixProductMMA.h']]],
  ['micro_5fmma_5fwork_108',['MICRO_MMA_WORK',['../MatrixProductMMA_8h.html#a81fdcb902c0ecc192aefae5503898a83',1,'MatrixProductMMA.h']]],
  ['micro_5fmma_5fwork_5fone_109',['MICRO_MMA_WORK_ONE',['../MatrixProductMMA_8h.html#a53bc6de6b7cd583c6c4ddbd7a33cbac3',1,'MatrixProductMMA.h']]],
  ['micro_5fnew_5frows_110',['MICRO_NEW_ROWS',['../MatrixProduct_8h.html#a197f93526274beedcb6735f29784a435',1,'MatrixProduct.h']]],
  ['micro_5fnormal_111',['MICRO_NORMAL',['../MatrixProductCommon_8h.html#a3c420664366d258d6fa22f4d15d31b70',1,'MatrixProductCommon.h']]],
  ['micro_5fnormal_5fcols_112',['MICRO_NORMAL_COLS',['../MatrixProductCommon_8h.html#ac78245d485c4db5a53a0b4c18a7ca657',1,'MatrixProductCommon.h']]],
  ['micro_5fnormal_5frows_113',['MICRO_NORMAL_ROWS',['../MatrixProduct_8h.html#ae1c85be77af849255b763399512e9a48',1,'MatrixProduct.h']]],
  ['micro_5fone4_114',['MICRO_ONE4',['../MatrixProduct_8h.html#a9b6fcfb08d1084fe4b8ba63f6c5bc5cf',1,'MatrixProduct.h']]],
  ['micro_5fone_5fpeel4_115',['MICRO_ONE_PEEL4',['../MatrixProduct_8h.html#a20348d233937bf059f7420779e6e51df',1,'MatrixProduct.h']]],
  ['micro_5fprefetch_116',['MICRO_PREFETCH',['../MatrixProduct_8h.html#a962c4233ecc9fce315b815ae273422e7',1,'MatrixProduct.h']]],
  ['micro_5fprefetch1_117',['MICRO_PREFETCH1',['../MatrixProductCommon_8h.html#ac08519afde486a3cc18269348ba05926',1,'MatrixProductCommon.h']]],
  ['micro_5fprefetch_5fone_118',['MICRO_PREFETCH_ONE',['../MatrixProductCommon_8h.html#a28d83095e7fc3cafa52f3151c7948616',1,'MatrixProductCommon.h']]],
  ['micro_5fprefetchn_119',['MICRO_PREFETCHN',['../MatrixProduct_8h.html#a18908b13f283262e82769361d814a819',1,'MatrixProduct.h']]],
  ['micro_5fprefetchn1_120',['MICRO_PREFETCHN1',['../MatrixProduct_8h.html#ad0b3eded2535bc9a03d6d7295c864924',1,'MatrixProduct.h']]],
  ['micro_5frhs_121',['MICRO_RHS',['../MatrixProduct_8h.html#a23d8127b6166eebbc29d075bc86accb4',1,'MatrixProduct.h']]],
  ['micro_5fsrc2_122',['MICRO_SRC2',['../MatrixProduct_8h.html#a70987f89664e4935a6a3cf6f2e71afc0',1,'MatrixProduct.h']]],
  ['micro_5fsrc2_5fptr_123',['MICRO_SRC2_PTR',['../MatrixProduct_8h.html#a9363e271f69ca12ee6076f894d0633e9',1,'MatrixProduct.h']]],
  ['micro_5fsrc_5fptr_124',['MICRO_SRC_PTR',['../MatrixProduct_8h.html#abadec34a9edc4aa513424c7cf64258da',1,'MatrixProduct.h']]],
  ['micro_5fsrc_5fptr1_125',['MICRO_SRC_PTR1',['../MatrixProductCommon_8h.html#a48fb3ea3fda99e7b3965fc2f2d50080b',1,'MatrixProductCommon.h']]],
  ['micro_5fsrc_5fptr_5fone_126',['MICRO_SRC_PTR_ONE',['../MatrixProductCommon_8h.html#a2b1c79545d65fe8e8dc9748a5ce0210c',1,'MatrixProductCommon.h']]],
  ['micro_5fstore_127',['MICRO_STORE',['../MatrixProduct_8h.html#a22ef340b9a4ae542d241d5a45789854a',1,'MatrixProduct.h']]],
  ['micro_5fstore_5fone_128',['MICRO_STORE_ONE',['../MatrixProduct_8h.html#ab4e725cbc44698135a52551aa54c65e7',1,'MatrixProduct.h']]],
  ['micro_5ftype_5fpeel4_129',['MICRO_TYPE_PEEL4',['../MatrixProduct_8h.html#a33615e516a72d9eff09864afb8884669',1,'MatrixProduct.h']]],
  ['micro_5funroll_130',['MICRO_UNROLL',['../MatrixProduct_8h.html#a0eb48b265232e30ff54439142e8d8df7',1,'MatrixProduct.h']]],
  ['micro_5funroll_5fiter_131',['MICRO_UNROLL_ITER',['../MatrixProductCommon_8h.html#aedc0bae9686a97a061104c350c00e4dd',1,'MatrixProductCommon.h']]],
  ['micro_5funroll_5fiter1_132',['MICRO_UNROLL_ITER1',['../MatrixProductCommon_8h.html#a8cbcdc5de5a2f28332e4917dba1edb6d',1,'MatrixProductCommon.h']]],
  ['micro_5funroll_5fiter2_133',['MICRO_UNROLL_ITER2',['../MatrixProduct_8h.html#aa223d5af15a55aaa103b7c96ab3f701e',1,'MatrixProduct.h']]],
  ['micro_5funroll_5ftype_134',['MICRO_UNROLL_TYPE',['../MatrixProduct_8h.html#a8814b6ef8cb767c24732f5f6e8fba4a1',1,'MatrixProduct.h']]],
  ['micro_5funroll_5ftype_5fone_135',['MICRO_UNROLL_TYPE_ONE',['../MatrixProduct_8h.html#a2d02f650495ffa75fe8dee9287370f00',1,'MatrixProduct.h']]],
  ['micro_5funroll_5ftype_5fpeel_136',['MICRO_UNROLL_TYPE_PEEL',['../MatrixProduct_8h.html#a985e76d946df959aa6f06e978b398fcd',1,'MatrixProduct.h']]],
  ['micro_5funroll_5fwork_137',['MICRO_UNROLL_WORK',['../MatrixProduct_8h.html#ab693f113c475cdcce8a12b32a2b7056b',1,'MatrixProduct.h']]],
  ['micro_5fupdate_138',['MICRO_UPDATE',['../MatrixProductCommon_8h.html#a83b151628649c2500002fb3bb95b1794',1,'MatrixProductCommon.h']]],
  ['micro_5fupdate_5fmask_139',['MICRO_UPDATE_MASK',['../MatrixProductCommon_8h.html#aef76dee83ac6e3152c3864282756b9b3',1,'MatrixProductCommon.h']]],
  ['micro_5fwork_5fone_140',['MICRO_WORK_ONE',['../MatrixProduct_8h.html#a2896ad090c288e5f45d644bba09c21c9',1,'MatrixProduct.h']]],
  ['micro_5fwork_5fpeel_141',['MICRO_WORK_PEEL',['../MatrixProduct_8h.html#a82d2875f16e7ddf60e3d68e6863d8690',1,'MatrixProduct.h']]],
  ['micro_5fwork_5fpeel_5frow_142',['MICRO_WORK_PEEL_ROW',['../MatrixProduct_8h.html#a29f5c2abbbc742c319f5d18b0f3ca3d0',1,'MatrixProduct.h']]],
  ['micro_5fzero_5fpeel_143',['MICRO_ZERO_PEEL',['../MatrixProduct_8h.html#ab782913982b12a6861c2914e860be0f9',1,'MatrixProduct.h']]],
  ['micro_5fzero_5fpeel_5frow_144',['MICRO_ZERO_PEEL_ROW',['../MatrixProduct_8h.html#a8ad7712a2da4b495d4e4cb4461297914',1,'MatrixProduct.h']]],
  ['mm256_5fshuffle_5fepi32_145',['MM256_SHUFFLE_EPI32',['../AVX_2PacketMath_8h.html#a9c9802d21c2703de826b560d74794722',1,'PacketMath.h']]],
  ['mm256_5funpackhi_5fepi32_146',['MM256_UNPACKHI_EPI32',['../AVX_2PacketMath_8h.html#ad8e183d69772fc22800cdd36570cc867',1,'PacketMath.h']]],
  ['mm256_5funpacklo_5fepi32_147',['MM256_UNPACKLO_EPI32',['../AVX_2PacketMath_8h.html#a493f27f304314bda8090ed78c283dd73',1,'PacketMath.h']]]
];
