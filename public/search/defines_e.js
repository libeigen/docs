var searchData=
[
  ['vec2d_5fduplane_0',['vec2d_duplane',['../SSE_2PacketMath_8h.html#a82105f8a0e30fae094a3f12d6a0c5b40',1,'PacketMath.h']]],
  ['vec2d_5fswizzle1_1',['vec2d_swizzle1',['../SSE_2PacketMath_8h.html#a5ba6bdb2b469d927d9fa06f5d1bfa6d2',1,'PacketMath.h']]],
  ['vec2d_5fswizzle2_2',['vec2d_swizzle2',['../SSE_2PacketMath_8h.html#aa7ad27c50bafd0959091663d5a475586',1,'PacketMath.h']]],
  ['vec4f_5fduplane_3',['vec4f_duplane',['../NEON_2PacketMath_8h.html#a20b4290b14c83139f2ded75e1b86cb0d',1,'vec4f_duplane:&#160;PacketMath.h'],['../SSE_2PacketMath_8h.html#a20b4290b14c83139f2ded75e1b86cb0d',1,'vec4f_duplane:&#160;PacketMath.h']]],
  ['vec4f_5fswizzle1_4',['vec4f_swizzle1',['../SSE_2PacketMath_8h.html#a387a69c0be53c7bd70e93cf4a54c16b2',1,'PacketMath.h']]],
  ['vec4f_5fswizzle2_5',['vec4f_swizzle2',['../SSE_2PacketMath_8h.html#a1ec65d254d25d48c68fbd7fadb9cd5b3',1,'PacketMath.h']]],
  ['vec4i_5fswizzle1_6',['vec4i_swizzle1',['../SSE_2PacketMath_8h.html#aa119a02009b26db7723ad850b7a5b17f',1,'PacketMath.h']]],
  ['vec4i_5fswizzle2_7',['vec4i_swizzle2',['../SSE_2PacketMath_8h.html#af51b8f5c0d4f6a33879d35685f2cac64',1,'PacketMath.h']]],
  ['vec4ui_5fswizzle1_8',['vec4ui_swizzle1',['../SSE_2PacketMath_8h.html#a68993ca03c57e7188ebd730eb9759866',1,'PacketMath.h']]],
  ['vec4ui_5fswizzle2_9',['vec4ui_swizzle2',['../SSE_2PacketMath_8h.html#a592c513835b46e18dd9a15168d551cf7',1,'PacketMath.h']]]
];
