var math__constants_8h =
[
    [ "HIPRT_INF", "math__constants_8h.html#a27bdd1c9a5992656077f68223d898fdf", null ],
    [ "HIPRT_INF_F", "math__constants_8h.html#a3db6bddafefe1a7198f2243d5870ab9d", null ],
    [ "HIPRT_MAX_NORMAL_F", "math__constants_8h.html#a40d528a48cf0d5fd96d9b8936918e830", null ],
    [ "HIPRT_MIN_DENORM_F", "math__constants_8h.html#a9cfcd289ffb28002e18cbe6755c99d3f", null ],
    [ "HIPRT_NAN", "math__constants_8h.html#afa1b5a1a0b228658d7c96c39f2257772", null ],
    [ "HIPRT_NAN_F", "math__constants_8h.html#ac3ab056fc540a017e783aeae64c02e16", null ],
    [ "HIPRT_NEG_ZERO_F", "math__constants_8h.html#acc9114402a72cb3d5bfee255e019983f", null ],
    [ "HIPRT_ONE_F", "math__constants_8h.html#aad23a309d18bd3a67d44a723b78547be", null ],
    [ "HIPRT_ZERO_F", "math__constants_8h.html#a14f27d467ac8bfdcf2047569f2aab359", null ]
];