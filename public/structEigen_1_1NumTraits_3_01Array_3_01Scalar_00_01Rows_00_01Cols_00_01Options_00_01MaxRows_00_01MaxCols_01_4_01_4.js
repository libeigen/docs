var structEigen_1_1NumTraits_3_01Array_3_01Scalar_00_01Rows_00_01Cols_00_01Options_00_01MaxRows_00_01MaxCols_01_4_01_4 =
[
    [ "ArrayType", "structEigen_1_1NumTraits_3_01Array_3_01Scalar_00_01Rows_00_01Cols_00_01Options_00_01MaxRows_00_01MaxCols_01_4_01_4.html#aa65515eccc2c3f0f88080835c975fbc2", null ],
    [ "Literal", "structEigen_1_1NumTraits_3_01Array_3_01Scalar_00_01Rows_00_01Cols_00_01Options_00_01MaxRows_00_01MaxCols_01_4_01_4.html#a1339c70e5bca976d329f493f9c8de37c", null ],
    [ "Nested", "structEigen_1_1NumTraits_3_01Array_3_01Scalar_00_01Rows_00_01Cols_00_01Options_00_01MaxRows_00_01MaxCols_01_4_01_4.html#a2424310bbf6d8d20521d6a8c79324981", null ],
    [ "NonInteger", "structEigen_1_1NumTraits_3_01Array_3_01Scalar_00_01Rows_00_01Cols_00_01Options_00_01MaxRows_00_01MaxCols_01_4_01_4.html#ae33a724d929f2212615ea61500a7acc9", null ],
    [ "NonIntegerScalar", "structEigen_1_1NumTraits_3_01Array_3_01Scalar_00_01Rows_00_01Cols_00_01Options_00_01MaxRows_00_01MaxCols_01_4_01_4.html#a4724a5585f6b9bb8b5b3bdce7de6f3bd", null ],
    [ "Real", "structEigen_1_1NumTraits_3_01Array_3_01Scalar_00_01Rows_00_01Cols_00_01Options_00_01MaxRows_00_01MaxCols_01_4_01_4.html#a4b36f130a9f51f5012d2630ca70431d7", null ],
    [ "RealScalar", "structEigen_1_1NumTraits_3_01Array_3_01Scalar_00_01Rows_00_01Cols_00_01Options_00_01MaxRows_00_01MaxCols_01_4_01_4.html#a0466bb695bcb2ebd0692ad6ae99c37e5", null ],
    [ "digits10", "structEigen_1_1NumTraits_3_01Array_3_01Scalar_00_01Rows_00_01Cols_00_01Options_00_01MaxRows_00_01MaxCols_01_4_01_4.html#ac42a505b6455600139fa25a967de04d6", null ],
    [ "dummy_precision", "structEigen_1_1NumTraits_3_01Array_3_01Scalar_00_01Rows_00_01Cols_00_01Options_00_01MaxRows_00_01MaxCols_01_4_01_4.html#a31fa07749eccb70df72f482baadbd1dd", null ],
    [ "epsilon", "structEigen_1_1NumTraits_3_01Array_3_01Scalar_00_01Rows_00_01Cols_00_01Options_00_01MaxRows_00_01MaxCols_01_4_01_4.html#ac7e6e5ff6b22974c6ed51ea506e9187c", null ],
    [ "max_digits10", "structEigen_1_1NumTraits_3_01Array_3_01Scalar_00_01Rows_00_01Cols_00_01Options_00_01MaxRows_00_01MaxCols_01_4_01_4.html#a7a311eb09eccf47728bb1043b2f8f87b", null ]
];