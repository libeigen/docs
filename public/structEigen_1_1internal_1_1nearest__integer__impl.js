var structEigen_1_1internal_1_1nearest__integer__impl =
[
    [ "run_ceil", "structEigen_1_1internal_1_1nearest__integer__impl.html#a725dd31d62769ddef107d5cfd60549b4", null ],
    [ "run_floor", "structEigen_1_1internal_1_1nearest__integer__impl.html#aab3cb998ff3875c5053a618eb4f4b6c8", null ],
    [ "run_rint", "structEigen_1_1internal_1_1nearest__integer__impl.html#abfb926501b7db0140c27592df1d83885", null ],
    [ "run_round", "structEigen_1_1internal_1_1nearest__integer__impl.html#a197e388a2484aa6a6a6d65eace81b27e", null ],
    [ "run_trunc", "structEigen_1_1internal_1_1nearest__integer__impl.html#a61c66f6d5b87ff211fcca647c288172e", null ]
];