var ReshapedHelper_8h =
[
    [ "Eigen::internal::get_compiletime_reshape_size< SizeType, OtherSize, TotalSize >", "structEigen_1_1internal_1_1get__compiletime__reshape__size.html", null ],
    [ "Eigen::internal::get_compiletime_reshape_size< AutoSize_t, OtherSize, TotalSize >", "structEigen_1_1internal_1_1get__compiletime__reshape__size_3_01AutoSize__t_00_01OtherSize_00_01TotalSize_01_4.html", null ],
    [ "Eigen::AutoSize_t", "namespaceEigen.html#aa6641573852afb7812d40effdb102dc0", [
      [ "Eigen::AutoSize", "namespaceEigen.html#aa6641573852afb7812d40effdb102dc0adc20cb6a2514f0a7c3504ede517cc268", null ]
    ] ],
    [ "Eigen::internal::get_compiletime_reshape_order", "namespaceEigen_1_1internal.html#adfe588422800741743f5d462ce8f98cb", null ],
    [ "Eigen::internal::get_runtime_reshape_size", "namespaceEigen_1_1internal.html#a437637e1c1c90f4b96393827888b40ff", null ],
    [ "Eigen::internal::get_runtime_reshape_size", "namespaceEigen_1_1internal.html#a15e60c6e705186dfa2364e87ed110dff", null ],
    [ "Eigen::AutoOrder", "namespaceEigen.html#a7cbdfe304a8319af94184bc781f583be", null ]
];