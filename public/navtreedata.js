/*
 @licstart  The following is the entire license notice for the JavaScript code in this file.

 The MIT License (MIT)

 Copyright (C) 1997-2020 by Dimitri van Heesch

 Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 and associated documentation files (the "Software"), to deal in the Software without restriction,
 including without limitation the rights to use, copy, modify, merge, publish, distribute,
 sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all copies or
 substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

 @licend  The above is the entire license notice for the JavaScript code in this file
*/
var NAVTREE =
[
  [ "Eigen", "index.html", [
    [ "Getting started", "GettingStarted.html", [
      [ "How to \"install\" Eigen?", "GettingStarted.html#GettingStartedInstallation", null ],
      [ "A simple first program", "GettingStarted.html#GettingStartedFirstProgram", null ],
      [ "Compiling and running your first program", "GettingStarted.html#GettingStartedCompiling", null ],
      [ "Explanation of the first program", "GettingStarted.html#GettingStartedExplanation", null ],
      [ "Example 2: Matrices and vectors", "GettingStarted.html#GettingStartedExample2", null ],
      [ "Explanation of the second example", "GettingStarted.html#GettingStartedExplanation2", null ],
      [ "Where to go from here?", "GettingStarted.html#GettingStartedConclusion", null ]
    ] ],
    [ "Extending/Customizing Eigen", "UserManual_CustomizingEigen.html", "UserManual_CustomizingEigen" ],
    [ "General topics", "UserManual_Generalities.html", "UserManual_Generalities" ],
    [ "Understanding Eigen", "UserManual_UnderstandingEigen.html", "UserManual_UnderstandingEigen" ],
    [ "Unclassified pages", "UnclassifiedPages.html", "UnclassifiedPages" ],
    [ "Chapters", "topics.html", "topics" ],
    [ "Classes", "annotated.html", [
      [ "Class List", "annotated.html", "annotated_dup" ],
      [ "Class Hierarchy", "hierarchy.html", "hierarchy" ],
      [ "Class Members", "functions.html", [
        [ "All", "functions.html", "functions_dup" ],
        [ "Functions", "functions_func.html", "functions_func" ],
        [ "Variables", "functions_vars.html", null ],
        [ "Typedefs", "functions_type.html", null ],
        [ "Enumerations", "functions_enum.html", null ],
        [ "Enumerator", "functions_eval.html", null ],
        [ "Related Symbols", "functions_rela.html", null ]
      ] ]
    ] ],
    [ "Files", "files.html", [
      [ "File List", "files.html", "files_dup" ]
    ] ]
  ] ]
];

var NAVTREEINDEX =
[
"AVX512_2Complex_8h_source.html",
"SPQRSupport_source.html",
"classEigen_1_1AMDOrdering.html#afce433557abcba9e49fa81581a58fa51",
"classEigen_1_1CholmodBase.html#a315bc06aca534380e1e40e0492537a3c",
"classEigen_1_1DenseBase.html#aa695378d5a24b19b4aebf2bbf34022db",
"classEigen_1_1HouseholderSequence.html#a182e62a04a5674d56a244390a67e4b38",
"classEigen_1_1MatrixBase.html#a6d285ea9b37cbd80209e70543d10fd67",
"classEigen_1_1QuaternionBase.html#a5e63b775d0a93161ce6137ec0a17f6b0",
"classEigen_1_1SolverBase.html",
"classEigen_1_1SparseQR.html#a6fdb992f3381ecc5b0a21a692fc0e0a4",
"classEigen_1_1VectorwiseOp.html#a5c6b797457895f11a7682b3a16f263bb",
"group__Geometry__Module.html#ga2fe1a2a012ce0ab0e8da6af134073039",
"group__matrixtypedefs.html#gabe77a3b84d755d0c5405f94a5b2fca60"
];

var SYNCONMSG = 'click to disable panel synchronization';
var SYNCOFFMSG = 'click to enable panel synchronization';