var structEigen_1_1internal_1_1traits_3_01SparseVector_3_01Scalar___00_01Options___00_01StorageIndex___01_4_01_4 =
[
    [ "Scalar", "structEigen_1_1internal_1_1traits_3_01SparseVector_3_01Scalar___00_01Options___00_01StorageIndex___01_4_01_4.html#a657dea2833a32ca777e4598531aad8dc", null ],
    [ "StorageIndex", "structEigen_1_1internal_1_1traits_3_01SparseVector_3_01Scalar___00_01Options___00_01StorageIndex___01_4_01_4.html#ae707ab3f2ee88fdd0a033dc3b7334885", null ],
    [ "StorageKind", "structEigen_1_1internal_1_1traits_3_01SparseVector_3_01Scalar___00_01Options___00_01StorageIndex___01_4_01_4.html#ac6bbac0a0837c900cc222a2ea4b6385d", null ],
    [ "XprKind", "structEigen_1_1internal_1_1traits_3_01SparseVector_3_01Scalar___00_01Options___00_01StorageIndex___01_4_01_4.html#a4951a96c447c2e05d87fb9c8f60fba06", null ]
];