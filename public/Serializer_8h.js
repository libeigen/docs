var Serializer_8h =
[
    [ "Eigen::Serializer< DenseBase< Derived >, void >::Header", "structEigen_1_1Serializer_3_01DenseBase_3_01Derived_01_4_00_01void_01_4_1_1Header.html", "structEigen_1_1Serializer_3_01DenseBase_3_01Derived_01_4_00_01void_01_4_1_1Header" ],
    [ "Eigen::internal::serialize_impl< 0 >", "structEigen_1_1internal_1_1serialize__impl_3_010_01_4.html", "structEigen_1_1internal_1_1serialize__impl_3_010_01_4" ],
    [ "Eigen::internal::serialize_impl< N, T1, Ts... >", "structEigen_1_1internal_1_1serialize__impl_3_01N_00_01T1_00_01Ts_8_8_8_01_4.html", "structEigen_1_1internal_1_1serialize__impl_3_01N_00_01T1_00_01Ts_8_8_8_01_4" ],
    [ "Eigen::Serializer< Array< Scalar, Rows, Cols, Options, MaxRows, MaxCols > >", "classEigen_1_1Serializer_3_01Array_3_01Scalar_00_01Rows_00_01Cols_00_01Options_00_01MaxRows_00_01MaxCols_01_4_01_4.html", null ],
    [ "Eigen::Serializer< DenseBase< Derived >, void >", "classEigen_1_1Serializer_3_01DenseBase_3_01Derived_01_4_00_01void_01_4.html", "classEigen_1_1Serializer_3_01DenseBase_3_01Derived_01_4_00_01void_01_4" ],
    [ "Eigen::Serializer< Matrix< Scalar, Rows, Cols, Options, MaxRows, MaxCols > >", "classEigen_1_1Serializer_3_01Matrix_3_01Scalar_00_01Rows_00_01Cols_00_01Options_00_01MaxRows_00_01MaxCols_01_4_01_4.html", null ],
    [ "Eigen::Serializer< T, typename std::enable_if_t< std::is_trivial< T >::value &&std::is_standard_layout< T >::value > >", "classEigen_1_1Serializer_3_01T_00_01typename_01std_1_1enable__if__t_3_01std_1_1is__trivial_3_01T65876fa03c84e8f1228a8825197da6e6.html", "classEigen_1_1Serializer_3_01T_00_01typename_01std_1_1enable__if__t_3_01std_1_1is__trivial_3_01T65876fa03c84e8f1228a8825197da6e6" ],
    [ "Eigen::deserialize", "namespaceEigen.html#ae07ca485dc99c539806e5edf5e66c335", null ],
    [ "Eigen::serialize", "namespaceEigen.html#aa8a483d2794b34abbece20ef229ab970", null ],
    [ "Eigen::serialize_size", "namespaceEigen.html#ab6414e0dd5cfdeb71d35ddad19198743", null ]
];