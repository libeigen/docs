var OrthoMethods_8h =
[
    [ "Eigen::internal::cross3_impl< Arch, VectorLhs, VectorRhs, Scalar, Vectorizable >", "structEigen_1_1internal_1_1cross3__impl.html", "structEigen_1_1internal_1_1cross3__impl" ],
    [ "Eigen::internal::cross_impl< Derived, OtherDerived, Size >", "structEigen_1_1internal_1_1cross__impl.html", "structEigen_1_1internal_1_1cross__impl" ],
    [ "Eigen::internal::cross_impl< Derived, OtherDerived, 2 >", "structEigen_1_1internal_1_1cross__impl_3_01Derived_00_01OtherDerived_00_012_01_4.html", "structEigen_1_1internal_1_1cross__impl_3_01Derived_00_01OtherDerived_00_012_01_4" ],
    [ "Eigen::internal::unitOrthogonal_selector< Derived, Size >", "structEigen_1_1internal_1_1unitOrthogonal__selector.html", "structEigen_1_1internal_1_1unitOrthogonal__selector" ],
    [ "Eigen::internal::unitOrthogonal_selector< Derived, 2 >", "structEigen_1_1internal_1_1unitOrthogonal__selector_3_01Derived_00_012_01_4.html", "structEigen_1_1internal_1_1unitOrthogonal__selector_3_01Derived_00_012_01_4" ],
    [ "Eigen::internal::unitOrthogonal_selector< Derived, 3 >", "structEigen_1_1internal_1_1unitOrthogonal__selector_3_01Derived_00_013_01_4.html", "structEigen_1_1internal_1_1unitOrthogonal__selector_3_01Derived_00_013_01_4" ]
];