var structEigen_1_1internal_1_1traits_3_01DiagonalWrapper_3_01DiagonalVectorType___01_4_01_4 =
[
    [ "DiagonalVectorType", "structEigen_1_1internal_1_1traits_3_01DiagonalWrapper_3_01DiagonalVectorType___01_4_01_4.html#a082222b03a6c77f80352cf740b2dd0cf", null ],
    [ "Scalar", "structEigen_1_1internal_1_1traits_3_01DiagonalWrapper_3_01DiagonalVectorType___01_4_01_4.html#a7a3c835206f2eb4ce52916806d785722", null ],
    [ "StorageIndex", "structEigen_1_1internal_1_1traits_3_01DiagonalWrapper_3_01DiagonalVectorType___01_4_01_4.html#aa39b96b9131f31d502227f176d764129", null ],
    [ "StorageKind", "structEigen_1_1internal_1_1traits_3_01DiagonalWrapper_3_01DiagonalVectorType___01_4_01_4.html#a61b5fa43f8f0b5b4d7b1a8c33029689d", null ],
    [ "XprKind", "structEigen_1_1internal_1_1traits_3_01DiagonalWrapper_3_01DiagonalVectorType___01_4_01_4.html#a7b985b97de31e319d99e9a2e6d0face0", null ]
];