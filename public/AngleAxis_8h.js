var AngleAxis_8h =
[
    [ "Eigen::internal::traits< AngleAxis< Scalar_ > >", "structEigen_1_1internal_1_1traits_3_01AngleAxis_3_01Scalar___01_4_01_4.html", "structEigen_1_1internal_1_1traits_3_01AngleAxis_3_01Scalar___01_4_01_4" ],
    [ "Eigen::AngleAxisd", "group__Geometry__Module.html#gaed936d6e9192d97f00a9608081fa9b64", null ],
    [ "Eigen::AngleAxisf", "group__Geometry__Module.html#gad823b9c674644b14d950fbfe165dfdbf", null ]
];