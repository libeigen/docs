var structEigen_1_1internal_1_1traits_3_01CwiseUnaryView_3_01ViewOp_00_01MatrixType_00_01StrideType_01_4_01_4 =
[
    [ "MatrixTypeNested", "structEigen_1_1internal_1_1traits_3_01CwiseUnaryView_3_01ViewOp_00_01MatrixType_00_01StrideType_01_4_01_4.html#a643ce56b74730707a54feea432976a45", null ],
    [ "MatrixTypeNested_", "structEigen_1_1internal_1_1traits_3_01CwiseUnaryView_3_01ViewOp_00_01MatrixType_00_01StrideType_01_4_01_4.html#a4063e8a83de804c6a2a21c8cf77a861e", null ],
    [ "Scalar", "structEigen_1_1internal_1_1traits_3_01CwiseUnaryView_3_01ViewOp_00_01MatrixType_00_01StrideType_01_4_01_4.html#ad715e75c06ab08836f8e3560180fb5f2", null ],
    [ "ScalarRef", "structEigen_1_1internal_1_1traits_3_01CwiseUnaryView_3_01ViewOp_00_01MatrixType_00_01StrideType_01_4_01_4.html#aeece844abd9f236740c3bb0586838f07", null ]
];