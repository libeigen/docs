var index =
[
    [ "Getting started", "GettingStarted.html", [
      [ "How to \"install\" Eigen?", "GettingStarted.html#GettingStartedInstallation", null ],
      [ "A simple first program", "GettingStarted.html#GettingStartedFirstProgram", null ],
      [ "Compiling and running your first program", "GettingStarted.html#GettingStartedCompiling", null ],
      [ "Explanation of the first program", "GettingStarted.html#GettingStartedExplanation", null ],
      [ "Example 2: Matrices and vectors", "GettingStarted.html#GettingStartedExample2", null ],
      [ "Explanation of the second example", "GettingStarted.html#GettingStartedExplanation2", null ],
      [ "Where to go from here?", "GettingStarted.html#GettingStartedConclusion", null ]
    ] ],
    [ "Extending/Customizing Eigen", "UserManual_CustomizingEigen.html", "UserManual_CustomizingEigen" ],
    [ "General topics", "UserManual_Generalities.html", "UserManual_Generalities" ],
    [ "Understanding Eigen", "UserManual_UnderstandingEigen.html", "UserManual_UnderstandingEigen" ],
    [ "Unclassified pages", "UnclassifiedPages.html", "UnclassifiedPages" ]
];