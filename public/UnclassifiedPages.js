var UnclassifiedPages =
[
    [ "Resizing", "TopicResizing.html", null ],
    [ "Vectorization", "TopicVectorization.html", null ],
    [ "Expression templates in Eigen", "TopicEigenExpressionTemplates.html", null ],
    [ "Scalar types", "TopicScalarTypes.html", null ],
    [ "TutorialSparse_example_details", "TutorialSparse_example_details.html", null ],
    [ "Writing efficient matrix product expressions", "TopicWritingEfficientProductExpression.html", [
      [ "General Matrix-Matrix product (GEMM)", "TopicWritingEfficientProductExpression.html#GEMM", [
        [ "Limitations", "TopicWritingEfficientProductExpression.html#GEMM_Limitations", null ]
      ] ]
    ] ],
    [ "Experimental parts of Eigen", "Experimental.html", [
      [ "Summary", "Experimental.html#Experimental_summary", null ],
      [ "Experimental modules", "Experimental.html#Experimental_modules", null ],
      [ "Experimental parts of the Core module", "Experimental.html#Experimental_core", null ]
    ] ]
];