var AltiVec_2TypeCasting_8h =
[
    [ "Eigen::internal::type_casting_traits< bfloat16, float >", "structEigen_1_1internal_1_1type__casting__traits_3_01bfloat16_00_01float_01_4.html", null ],
    [ "Eigen::internal::type_casting_traits< bfloat16, unsigned short int >", "structEigen_1_1internal_1_1type__casting__traits_3_01bfloat16_00_01unsigned_01short_01int_01_4.html", null ],
    [ "Eigen::internal::type_casting_traits< float, bfloat16 >", "structEigen_1_1internal_1_1type__casting__traits_3_01float_00_01bfloat16_01_4.html", null ],
    [ "Eigen::internal::type_casting_traits< float, int >", "structEigen_1_1internal_1_1type__casting__traits_3_01float_00_01int_01_4.html", null ],
    [ "Eigen::internal::type_casting_traits< int, float >", "structEigen_1_1internal_1_1type__casting__traits_3_01int_00_01float_01_4.html", null ],
    [ "Eigen::internal::type_casting_traits< unsigned short int, bfloat16 >", "structEigen_1_1internal_1_1type__casting__traits_3_01unsigned_01short_01int_00_01bfloat16_01_4.html", null ],
    [ "Eigen::internal::pcast< Packet4f, Packet4i >", "namespaceEigen_1_1internal.html#a671f91448937ad4bff7d5fdcb9c0f89c", null ],
    [ "Eigen::internal::pcast< Packet4f, Packet4ui >", "namespaceEigen_1_1internal.html#acd16f3338f90e175a7ea2f5b7b9eaec9", null ],
    [ "Eigen::internal::pcast< Packet4f, Packet8bf >", "namespaceEigen_1_1internal.html#afc3f573a0c06e3baff76b56bd692e86d", null ],
    [ "Eigen::internal::pcast< Packet4i, Packet4f >", "namespaceEigen_1_1internal.html#a877472cde5094ed1ca916131b4dfa4a4", null ],
    [ "Eigen::internal::pcast< Packet4ui, Packet4f >", "namespaceEigen_1_1internal.html#a818201c6bee0a953125b06f8ee349ac5", null ],
    [ "Eigen::internal::pcast< Packet8bf, Packet4f >", "namespaceEigen_1_1internal.html#ac7f924a48fcec4ebaa45caf26763e71f", null ],
    [ "Eigen::internal::pcast< Packet8bf, Packet8us >", "namespaceEigen_1_1internal.html#af8145b49f3c275b8633c95dcfe96b532", null ],
    [ "Eigen::internal::pcast< Packet8us, Packet8bf >", "namespaceEigen_1_1internal.html#a27f94cf82eea4de26b9f95aa291e7729", null ],
    [ "Eigen::internal::preinterpret< Packet4f, Packet4i >", "namespaceEigen_1_1internal.html#a3b4633cdbbe2ea0b6e1fad93a38ddd36", null ],
    [ "Eigen::internal::preinterpret< Packet4i, Packet4f >", "namespaceEigen_1_1internal.html#ab441ffdc91287f95b6c5d8558056f6b0", null ]
];