var structEigen_1_1internal_1_1pastix__traits_3_01PastixLU_3_01MatrixType___01_4_01_4 =
[
    [ "MatrixType", "structEigen_1_1internal_1_1pastix__traits_3_01PastixLU_3_01MatrixType___01_4_01_4.html#a5f07ab35b8e62b19bbbaa7ae5c7103c1", null ],
    [ "RealScalar", "structEigen_1_1internal_1_1pastix__traits_3_01PastixLU_3_01MatrixType___01_4_01_4.html#a21286614fa6a042910ecd172b5266a8b", null ],
    [ "Scalar", "structEigen_1_1internal_1_1pastix__traits_3_01PastixLU_3_01MatrixType___01_4_01_4.html#a0b9f33b19bfa8300a7e669739056d11d", null ],
    [ "StorageIndex", "structEigen_1_1internal_1_1pastix__traits_3_01PastixLU_3_01MatrixType___01_4_01_4.html#a35ea94a42fe919251cd25264f981c16a", null ]
];