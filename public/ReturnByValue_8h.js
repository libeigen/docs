var ReturnByValue_8h =
[
    [ "Eigen::internal::evaluator< ReturnByValue< Derived > >", "structEigen_1_1internal_1_1evaluator_3_01ReturnByValue_3_01Derived_01_4_01_4.html", "structEigen_1_1internal_1_1evaluator_3_01ReturnByValue_3_01Derived_01_4_01_4" ],
    [ "Eigen::internal::nested_eval< ReturnByValue< Derived >, n, PlainObject >", "structEigen_1_1internal_1_1nested__eval_3_01ReturnByValue_3_01Derived_01_4_00_01n_00_01PlainObject_01_4.html", "structEigen_1_1internal_1_1nested__eval_3_01ReturnByValue_3_01Derived_01_4_00_01n_00_01PlainObject_01_4" ],
    [ "Eigen::internal::traits< ReturnByValue< Derived > >", "structEigen_1_1internal_1_1traits_3_01ReturnByValue_3_01Derived_01_4_01_4.html", null ]
];