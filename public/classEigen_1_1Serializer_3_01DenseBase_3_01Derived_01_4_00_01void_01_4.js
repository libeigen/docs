var classEigen_1_1Serializer_3_01DenseBase_3_01Derived_01_4_00_01void_01_4 =
[
    [ "Header", "structEigen_1_1Serializer_3_01DenseBase_3_01Derived_01_4_00_01void_01_4_1_1Header.html", "structEigen_1_1Serializer_3_01DenseBase_3_01Derived_01_4_00_01void_01_4_1_1Header" ],
    [ "Scalar", "classEigen_1_1Serializer_3_01DenseBase_3_01Derived_01_4_00_01void_01_4.html#a5323be9b4b73c4b0f25ebbb32e8aef73", null ],
    [ "deserialize", "classEigen_1_1Serializer_3_01DenseBase_3_01Derived_01_4_00_01void_01_4.html#ac4fa328f795c10d3a3add783cae3b248", null ],
    [ "serialize", "classEigen_1_1Serializer_3_01DenseBase_3_01Derived_01_4_00_01void_01_4.html#a516cd8f672444a0ac0bec01b1c7e42d7", null ],
    [ "size", "classEigen_1_1Serializer_3_01DenseBase_3_01Derived_01_4_00_01void_01_4.html#ae01c9f56bc5b64df7a6a0d105adc47fa", null ]
];