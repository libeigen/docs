var structEigen_1_1internal_1_1traits_3_01SparseMatrix_3_01Scalar___00_01Options___00_01StorageIndex___01_4_01_4 =
[
    [ "Scalar", "structEigen_1_1internal_1_1traits_3_01SparseMatrix_3_01Scalar___00_01Options___00_01StorageIndex___01_4_01_4.html#ab470db8295411979125d483078cf9136", null ],
    [ "StorageIndex", "structEigen_1_1internal_1_1traits_3_01SparseMatrix_3_01Scalar___00_01Options___00_01StorageIndex___01_4_01_4.html#adcea32b4115e7d5b5f750d941f5bdb6d", null ],
    [ "StorageKind", "structEigen_1_1internal_1_1traits_3_01SparseMatrix_3_01Scalar___00_01Options___00_01StorageIndex___01_4_01_4.html#a96bb27f30bce949d0908bb319da9ed20", null ],
    [ "XprKind", "structEigen_1_1internal_1_1traits_3_01SparseMatrix_3_01Scalar___00_01Options___00_01StorageIndex___01_4_01_4.html#a0fd8861814473d6afdce2aa072f8ac30", null ]
];