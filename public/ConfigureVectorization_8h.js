var ConfigureVectorization_8h =
[
    [ "EIGEN_ALIGN16", "ConfigureVectorization_8h.html#ac507b2a5b118b428ea7af180982e1fef", null ],
    [ "EIGEN_ALIGN32", "ConfigureVectorization_8h.html#aa6f9a310ee3536433dceaf6e3c897f86", null ],
    [ "EIGEN_ALIGN64", "ConfigureVectorization_8h.html#ac8b94b14bbd3018198d9cb5c1916279a", null ],
    [ "EIGEN_ALIGN8", "ConfigureVectorization_8h.html#a41890ee2a3acbe0e19ad616b62ed9abd", null ],
    [ "EIGEN_ALIGN_MAX", "ConfigureVectorization_8h.html#a13d55467be312f50f2ae60737f1ed9f3", null ],
    [ "EIGEN_ALIGN_TO_AVOID_FALSE_SHARING", "ConfigureVectorization_8h.html#a0e35db8b76beaf3825d9c9bf7402d0e8", null ],
    [ "EIGEN_ALIGN_TO_BOUNDARY", "ConfigureVectorization_8h.html#aca89938efef0393f2715c194d4c25735", null ],
    [ "EIGEN_ALIGNOF", "ConfigureVectorization_8h.html#a747de8f261b6a3cab61f74d7a5deab1c", null ],
    [ "EIGEN_ARCH_WANTS_STACK_ALIGNMENT", "ConfigureVectorization_8h.html#a74a5abac92cee7f6a1341ea198699ead", null ],
    [ "EIGEN_DEFAULT_ALIGN_BYTES", "ConfigureVectorization_8h.html#ac9abd2bd8168b534c444bc66034cdfd8", null ],
    [ "EIGEN_DISABLE_UNALIGNED_ARRAY_ASSERT", "ConfigureVectorization_8h.html#a96733d752f26337e8085e1827258b973", null ],
    [ "EIGEN_DONT_VECTORIZE", "ConfigureVectorization_8h.html#a4962bceade5775b0464f7b604f886cd4", null ],
    [ "EIGEN_GCC_AND_ARCH_DOESNT_WANT_STACK_ALIGNMENT", "ConfigureVectorization_8h.html#ab147968602ac1c98842dfb165901f849", null ],
    [ "EIGEN_IDEAL_MAX_ALIGN_BYTES", "ConfigureVectorization_8h.html#a2cba8f05b6cd8b4db2b2e89ce3975ac7", null ],
    [ "EIGEN_MAX_ALIGN_BYTES", "ConfigureVectorization_8h.html#aabf37293a258a90b1aa96a5d5ec04bfb", null ],
    [ "EIGEN_MAX_STATIC_ALIGN_BYTES", "ConfigureVectorization_8h.html#a5d8e4d9a4f962348bb641fb821508fd1", null ],
    [ "EIGEN_MIN_ALIGN_BYTES", "ConfigureVectorization_8h.html#a1606249c901f685bf6cbd231b280a62a", null ],
    [ "EIGEN_UNALIGNED_VECTORIZE", "ConfigureVectorization_8h.html#a1867e397bb6f521970e4a8e724a5e0db", null ],
    [ "Eigen::SimdInstructionSetsInUse", "namespaceEigen.html#a2cd9a26113d5fae6c6aaa31cc38da4bb", null ]
];