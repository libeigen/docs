var structEigen_1_1Serializer_3_01SparseMatrix_3_01Scalar_00_01Options_00_01StorageIndex_01_4_00_01void_01_4_1_1Header =
[
    [ "cols", "structEigen_1_1Serializer_3_01SparseMatrix_3_01Scalar_00_01Options_00_01StorageIndex_01_4_00_01void_01_4_1_1Header.html#a8c79a93f8a431a256a8d8785610bae2a", null ],
    [ "compressed", "structEigen_1_1Serializer_3_01SparseMatrix_3_01Scalar_00_01Options_00_01StorageIndex_01_4_00_01void_01_4_1_1Header.html#a193e30d4a5760e4e61630f1d9d35fd8f", null ],
    [ "inner_buffer_size", "structEigen_1_1Serializer_3_01SparseMatrix_3_01Scalar_00_01Options_00_01StorageIndex_01_4_00_01void_01_4_1_1Header.html#a3c231dde7f9dbedf2b4cf3ba93ba1979", null ],
    [ "outer_size", "structEigen_1_1Serializer_3_01SparseMatrix_3_01Scalar_00_01Options_00_01StorageIndex_01_4_00_01void_01_4_1_1Header.html#aaa5c394f979e209512cc49a684afbc1f", null ],
    [ "rows", "structEigen_1_1Serializer_3_01SparseMatrix_3_01Scalar_00_01Options_00_01StorageIndex_01_4_00_01void_01_4_1_1Header.html#acb12d7545869b1b64e5a01c575f0181e", null ]
];