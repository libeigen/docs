var structEigen_1_1internal_1_1nearest__integer__packetop__impl_3_01Packet_00_01false_00_01false_01_4 =
[
    [ "Scalar", "structEigen_1_1internal_1_1nearest__integer__packetop__impl_3_01Packet_00_01false_00_01false_01_4.html#aa6d5e0b1f8217f07a23e802a7f24d291", null ],
    [ "run_ceil", "structEigen_1_1internal_1_1nearest__integer__packetop__impl_3_01Packet_00_01false_00_01false_01_4.html#aed67291a65ab2473b4165538bb88fc4b", null ],
    [ "run_ceil", "structEigen_1_1internal_1_1nearest__integer__packetop__impl.html#a6b49ffbf08e930fac02edb7893406b8e", null ],
    [ "run_floor", "structEigen_1_1internal_1_1nearest__integer__packetop__impl_3_01Packet_00_01false_00_01false_01_4.html#ae706d84097f6094b29eeeeb19a984dc1", null ],
    [ "run_floor", "structEigen_1_1internal_1_1nearest__integer__packetop__impl.html#a0a20484e279e2dd2622f658373003afd", null ],
    [ "run_rint", "structEigen_1_1internal_1_1nearest__integer__packetop__impl_3_01Packet_00_01false_00_01false_01_4.html#a2c114ad6c0ce88f0b8d955c6dc2b61e4", null ],
    [ "run_rint", "structEigen_1_1internal_1_1nearest__integer__packetop__impl.html#ab4d6844b9fe8e42786af3e74833cd78c", null ],
    [ "run_round", "structEigen_1_1internal_1_1nearest__integer__packetop__impl_3_01Packet_00_01false_00_01false_01_4.html#a00d2322310ca356fda63d7c6b1227afe", null ],
    [ "run_round", "structEigen_1_1internal_1_1nearest__integer__packetop__impl.html#a7e43491a01d05269b4fdfc873e8e761e", null ],
    [ "run_trunc", "structEigen_1_1internal_1_1nearest__integer__packetop__impl_3_01Packet_00_01false_00_01false_01_4.html#aeb7d75637d8a4cf3ce2530439ebbbfbe", null ],
    [ "run_trunc", "structEigen_1_1internal_1_1nearest__integer__packetop__impl.html#a9d48264c073fefbecf9b346c9cdbc43d", null ]
];