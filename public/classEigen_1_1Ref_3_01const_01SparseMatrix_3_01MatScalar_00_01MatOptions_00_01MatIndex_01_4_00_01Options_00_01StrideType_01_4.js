var classEigen_1_1Ref_3_01const_01SparseMatrix_3_01MatScalar_00_01MatOptions_00_01MatIndex_01_4_00_01Options_00_01StrideType_01_4 =
[
    [ "Base", "classEigen_1_1Ref.html#a6df268d7056480b5377f0295bc6c28ab", null ],
    [ "Base", "classEigen_1_1Ref_3_01const_01SparseMatrix_3_01MatScalar_00_01MatOptions_00_01MatIndex_01_4_00_01Options_00_01StrideType_01_4.html#a8a8f6c044d88e4d3bb0055c282ecf76e", null ],
    [ "TPlainObjectType", "classEigen_1_1Ref_3_01const_01SparseMatrix_3_01MatScalar_00_01MatOptions_00_01MatIndex_01_4_00_01Options_00_01StrideType_01_4.html#a059a7861ff08f5965eb317ffe34b3546", null ],
    [ "Traits", "classEigen_1_1Ref.html#a49325e714d6d855ac077a67f11d535a1", null ],
    [ "Traits", "classEigen_1_1Ref_3_01const_01SparseMatrix_3_01MatScalar_00_01MatOptions_00_01MatIndex_01_4_00_01Options_00_01StrideType_01_4.html#a29ccc1088e320515c5a0fc77d92f642f", null ],
    [ "Ref", "classEigen_1_1Ref_3_01const_01SparseMatrix_3_01MatScalar_00_01MatOptions_00_01MatIndex_01_4_00_01Options_00_01StrideType_01_4.html#a0f416e560a1816c9cc0162ca34577127", null ],
    [ "Ref", "classEigen_1_1Ref_3_01const_01SparseMatrix_3_01MatScalar_00_01MatOptions_00_01MatIndex_01_4_00_01Options_00_01StrideType_01_4.html#a38fe52e2db43fea26ce6636fe6fdebbe", null ],
    [ "Ref", "classEigen_1_1Ref_3_01const_01SparseMatrix_3_01MatScalar_00_01MatOptions_00_01MatIndex_01_4_00_01Options_00_01StrideType_01_4.html#ace5363db924bb87bef960a7a8a4c60b5", null ],
    [ "~Ref", "classEigen_1_1Ref_3_01const_01SparseMatrix_3_01MatScalar_00_01MatOptions_00_01MatIndex_01_4_00_01Options_00_01StrideType_01_4.html#a5115affb3f5d94efae72aa3c083d75cc", null ],
    [ "Ref", "classEigen_1_1Ref.html#a6dbd3c166ffa7e06a6314b2dc0b5f387", null ],
    [ "Ref", "classEigen_1_1Ref.html#a037addaa81f13e5765e30a92d2c4f2b1", null ],
    [ "construct", "classEigen_1_1Ref_3_01const_01SparseMatrix_3_01MatScalar_00_01MatOptions_00_01MatIndex_01_4_00_01Options_00_01StrideType_01_4.html#adbf43a3cd2ee0d0f95a06fb3c9a1a9e7", null ],
    [ "construct", "classEigen_1_1Ref_3_01const_01SparseMatrix_3_01MatScalar_00_01MatOptions_00_01MatIndex_01_4_00_01Options_00_01StrideType_01_4.html#a12064569c004e296512a2392fc42ead6", null ],
    [ "m_hasCopy", "classEigen_1_1Ref_3_01const_01SparseMatrix_3_01MatScalar_00_01MatOptions_00_01MatIndex_01_4_00_01Options_00_01StrideType_01_4.html#abec17deb79f1e51209e9df1b2f421d9b", null ],
    [ "m_storage", "classEigen_1_1Ref_3_01const_01SparseMatrix_3_01MatScalar_00_01MatOptions_00_01MatIndex_01_4_00_01Options_00_01StrideType_01_4.html#a6b7db0e4ef694b1bc3a7ea286f12f232", null ]
];