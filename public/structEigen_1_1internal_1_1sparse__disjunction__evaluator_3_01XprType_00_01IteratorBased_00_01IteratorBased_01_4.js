var structEigen_1_1internal_1_1sparse__disjunction__evaluator_3_01XprType_00_01IteratorBased_00_01IteratorBased_01_4 =
[
    [ "InnerIterator", "classEigen_1_1internal_1_1sparse__disjunction__evaluator_3_01XprType_00_01IteratorBased_00_01Ite085e4e9ffbb189a8a69bafa13a3194f5.html", "classEigen_1_1internal_1_1sparse__disjunction__evaluator_3_01XprType_00_01IteratorBased_00_01Ite085e4e9ffbb189a8a69bafa13a3194f5" ],
    [ "LhsArg", "structEigen_1_1internal_1_1sparse__disjunction__evaluator_3_01XprType_00_01IteratorBased_00_01IteratorBased_01_4.html#a4236cbb0358821b80345ad445ff6bbee", null ],
    [ "LhsIterator", "structEigen_1_1internal_1_1sparse__disjunction__evaluator_3_01XprType_00_01IteratorBased_00_01IteratorBased_01_4.html#ac8355ea4db77260ef3fdbe067745058a", null ],
    [ "RhsArg", "structEigen_1_1internal_1_1sparse__disjunction__evaluator_3_01XprType_00_01IteratorBased_00_01IteratorBased_01_4.html#ab71be9f90b558b4e5e6643a4b6f767bb", null ],
    [ "RhsIterator", "structEigen_1_1internal_1_1sparse__disjunction__evaluator_3_01XprType_00_01IteratorBased_00_01IteratorBased_01_4.html#a411cd245b8e7f2b6bcd2477b4544e6b3", null ],
    [ "Scalar", "structEigen_1_1internal_1_1sparse__disjunction__evaluator_3_01XprType_00_01IteratorBased_00_01IteratorBased_01_4.html#a877dd11162029ee42d7c2c54bdd1760d", null ],
    [ "StorageIndex", "structEigen_1_1internal_1_1sparse__disjunction__evaluator_3_01XprType_00_01IteratorBased_00_01IteratorBased_01_4.html#a482a34df74e49773e72b2cb04c6801ce", null ],
    [ "sparse_disjunction_evaluator", "structEigen_1_1internal_1_1sparse__disjunction__evaluator_3_01XprType_00_01IteratorBased_00_01IteratorBased_01_4.html#aaccd56904523c1bdee793f1e87b8527d", null ],
    [ "nonZerosEstimate", "structEigen_1_1internal_1_1sparse__disjunction__evaluator_3_01XprType_00_01IteratorBased_00_01IteratorBased_01_4.html#a910de0720742ad786e48ebb81526cc42", null ],
    [ "m_functor", "structEigen_1_1internal_1_1sparse__disjunction__evaluator_3_01XprType_00_01IteratorBased_00_01IteratorBased_01_4.html#a8d2383fcc1d26e4541b2bd5009073e3d", null ],
    [ "m_lhsImpl", "structEigen_1_1internal_1_1sparse__disjunction__evaluator_3_01XprType_00_01IteratorBased_00_01IteratorBased_01_4.html#a19ce749cbe3d2fbb882b1f9372ca49d7", null ],
    [ "m_rhsImpl", "structEigen_1_1internal_1_1sparse__disjunction__evaluator_3_01XprType_00_01IteratorBased_00_01IteratorBased_01_4.html#a2a19ec1e7f04e476192b18d03687a96b", null ]
];