var structEigen_1_1NumTraits_3_01Eigen_1_1half_01_4 =
[
    [ "dummy_precision", "structEigen_1_1NumTraits_3_01Eigen_1_1half_01_4.html#a5fe9a257226f4fed918a669112c842cc", null ],
    [ "epsilon", "structEigen_1_1NumTraits_3_01Eigen_1_1half_01_4.html#aad0b621b0fd3049f575241d587f5afc7", null ],
    [ "highest", "structEigen_1_1NumTraits_3_01Eigen_1_1half_01_4.html#ad88c91da24d7de8bfaf80d32ed32a9c8", null ],
    [ "infinity", "structEigen_1_1NumTraits_3_01Eigen_1_1half_01_4.html#a0f7ecd4a55eaf3bdf72d0bc0beae27ac", null ],
    [ "lowest", "structEigen_1_1NumTraits_3_01Eigen_1_1half_01_4.html#ab0dd5cefb622e9780d65fa66bff094f0", null ],
    [ "quiet_NaN", "structEigen_1_1NumTraits_3_01Eigen_1_1half_01_4.html#add5109f87bb83ab7dc2c55560bc33f7e", null ]
];