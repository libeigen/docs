var structEigen_1_1internal_1_1scalar__fuzzy__default__impl_3_01Scalar_00_01false_00_01false_01_4 =
[
    [ "RealScalar", "structEigen_1_1internal_1_1scalar__fuzzy__default__impl_3_01Scalar_00_01false_00_01false_01_4.html#ac7a6fe76e747630758fe3c88904f1836", null ],
    [ "isApprox", "structEigen_1_1internal_1_1scalar__fuzzy__default__impl_3_01Scalar_00_01false_00_01false_01_4.html#a2a6ea2e2d300734f530473334d0acbd5", null ],
    [ "isApproxOrLessThan", "structEigen_1_1internal_1_1scalar__fuzzy__default__impl_3_01Scalar_00_01false_00_01false_01_4.html#a8f6ff31f914ac58f65bd8f7135e7b9f9", null ],
    [ "isMuchSmallerThan", "structEigen_1_1internal_1_1scalar__fuzzy__default__impl_3_01Scalar_00_01false_00_01false_01_4.html#a54c13af5b34932265724af116c424a53", null ]
];