var Geometry__SIMD_8h =
[
    [ "Eigen::internal::cross3_impl< Architecture::Target, VectorLhs, VectorRhs, float, true >", "structEigen_1_1internal_1_1cross3__impl_3_01Architecture_1_1Target_00_01VectorLhs_00_01VectorRhs_00_01float_00_01true_01_4.html", "structEigen_1_1internal_1_1cross3__impl_3_01Architecture_1_1Target_00_01VectorLhs_00_01VectorRhs_00_01float_00_01true_01_4" ],
    [ "Eigen::internal::quat_conj< Architecture::Target, Derived, float >", "structEigen_1_1internal_1_1quat__conj_3_01Architecture_1_1Target_00_01Derived_00_01float_01_4.html", "structEigen_1_1internal_1_1quat__conj_3_01Architecture_1_1Target_00_01Derived_00_01float_01_4" ],
    [ "Eigen::internal::quat_product< Architecture::Target, Derived, OtherDerived, float >", "structEigen_1_1internal_1_1quat__product_3_01Architecture_1_1Target_00_01Derived_00_01OtherDerived_00_01float_01_4.html", "structEigen_1_1internal_1_1quat__product_3_01Architecture_1_1Target_00_01Derived_00_01OtherDerived_00_01float_01_4" ]
];