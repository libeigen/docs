var dir_2710d50ee2280fdb2ea98917fa21ee56 =
[
    [ "AltiVec", "dir_c0a4eaf1523d89053691d736e2856e47.html", "dir_c0a4eaf1523d89053691d736e2856e47" ],
    [ "AVX", "dir_5a7fc090d51efb50257cd335b03baa3e.html", "dir_5a7fc090d51efb50257cd335b03baa3e" ],
    [ "AVX512", "dir_a4aa9145cbb69fad6fff705d34625707.html", "dir_a4aa9145cbb69fad6fff705d34625707" ],
    [ "Default", "dir_b851db232517adb705f79819ba2c1ef8.html", "dir_b851db232517adb705f79819ba2c1ef8" ],
    [ "GPU", "dir_7945fe0dccb514807ab203c4a0e4cebc.html", "dir_7945fe0dccb514807ab203c4a0e4cebc" ],
    [ "HIP", "dir_955a6e50345cc79aa9db8091c438f477.html", "dir_955a6e50345cc79aa9db8091c438f477" ],
    [ "HVX", "dir_36af9c9eb7d6b795e04bd742266ff3be.html", "dir_36af9c9eb7d6b795e04bd742266ff3be" ],
    [ "LSX", "dir_1a05b4beb17d4096cab6a22c3cae540e.html", "dir_1a05b4beb17d4096cab6a22c3cae540e" ],
    [ "MSA", "dir_cf00adba3d57141f7adc55d95f00d2e5.html", "dir_cf00adba3d57141f7adc55d95f00d2e5" ],
    [ "NEON", "dir_cb9419b9733dfc18b6491146e60df8b5.html", "dir_cb9419b9733dfc18b6491146e60df8b5" ],
    [ "SSE", "dir_f0fd0cd87864c3bda9a752310eede656.html", "dir_f0fd0cd87864c3bda9a752310eede656" ],
    [ "SVE", "dir_c07583c6eaec9b90564507dee16168db.html", "dir_c07583c6eaec9b90564507dee16168db" ],
    [ "SYCL", "dir_398cb4c693b07fd4682f60d4d3a55dfe.html", "dir_398cb4c693b07fd4682f60d4d3a55dfe" ],
    [ "ZVector", "dir_25ea5696da530541b61d3c444af661e4.html", "dir_25ea5696da530541b61d3c444af661e4" ]
];